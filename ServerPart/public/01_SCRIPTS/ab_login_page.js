var DEFAULT_WIDTH = 420;
var VIEWPORT_WIDTH_FACT_MOB = 0.8;
var VIEWPORT_HEIGHT_FACT_MOB = 0.7;
var VIEWPORT_HEIGHT_FACT = 0.7;
var MOBILE_WIDTH_BREAK_DOWN = 498;
var adjust_shadow_offset;
var current_X = -1;
var current_Y = 0;
var increment = 1;
var width_height;
var adjust_top;
var adjust_bottom;
var height;
var width;
var depth;
var HEIGHT_WIDTH_RATIO_MOBILE = 1.4;
var HEIGHT_WIDTH_RATIO = 1.4;
var shadow_offset = 40;
var front_translate;
var back_translate;
var top_translate;
var bottom_translate;
var left_translate;
var right_translate;
var angle = 0;
var transitionEvent;
var button_flag;

function whichTransitionEvent() {
    var t, el = document.createElement("fakeelement");
    var transitions = {
        "transition": "transitionend",
        "OTransition": "oTransitionEnd",
        "MozTransition": "transitionend",
        "WebkitTransition": "webkitTransitionEnd"
    };
    for (t in transitions) {
        if (el.style[t] !== undefined) {
            return transitions[t];
        }
    }
}
////////////////////////////////////////////// 
function rotate_align_reset(angle_val_reset, rotate) {
    $("#cubeR").removeClass('rotate_transition');
    rot_Y = 'rotateY(' + 0 + 'deg)';
    $("#cubeR").css({
        transform: rot_Y
    });
    rot_Y = 'rotateY(' + angle_val_reset + 'deg)';
    $("#cube").css({
        transform: rot_Y
    }).promise().done(function() {
        setTimeout(function() {
            rotate_align_resetC(rotate);
        }, 200);
    });
}

function rotate_align_resetC(rotate) {
    $("#cubeR").addClass("rotate_transition");
    var rot_Y = 'rotateY(' + rotate + 'deg)';
    $("#cubeR").css({
        transform: rot_Y
    });
    $("#cubeR").one(transitionEvent, function(event) {
        //alert("The transition has ended!"); 
        button_flag = true;
        //  $("#cube").addClass("rotate_transition");
    });
}

function rotate_align_reseti(angle_val_reset, rotate) {
    rotate_align_resetR(angle_val_reset);
    //   rotate_align_resetC(rotate);
}
// .load() method deprecated from jQuery 1.8 onward
$(window).on("load", function() {
    transitionEvent = whichTransitionEvent();
    button_flag = true;
    /// front
    $("#r-back-f").click(function() {
        if (button_flag) {
            button_flag = false;
            rotate_align_reset(0, 90);
        }
    });
    // Link admin
    //preview
    $("#r-next-f").click(function() {
        if (button_flag) {
            button_flag = false;
            rotate_align_reset(0, -90);
        }
    });
    /////////////////////////////////////////////////////  
    //left
    $("#r-backl").click(function() {
        if (button_flag) {
            button_flag = false;
            rotate_align_reset(90, 90);
            //rotate_align_resetR(90);
        } // rotate_align_resetR(90);
        // rotate_align_resetC(90);
    });
    $("#r-nextl").click(function() {
        if (button_flag) {
            button_flag = false;
            rotate_align_reset(90, -90);
        }
        // alert("hey");
    });
    /////////////////////////////////////////////////////////   
    //back     
    $("#r-backb").click(function() {
        if (button_flag) {
            button_flag = false;
            rotate_align_reset(180, 90);
        } //alert("hey");
    });
    //user login
    $("#r-nextb").click(function() {
        if (button_flag) {
            button_flag = false;
            rotate_align_reset(180, -90);
        }
    });
    ///////////////////////////////////////////    
    //right
    $("#r-backv").click(function() {
        if (button_flag) {
            button_flag = false;
            rotate_align_reset(270, 90);
        }
    });
    //user login
    $("#r-nextv").click(function() {
        if (button_flag) {
            button_flag = false;
            rotate_align_reset(270, -90);
        }
    });
    //////////////////////////// LEFT PANEL /////////////////////////////
    //////////////////////////// BACK  PANEL  ADDRESS /////////////////////////////  
    //contact
    test_fps();
    make_cube();
    // adjust();
    keyboard();
    $(window).resize(function() {
        // alert("Test");
        make_cube();
    });
});
$(document).ready(function() {
    ///////////////////////////////// FRONT PANEL ////////////////////////////////  
    hide_cube(true);
});

function hide_cube(hide) {
    if (hide == true) {
        $("#top").css("visibility", "hidden");
        $("#bottom").css("visibility", "hidden");
        $("#left").css("visibility", "hidden");
        $("#right").css("visibility", "hidden");
        $("#front").css("visibility", "hidden");
        $("#back").css("visibility", "hidden");
        $("#shadow").css("visibility", "hidden");
    } else {
        $("#top").css("visibility", "visible");
        $("#bottom").css("visibility", "visible");
        $("#left").css("visibility", "visible");
        $("#right").css("visibility", "visible");
        $("#front").css("visibility", "visible");
        $("#back").css("visibility", "visible");
        $("#shadow").css("visibility", "visible");
    }
}

function make_cube() {
    hide_cube(true);
    if ($(window).width() >= MOBILE_WIDTH_BREAK_DOWN) {
        adjust_full_width();
        // alert("desktop");
    } else {
        //  alert("mobile");
        adjust_mobile_width();
    }
    hide_cube(false);
}

function adjust_mobile_width() {
    depth = width = Math.floor($(window).width() * VIEWPORT_WIDTH_FACT_MOB);
    height = Math.floor($("#form_containter").outerHeight() + 30);
    $("#viewport").css("width", width);
    $("#viewport").css("height", height);
    $("#viewport").addClass("center_viewport");
    //width = height = depth = DEFAULT_WIDTH;
    var half_depth = Math.floor(depth / 2);
    var half_width = Math.floor(width / 2);
    var half_height = Math.floor(height / 2);
    // $("#viewport").css("top", Math.floor($(document).height() / 2 ));
    // $("#viewport").css("left", Math.floor($(document).width() / 2 ));
    /*
     $("#viewport").css("top", Math.floor($(document).height() / 2 - half_height));
     $("#viewport").css("left", Math.floor($(document).width() / 2 - half_width));
     */
    adjust_shadow_offset = half_height + shadow_offset;
    front_translate = 'translateZ(' + half_depth + 'px)';
    back_translate = 'translateZ(' + (-half_depth) + 'px)' + ' ' + 'rotateY(180deg)';
    top_translate = 'translateY(' + (-half_height) + 'px)' + ' ' + 'rotateX(90deg)';
    bottom_translate = 'translateY(' + half_height + 'px)' + ' ' + 'rotateX(-90deg)';
    left_translate = 'translateX(' + (-half_width) + 'px)' + ' ' + 'rotateY(-90deg)';
    right_translate = 'translateX(' + half_width + 'px)' + ' ' + 'rotateY(90deg)';
    shdow_translate = 'translateY(' + adjust_shadow_offset + 'px)' + ' ' + 'rotateX(-90deg)';
    adjust_view = 'translateZ(' + (-half_depth) + 'px)';
    $("#cubeB").css({
        transform: adjust_view
    });
    $("#front").css({
        transform: front_translate
    });
    $("#back").css({
        transform: back_translate
    });
    $("#top").css({
        transform: top_translate
    });
    $("#bottom").css({
        transform: bottom_translate
    });
    $("#left").css({
        transform: left_translate
    });
    $("#right").css({
        transform: right_translate
    });
    $("#shadow").css({
        transform: shdow_translate
    });
    // var height_content = 
    $("#top").css("height", width);
    $("#bottom").css("height", width);
    $("#shadow").css("height", width);
    var adjust_top = Math.floor(-width / 2);
    var adjust_bottom = Math.floor(height - width / 2);
    top_translate = 'translateY(' + (adjust_top) + 'px)' + ' ' + 'rotateX(90deg)';
    bottom_translate = 'translateY(' + adjust_bottom + 'px)' + ' ' + 'rotateX(-90deg)';
    $("#top").css({
        transform: top_translate
    });
    // $("#top").css("height", width);
    adjust_shadow_offset = adjust_bottom + shadow_offset;
    shdow_translate = 'translateY(' + adjust_shadow_offset + 'px)' + ' ' + 'rotateX(-90deg)';
    $("#bottom").css({
        transform: bottom_translate
    });
    $("#shadow").css({
        transform: shdow_translate
    });
}

function adjust_mobile_widthX() {
    depth = width = Math.floor($(window).width() * VIEWPORT_WIDTH_FACT_MOB);
    height = Math.floor($("#form_containter").outerHeight() + 30);
    $("#viewport").css("width", width);
    $("#viewport").css("height", height);
    $("#viewport").addClass("center_viewport");
    //width = height = depth = DEFAULT_WIDTH;
    var half_depth = Math.floor(depth / 2);
    var half_width = Math.floor(width / 2);
    var half_height = Math.floor(height / 2);
    // $("#viewport").css("top", Math.floor($(document).height() / 2 ));
    // $("#viewport").css("left", Math.floor($(document).width() / 2 ));
    /*
     $("#viewport").css("top", Math.floor($(document).height() / 2 - half_height));
     $("#viewport").css("left", Math.floor($(document).width() / 2 - half_width));
     */
    adjust_shadow_offset = half_height + shadow_offset;
    front_translate = 'translateZ(' + half_depth + 'px)';
    back_translate = 'translateZ(' + (-half_depth) + 'px)' + ' ' + 'rotateY(180deg)';
    top_translate = 'translateY(' + (-half_height) + 'px)' + ' ' + 'rotateX(90deg)';
    bottom_translate = 'translateY(' + half_height + 'px)' + ' ' + 'rotateX(-90deg)';
    left_translate = 'translateX(' + (-half_width) + 'px)' + ' ' + 'rotateY(-90deg)';
    right_translate = 'translateX(' + half_width + 'px)' + ' ' + 'rotateY(90deg)';
    shdow_translate = 'translateY(' + adjust_shadow_offset + 'px)' + ' ' + 'rotateX(-90deg)';
    adjust_view = 'translateZ(' + (-half_depth) + 'px)';
    $("#cubeB").css({
        transform: adjust_view
    });
    $("#front").css({
        transform: front_translate
    });
    $("#back").css({
        transform: back_translate
    });
    $("#top").css({
        transform: top_translate
    });
    $("#bottom").css({
        transform: bottom_translate
    });
    $("#left").css({
        transform: left_translate
    });
    $("#right").css({
        transform: right_translate
    });
    $("#shadow").css({
        transform: shdow_translate
    });
    // var height_content = 
    $("#top").css("height", width);
    $("#bottom").css("height", width);
    $("#shadow").css("height", width);
    var adjust_top = Math.floor(-width / 2);
    var adjust_bottom = Math.floor(height - width / 2);
    top_translate = 'translateY(' + (adjust_top) + 'px)' + ' ' + 'rotateX(90deg)';
    bottom_translate = 'translateY(' + adjust_bottom + 'px)' + ' ' + 'rotateX(-90deg)';
    $("#top").css({
        transform: top_translate
    });
    // $("#top").css("height", width);
    adjust_shadow_offset = adjust_bottom + shadow_offset;
    shdow_translate = 'translateY(' + adjust_shadow_offset + 'px)' + ' ' + 'rotateX(-90deg)';
    $("#bottom").css({
        transform: bottom_translate
    });
    $("#shadow").css({
        transform: shdow_translate
    });
}

function adjust_full_width() {
    width = height = depth = Math.floor($("#form_containter").outerHeight() + 30);
    $("#viewport").css("width", width);
    $("#viewport").css("height", height);
    $("#viewport").addClass("center_viewport");
    //width = height = depth = DEFAULT_WIDTH;
    var half_depth = Math.floor(depth / 2);
    var half_width = Math.floor(width / 2);
    var half_height = Math.floor(height / 2);
    // $("#viewport").css("top", Math.floor($(document).height() / 2 ));
    // $("#viewport").css("left", Math.floor($(document).width() / 2 ));
    /*
     $("#viewport").css("top", Math.floor($(document).height() / 2 - half_height));
     $("#viewport").css("left", Math.floor($(document).width() / 2 - half_width));
     */
    adjust_shadow_offset = half_height + shadow_offset;
    front_translate = 'translateZ(' + half_depth + 'px)';
    back_translate = 'translateZ(' + (-half_depth) + 'px)' + ' ' + 'rotateY(180deg)';
    top_translate = 'translateY(' + (-half_height) + 'px)' + ' ' + 'rotateX(90deg)';
    bottom_translate = 'translateY(' + half_height + 'px)' + ' ' + 'rotateX(-90deg)';
    left_translate = 'translateX(' + (-half_width) + 'px)' + ' ' + 'rotateY(-90deg)';
    right_translate = 'translateX(' + half_width + 'px)' + ' ' + 'rotateY(90deg)';
    shdow_translate = 'translateY(' + adjust_shadow_offset + 'px)' + ' ' + 'rotateX(-90deg)';
    adjust_view = 'translateZ(' + (-half_depth) + 'px)';
    $("#cubeB").css({
        transform: adjust_view
    });
    $("#front").css({
        transform: front_translate
    });
    $("#back").css({
        transform: back_translate
    });
    /*
    $("#top").css({
        transform: top_translate
    });
    $("#bottom").css({
        transform: bottom_translate
    });*/
    $("#left").css({
        transform: left_translate
    });
    $("#right").css({
        transform: right_translate
    });
    /*
    $("#shadow").css({
        transform: shdow_translate
    });*/
    // var height_content = 
    // var height_content = 
    $("#top").css("height", width);
    $("#bottom").css("height", width);
    $("#shadow").css("height", width);
    var adjust_top = Math.floor(-width / 2);
    var adjust_bottom = Math.floor(height - width / 2);
    top_translate = 'translateY(' + (adjust_top) + 'px)' + ' ' + 'rotateX(90deg)';
    bottom_translate = 'translateY(' + adjust_bottom + 'px)' + ' ' + 'rotateX(-90deg)';
    $("#top").css({
        transform: top_translate
    });
    // $("#top").css("height", width);
    adjust_shadow_offset = adjust_bottom + shadow_offset;
    shdow_translate = 'translateY(' + adjust_shadow_offset + 'px)' + ' ' + 'rotateX(-90deg)';
    $("#bottom").css({
        transform: bottom_translate
    });
    $("#shadow").css({
        transform: shdow_translate
    });
}

function test_fps() {
    var rot_X = 'rotateX(' + current_X + 'deg)';
    $("#cube").css({
        transform: rot_X
    });
    $("#X").html(current_X);
}

function keyboard() {
    // your base. I'm in it
    $(document).keydown(function(e) {
        /*
         37 - left
         38 - up
         39 - right
         40 - down 
         */
        var rot_XY;
        if (e.keyCode == 40) {
            console.log("left pressed");
            // current_X = current_X - increment;
            rot_XY = 'rotateX(' + current_X + 'deg)' + ' ' + 'rotateY(' + current_Y + 'deg)';
            $("#cube").css({
                transform: rot_XY
            });
            $("#X").html(current_X);
            $("#Y").html(current_Y);
            return false;
        }
        //right
        if (e.keyCode == 38) {
            //   current_X = current_X + increment;
            console.log("right pressed");
            rot_XY = 'rotateX(' + current_X + 'deg)' + ' ' + 'rotateY(' + current_Y + 'deg)';
            $("#cube").css({
                transform: rot_XY
            });
            $("#X").html(current_X);
            $("#Y").html(current_Y);
            return false;
        }
        if (e.keyCode == 37) {
            current_Y = current_Y - increment;
            console.log("up pressed");
            rot_XY = 'rotateX(' + current_X + 'deg)' + ' ' + 'rotateY(' + current_Y + 'deg)';
            $("#cube").css({
                transform: rot_XY
            });
            $("#X").html(current_X);
            $("#Y").html(current_Y);
            return false;
        }
        //40 down
        if (e.keyCode == 39) {
            current_Y = current_Y + increment;
            console.log("down pressed");
            rot_XY = 'rotateX(' + current_X + 'deg)' + ' ' + 'rotateY(' + current_Y + 'deg)';
            $("#cube").css({
                transform: rot_XY
            });
            $("#X").html(current_X);
            $("#Y").html(current_Y);
            return false;
        }
    });
}
$(document).ready(function() {
    /*
     $('#loginformf').validate({// initialize the plugin
            rules: {
                email_name:
                        {
                            required: true,
                            email: true
                        },
                password_name:
                        {
                            required: true,
                            minlength: 3
                        }
            } 
        });
        $('#loginforml').validate({// initialize the plugin
            rules: {
                email_name:
                        {
                            required: true,
                            email: true
                        },
                password_name:
                        {
                            required: true,
                            minlength: 3
                        }
            }
        });
    */
});
window.countFPS = (function() {
    var lastLoop = (new Date()).getMilliseconds();
    var count = 1;
    var fps = 0;
    return function() {
        var currentLoop = (new Date()).getMilliseconds();
        if (lastLoop > currentLoop) {
            fps = count;
            count = 1;
        } else {
            count += 1;
        }
        lastLoop = currentLoop;
        return fps;
    };
}());
var $out = $('#FPS');
(function loop() {
    requestAnimationFrame(function() {
        $out.html(countFPS());
        loop();
    });
}());
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFhX2N1YmUvYWFfY3ViZS5qcyIsImFhX2N1YmUvYWJfdmFsaWRhdGlvbi5qcyIsImFiX3N0YXR1cy9zdGF0cy5qcyIsImFiX3N0YXR1cy96RlBTLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUN2ZUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQzlDQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImFiX2xvZ2luX3BhZ2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgREVGQVVMVF9XSURUSCA9IDQyMDtcbnZhciBWSUVXUE9SVF9XSURUSF9GQUNUX01PQiA9IDAuODtcbnZhciBWSUVXUE9SVF9IRUlHSFRfRkFDVF9NT0IgPSAwLjc7XG52YXIgVklFV1BPUlRfSEVJR0hUX0ZBQ1QgPSAwLjc7XG52YXIgTU9CSUxFX1dJRFRIX0JSRUFLX0RPV04gPSA0OTg7XG52YXIgYWRqdXN0X3NoYWRvd19vZmZzZXQ7XG52YXIgY3VycmVudF9YID0gLTE7XG52YXIgY3VycmVudF9ZID0gMDtcbnZhciBpbmNyZW1lbnQgPSAxO1xudmFyIHdpZHRoX2hlaWdodDtcbnZhciBhZGp1c3RfdG9wO1xudmFyIGFkanVzdF9ib3R0b207XG52YXIgaGVpZ2h0O1xudmFyIHdpZHRoO1xudmFyIGRlcHRoO1xudmFyIEhFSUdIVF9XSURUSF9SQVRJT19NT0JJTEUgPSAxLjQ7XG52YXIgSEVJR0hUX1dJRFRIX1JBVElPID0gMS40O1xudmFyIHNoYWRvd19vZmZzZXQgPSA0MDtcbnZhciBmcm9udF90cmFuc2xhdGU7XG52YXIgYmFja190cmFuc2xhdGU7XG52YXIgdG9wX3RyYW5zbGF0ZTtcbnZhciBib3R0b21fdHJhbnNsYXRlO1xudmFyIGxlZnRfdHJhbnNsYXRlO1xudmFyIHJpZ2h0X3RyYW5zbGF0ZTtcbnZhciBhbmdsZSA9IDA7XG52YXIgdHJhbnNpdGlvbkV2ZW50O1xudmFyIGJ1dHRvbl9mbGFnO1xuXG5mdW5jdGlvbiB3aGljaFRyYW5zaXRpb25FdmVudCgpIHtcbiAgICB2YXIgdCwgZWwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZmFrZWVsZW1lbnRcIik7XG4gICAgdmFyIHRyYW5zaXRpb25zID0ge1xuICAgICAgICBcInRyYW5zaXRpb25cIjogXCJ0cmFuc2l0aW9uZW5kXCIsXG4gICAgICAgIFwiT1RyYW5zaXRpb25cIjogXCJvVHJhbnNpdGlvbkVuZFwiLFxuICAgICAgICBcIk1velRyYW5zaXRpb25cIjogXCJ0cmFuc2l0aW9uZW5kXCIsXG4gICAgICAgIFwiV2Via2l0VHJhbnNpdGlvblwiOiBcIndlYmtpdFRyYW5zaXRpb25FbmRcIlxuICAgIH07XG4gICAgZm9yICh0IGluIHRyYW5zaXRpb25zKSB7XG4gICAgICAgIGlmIChlbC5zdHlsZVt0XSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICByZXR1cm4gdHJhbnNpdGlvbnNbdF07XG4gICAgICAgIH1cbiAgICB9XG59XG4vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vIFxuZnVuY3Rpb24gcm90YXRlX2FsaWduX3Jlc2V0KGFuZ2xlX3ZhbF9yZXNldCwgcm90YXRlKSB7XG4gICAgJChcIiNjdWJlUlwiKS5yZW1vdmVDbGFzcygncm90YXRlX3RyYW5zaXRpb24nKTtcbiAgICByb3RfWSA9ICdyb3RhdGVZKCcgKyAwICsgJ2RlZyknO1xuICAgICQoXCIjY3ViZVJcIikuY3NzKHtcbiAgICAgICAgdHJhbnNmb3JtOiByb3RfWVxuICAgIH0pO1xuICAgIHJvdF9ZID0gJ3JvdGF0ZVkoJyArIGFuZ2xlX3ZhbF9yZXNldCArICdkZWcpJztcbiAgICAkKFwiI2N1YmVcIikuY3NzKHtcbiAgICAgICAgdHJhbnNmb3JtOiByb3RfWVxuICAgIH0pLnByb21pc2UoKS5kb25lKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICByb3RhdGVfYWxpZ25fcmVzZXRDKHJvdGF0ZSk7XG4gICAgICAgIH0sIDIwMCk7XG4gICAgfSk7XG59XG5cbmZ1bmN0aW9uIHJvdGF0ZV9hbGlnbl9yZXNldEMocm90YXRlKSB7XG4gICAgJChcIiNjdWJlUlwiKS5hZGRDbGFzcyhcInJvdGF0ZV90cmFuc2l0aW9uXCIpO1xuICAgIHZhciByb3RfWSA9ICdyb3RhdGVZKCcgKyByb3RhdGUgKyAnZGVnKSc7XG4gICAgJChcIiNjdWJlUlwiKS5jc3Moe1xuICAgICAgICB0cmFuc2Zvcm06IHJvdF9ZXG4gICAgfSk7XG4gICAgJChcIiNjdWJlUlwiKS5vbmUodHJhbnNpdGlvbkV2ZW50LCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgLy9hbGVydChcIlRoZSB0cmFuc2l0aW9uIGhhcyBlbmRlZCFcIik7IFxuICAgICAgICBidXR0b25fZmxhZyA9IHRydWU7XG4gICAgICAgIC8vICAkKFwiI2N1YmVcIikuYWRkQ2xhc3MoXCJyb3RhdGVfdHJhbnNpdGlvblwiKTtcbiAgICB9KTtcbn1cblxuZnVuY3Rpb24gcm90YXRlX2FsaWduX3Jlc2V0aShhbmdsZV92YWxfcmVzZXQsIHJvdGF0ZSkge1xuICAgIHJvdGF0ZV9hbGlnbl9yZXNldFIoYW5nbGVfdmFsX3Jlc2V0KTtcbiAgICAvLyAgIHJvdGF0ZV9hbGlnbl9yZXNldEMocm90YXRlKTtcbn1cblxuLy8gLmxvYWQoKSBtZXRob2QgZGVwcmVjYXRlZCBmcm9tIGpRdWVyeSAxLjggb253YXJkXG4kKHdpbmRvdykub24oXCJsb2FkXCIsIGZ1bmN0aW9uKCkge1xuXG4gICAgICAgIHRyYW5zaXRpb25FdmVudCA9IHdoaWNoVHJhbnNpdGlvbkV2ZW50KCk7XG4gICAgYnV0dG9uX2ZsYWcgPSB0cnVlO1xuICAgIC8vLyBmcm9udFxuICAgICQoXCIjci1iYWNrLWZcIikuY2xpY2soZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAoYnV0dG9uX2ZsYWcpIHtcbiAgICAgICAgICAgIGJ1dHRvbl9mbGFnID0gZmFsc2U7XG4gICAgICAgICAgICByb3RhdGVfYWxpZ25fcmVzZXQoMCwgOTApO1xuICAgICAgICB9XG4gICAgfSk7XG4gICAgLy8gTGluayBhZG1pblxuICAgIC8vcHJldmlld1xuICAgICQoXCIjci1uZXh0LWZcIikuY2xpY2soZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAoYnV0dG9uX2ZsYWcpIHtcbiAgICAgICAgICAgIGJ1dHRvbl9mbGFnID0gZmFsc2U7XG4gICAgICAgICAgICByb3RhdGVfYWxpZ25fcmVzZXQoMCwgLTkwKTtcbiAgICAgICAgfVxuICAgIH0pO1xuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vICBcbiAgICAvL2xlZnRcbiAgICAkKFwiI3ItYmFja2xcIikuY2xpY2soZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAoYnV0dG9uX2ZsYWcpIHtcbiAgICAgICAgICAgIGJ1dHRvbl9mbGFnID0gZmFsc2U7XG4gICAgICAgICAgICByb3RhdGVfYWxpZ25fcmVzZXQoOTAsIDkwKTtcbiAgICAgICAgICAgIC8vcm90YXRlX2FsaWduX3Jlc2V0Uig5MCk7XG4gICAgICAgIH0gLy8gcm90YXRlX2FsaWduX3Jlc2V0Uig5MCk7XG4gICAgICAgIC8vIHJvdGF0ZV9hbGlnbl9yZXNldEMoOTApO1xuICAgIH0pO1xuICAgICQoXCIjci1uZXh0bFwiKS5jbGljayhmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmIChidXR0b25fZmxhZykge1xuICAgICAgICAgICAgYnV0dG9uX2ZsYWcgPSBmYWxzZTtcbiAgICAgICAgICAgIHJvdGF0ZV9hbGlnbl9yZXNldCg5MCwgLTkwKTtcbiAgICAgICAgfVxuICAgICAgICAvLyBhbGVydChcImhleVwiKTtcbiAgICB9KTtcbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8gICBcbiAgICAvL2JhY2sgICAgIFxuICAgICQoXCIjci1iYWNrYlwiKS5jbGljayhmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmIChidXR0b25fZmxhZykge1xuICAgICAgICAgICAgYnV0dG9uX2ZsYWcgPSBmYWxzZTtcbiAgICAgICAgICAgIHJvdGF0ZV9hbGlnbl9yZXNldCgxODAsIDkwKTtcbiAgICAgICAgfSAvL2FsZXJ0KFwiaGV5XCIpO1xuICAgIH0pO1xuICAgIC8vdXNlciBsb2dpblxuICAgICQoXCIjci1uZXh0YlwiKS5jbGljayhmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmIChidXR0b25fZmxhZykge1xuICAgICAgICAgICAgYnV0dG9uX2ZsYWcgPSBmYWxzZTtcbiAgICAgICAgICAgIHJvdGF0ZV9hbGlnbl9yZXNldCgxODAsIC05MCk7XG4gICAgICAgIH1cbiAgICB9KTtcbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vICAgIFxuICAgIC8vcmlnaHRcbiAgICAkKFwiI3ItYmFja3ZcIikuY2xpY2soZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAoYnV0dG9uX2ZsYWcpIHtcbiAgICAgICAgICAgIGJ1dHRvbl9mbGFnID0gZmFsc2U7XG4gICAgICAgICAgICByb3RhdGVfYWxpZ25fcmVzZXQoMjcwLCA5MCk7XG4gICAgICAgIH1cbiAgICB9KTtcbiAgICAvL3VzZXIgbG9naW5cbiAgICAkKFwiI3ItbmV4dHZcIikuY2xpY2soZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAoYnV0dG9uX2ZsYWcpIHtcbiAgICAgICAgICAgIGJ1dHRvbl9mbGFnID0gZmFsc2U7XG4gICAgICAgICAgICByb3RhdGVfYWxpZ25fcmVzZXQoMjcwLCAtOTApO1xuICAgICAgICB9XG4gICAgfSk7XG4gICAgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLyBMRUZUIFBBTkVMIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG4gICAgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLyBCQUNLICBQQU5FTCAgQUREUkVTUyAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLyAgXG4gICAgLy9jb250YWN0XG4gICAgdGVzdF9mcHMoKTtcbiAgICBtYWtlX2N1YmUoKTtcbiAgICAvLyBhZGp1c3QoKTtcbiAgICBrZXlib2FyZCgpO1xuICAgICQod2luZG93KS5yZXNpemUoZnVuY3Rpb24gKCkgXG4gICAge1xuICAgICAgICAvLyBhbGVydChcIlRlc3RcIik7XG4gICAgICAgIG1ha2VfY3ViZSgpO1xuICAgIH0pO1xuICBcbn0pO1xuXG5cbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8gRlJPTlQgUEFORUwgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8gIFxuICAgICAgIGhpZGVfY3ViZSh0cnVlKTtcbn0pO1xuXG5mdW5jdGlvbiBoaWRlX2N1YmUoaGlkZSkge1xuICAgIGlmIChoaWRlID09IHRydWUpIHtcbiAgICAgICAgJChcIiN0b3BcIikuY3NzKFwidmlzaWJpbGl0eVwiLCBcImhpZGRlblwiKTtcbiAgICAgICAgJChcIiNib3R0b21cIikuY3NzKFwidmlzaWJpbGl0eVwiLCBcImhpZGRlblwiKTtcbiAgICAgICAgJChcIiNsZWZ0XCIpLmNzcyhcInZpc2liaWxpdHlcIiwgXCJoaWRkZW5cIik7XG4gICAgICAgICQoXCIjcmlnaHRcIikuY3NzKFwidmlzaWJpbGl0eVwiLCBcImhpZGRlblwiKTtcbiAgICAgICAgJChcIiNmcm9udFwiKS5jc3MoXCJ2aXNpYmlsaXR5XCIsIFwiaGlkZGVuXCIpO1xuICAgICAgICAkKFwiI2JhY2tcIikuY3NzKFwidmlzaWJpbGl0eVwiLCBcImhpZGRlblwiKTtcbiAgICAgICAgJChcIiNzaGFkb3dcIikuY3NzKFwidmlzaWJpbGl0eVwiLCBcImhpZGRlblwiKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICAkKFwiI3RvcFwiKS5jc3MoXCJ2aXNpYmlsaXR5XCIsIFwidmlzaWJsZVwiKTtcbiAgICAgICAgJChcIiNib3R0b21cIikuY3NzKFwidmlzaWJpbGl0eVwiLCBcInZpc2libGVcIik7XG4gICAgICAgICQoXCIjbGVmdFwiKS5jc3MoXCJ2aXNpYmlsaXR5XCIsIFwidmlzaWJsZVwiKTtcbiAgICAgICAgJChcIiNyaWdodFwiKS5jc3MoXCJ2aXNpYmlsaXR5XCIsIFwidmlzaWJsZVwiKTtcbiAgICAgICAgJChcIiNmcm9udFwiKS5jc3MoXCJ2aXNpYmlsaXR5XCIsIFwidmlzaWJsZVwiKTtcbiAgICAgICAgJChcIiNiYWNrXCIpLmNzcyhcInZpc2liaWxpdHlcIiwgXCJ2aXNpYmxlXCIpO1xuICAgICAgICAkKFwiI3NoYWRvd1wiKS5jc3MoXCJ2aXNpYmlsaXR5XCIsIFwidmlzaWJsZVwiKTtcbiAgICB9XG59XG5cbmZ1bmN0aW9uIG1ha2VfY3ViZSgpIHtcbiAgICBoaWRlX2N1YmUodHJ1ZSk7XG4gICAgaWYgKCQod2luZG93KS53aWR0aCgpID49IE1PQklMRV9XSURUSF9CUkVBS19ET1dOKSB7XG4gICAgICAgIGFkanVzdF9mdWxsX3dpZHRoKCk7XG4gICAgICAgICAvLyBhbGVydChcImRlc2t0b3BcIik7XG4gICAgfSBlbHNlIHtcbiAgICAgICAvLyAgYWxlcnQoXCJtb2JpbGVcIik7XG4gICAgICAgIGFkanVzdF9tb2JpbGVfd2lkdGgoKTtcbiAgICB9XG4gICAgaGlkZV9jdWJlKGZhbHNlKTtcbn1cblxuZnVuY3Rpb24gYWRqdXN0X21vYmlsZV93aWR0aCgpIHtcbiAgICBkZXB0aCA9IHdpZHRoID0gTWF0aC5mbG9vcigkKHdpbmRvdykud2lkdGgoKSAqIFZJRVdQT1JUX1dJRFRIX0ZBQ1RfTU9CKTtcbiAgICBoZWlnaHQgPSBNYXRoLmZsb29yKCQoXCIjZm9ybV9jb250YWludGVyXCIpLm91dGVySGVpZ2h0KCkgKyAzMCk7XG4gICAgJChcIiN2aWV3cG9ydFwiKS5jc3MoXCJ3aWR0aFwiLCB3aWR0aCk7XG4gICAgJChcIiN2aWV3cG9ydFwiKS5jc3MoXCJoZWlnaHRcIiwgaGVpZ2h0KTtcbiAgICAkKFwiI3ZpZXdwb3J0XCIpLmFkZENsYXNzKFwiY2VudGVyX3ZpZXdwb3J0XCIpO1xuICAgIFxuICAgIFxuICAgIFxuICAgIC8vd2lkdGggPSBoZWlnaHQgPSBkZXB0aCA9IERFRkFVTFRfV0lEVEg7XG4gICAgdmFyIGhhbGZfZGVwdGggPSBNYXRoLmZsb29yKGRlcHRoIC8gMik7XG4gICAgdmFyIGhhbGZfd2lkdGggPSBNYXRoLmZsb29yKHdpZHRoIC8gMik7XG4gICAgdmFyIGhhbGZfaGVpZ2h0ID0gTWF0aC5mbG9vcihoZWlnaHQgLyAyKTtcbiAgICAvLyAkKFwiI3ZpZXdwb3J0XCIpLmNzcyhcInRvcFwiLCBNYXRoLmZsb29yKCQoZG9jdW1lbnQpLmhlaWdodCgpIC8gMiApKTtcbiAgICAvLyAkKFwiI3ZpZXdwb3J0XCIpLmNzcyhcImxlZnRcIiwgTWF0aC5mbG9vcigkKGRvY3VtZW50KS53aWR0aCgpIC8gMiApKTtcbiAgICAvKlxuICAgICAkKFwiI3ZpZXdwb3J0XCIpLmNzcyhcInRvcFwiLCBNYXRoLmZsb29yKCQoZG9jdW1lbnQpLmhlaWdodCgpIC8gMiAtIGhhbGZfaGVpZ2h0KSk7XG4gICAgICQoXCIjdmlld3BvcnRcIikuY3NzKFwibGVmdFwiLCBNYXRoLmZsb29yKCQoZG9jdW1lbnQpLndpZHRoKCkgLyAyIC0gaGFsZl93aWR0aCkpO1xuICAgICAqL1xuICAgIGFkanVzdF9zaGFkb3dfb2Zmc2V0ID0gaGFsZl9oZWlnaHQgKyBzaGFkb3dfb2Zmc2V0O1xuICAgIGZyb250X3RyYW5zbGF0ZSA9ICd0cmFuc2xhdGVaKCcgKyBoYWxmX2RlcHRoICsgJ3B4KSc7XG4gICAgYmFja190cmFuc2xhdGUgPSAndHJhbnNsYXRlWignICsgKC1oYWxmX2RlcHRoKSArICdweCknICsgJyAnICsgJ3JvdGF0ZVkoMTgwZGVnKSc7XG4gICAgdG9wX3RyYW5zbGF0ZSA9ICd0cmFuc2xhdGVZKCcgKyAoLWhhbGZfaGVpZ2h0KSArICdweCknICsgJyAnICsgJ3JvdGF0ZVgoOTBkZWcpJztcbiAgICBib3R0b21fdHJhbnNsYXRlID0gJ3RyYW5zbGF0ZVkoJyArIGhhbGZfaGVpZ2h0ICsgJ3B4KScgKyAnICcgKyAncm90YXRlWCgtOTBkZWcpJztcbiAgICBsZWZ0X3RyYW5zbGF0ZSA9ICd0cmFuc2xhdGVYKCcgKyAoLWhhbGZfd2lkdGgpICsgJ3B4KScgKyAnICcgKyAncm90YXRlWSgtOTBkZWcpJztcbiAgICByaWdodF90cmFuc2xhdGUgPSAndHJhbnNsYXRlWCgnICsgaGFsZl93aWR0aCArICdweCknICsgJyAnICsgJ3JvdGF0ZVkoOTBkZWcpJztcbiAgICBzaGRvd190cmFuc2xhdGUgPSAndHJhbnNsYXRlWSgnICsgYWRqdXN0X3NoYWRvd19vZmZzZXQgKyAncHgpJyArICcgJyArICdyb3RhdGVYKC05MGRlZyknO1xuICAgIGFkanVzdF92aWV3ID0gJ3RyYW5zbGF0ZVooJyArICgtaGFsZl9kZXB0aCkgKyAncHgpJztcbiAgICAkKFwiI2N1YmVCXCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogYWRqdXN0X3ZpZXdcbiAgICB9KTtcbiAgICAkKFwiI2Zyb250XCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogZnJvbnRfdHJhbnNsYXRlXG4gICAgfSk7XG4gICAgJChcIiNiYWNrXCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogYmFja190cmFuc2xhdGVcbiAgICB9KTtcbiAgICAkKFwiI3RvcFwiKS5jc3Moe1xuICAgICAgICB0cmFuc2Zvcm06IHRvcF90cmFuc2xhdGVcbiAgICB9KTtcbiAgICAkKFwiI2JvdHRvbVwiKS5jc3Moe1xuICAgICAgICB0cmFuc2Zvcm06IGJvdHRvbV90cmFuc2xhdGVcbiAgICB9KTtcbiAgICAkKFwiI2xlZnRcIikuY3NzKHtcbiAgICAgICAgdHJhbnNmb3JtOiBsZWZ0X3RyYW5zbGF0ZVxuICAgIH0pO1xuICAgICQoXCIjcmlnaHRcIikuY3NzKHtcbiAgICAgICAgdHJhbnNmb3JtOiByaWdodF90cmFuc2xhdGVcbiAgICB9KTtcbiAgICAkKFwiI3NoYWRvd1wiKS5jc3Moe1xuICAgICAgICB0cmFuc2Zvcm06IHNoZG93X3RyYW5zbGF0ZVxuICAgIH0pO1xuICAgIC8vIHZhciBoZWlnaHRfY29udGVudCA9IFxuICAgICQoXCIjdG9wXCIpLmNzcyhcImhlaWdodFwiLCB3aWR0aCk7XG4gICAgJChcIiNib3R0b21cIikuY3NzKFwiaGVpZ2h0XCIsIHdpZHRoKTtcbiAgICAkKFwiI3NoYWRvd1wiKS5jc3MoXCJoZWlnaHRcIiwgd2lkdGgpO1xuICAgIHZhciBhZGp1c3RfdG9wID0gTWF0aC5mbG9vcigtd2lkdGggLyAyKTtcbiAgICB2YXIgYWRqdXN0X2JvdHRvbSA9IE1hdGguZmxvb3IoaGVpZ2h0IC0gd2lkdGggLyAyKTtcbiAgICB0b3BfdHJhbnNsYXRlID0gJ3RyYW5zbGF0ZVkoJyArIChhZGp1c3RfdG9wKSArICdweCknICsgJyAnICsgJ3JvdGF0ZVgoOTBkZWcpJztcbiAgICBib3R0b21fdHJhbnNsYXRlID0gJ3RyYW5zbGF0ZVkoJyArIGFkanVzdF9ib3R0b20gKyAncHgpJyArICcgJyArICdyb3RhdGVYKC05MGRlZyknO1xuICAgICQoXCIjdG9wXCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogdG9wX3RyYW5zbGF0ZVxuICAgIH0pO1xuICAgIC8vICQoXCIjdG9wXCIpLmNzcyhcImhlaWdodFwiLCB3aWR0aCk7XG4gICAgYWRqdXN0X3NoYWRvd19vZmZzZXQgPSBhZGp1c3RfYm90dG9tICsgc2hhZG93X29mZnNldDtcbiAgICBzaGRvd190cmFuc2xhdGUgPSAndHJhbnNsYXRlWSgnICsgYWRqdXN0X3NoYWRvd19vZmZzZXQgKyAncHgpJyArICcgJyArICdyb3RhdGVYKC05MGRlZyknO1xuICAgICQoXCIjYm90dG9tXCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogYm90dG9tX3RyYW5zbGF0ZVxuICAgIH0pO1xuICAgICQoXCIjc2hhZG93XCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogc2hkb3dfdHJhbnNsYXRlXG4gICAgfSk7XG59XG5mdW5jdGlvbiBhZGp1c3RfbW9iaWxlX3dpZHRoWCgpIHtcbiAgICBkZXB0aCA9IHdpZHRoID0gTWF0aC5mbG9vcigkKHdpbmRvdykud2lkdGgoKSAqIFZJRVdQT1JUX1dJRFRIX0ZBQ1RfTU9CKTtcbiAgICBoZWlnaHQgPSBNYXRoLmZsb29yKCQoXCIjZm9ybV9jb250YWludGVyXCIpLm91dGVySGVpZ2h0KCkgKyAzMCk7XG4gICAgJChcIiN2aWV3cG9ydFwiKS5jc3MoXCJ3aWR0aFwiLCB3aWR0aCk7XG4gICAgJChcIiN2aWV3cG9ydFwiKS5jc3MoXCJoZWlnaHRcIiwgaGVpZ2h0KTtcbiAgICAkKFwiI3ZpZXdwb3J0XCIpLmFkZENsYXNzKFwiY2VudGVyX3ZpZXdwb3J0XCIpO1xuICAgIFxuICAgIFxuICAgIFxuICAgIC8vd2lkdGggPSBoZWlnaHQgPSBkZXB0aCA9IERFRkFVTFRfV0lEVEg7XG4gICAgdmFyIGhhbGZfZGVwdGggPSBNYXRoLmZsb29yKGRlcHRoIC8gMik7XG4gICAgdmFyIGhhbGZfd2lkdGggPSBNYXRoLmZsb29yKHdpZHRoIC8gMik7XG4gICAgdmFyIGhhbGZfaGVpZ2h0ID0gTWF0aC5mbG9vcihoZWlnaHQgLyAyKTtcbiAgICAvLyAkKFwiI3ZpZXdwb3J0XCIpLmNzcyhcInRvcFwiLCBNYXRoLmZsb29yKCQoZG9jdW1lbnQpLmhlaWdodCgpIC8gMiApKTtcbiAgICAvLyAkKFwiI3ZpZXdwb3J0XCIpLmNzcyhcImxlZnRcIiwgTWF0aC5mbG9vcigkKGRvY3VtZW50KS53aWR0aCgpIC8gMiApKTtcbiAgICAvKlxuICAgICAkKFwiI3ZpZXdwb3J0XCIpLmNzcyhcInRvcFwiLCBNYXRoLmZsb29yKCQoZG9jdW1lbnQpLmhlaWdodCgpIC8gMiAtIGhhbGZfaGVpZ2h0KSk7XG4gICAgICQoXCIjdmlld3BvcnRcIikuY3NzKFwibGVmdFwiLCBNYXRoLmZsb29yKCQoZG9jdW1lbnQpLndpZHRoKCkgLyAyIC0gaGFsZl93aWR0aCkpO1xuICAgICAqL1xuICAgIGFkanVzdF9zaGFkb3dfb2Zmc2V0ID0gaGFsZl9oZWlnaHQgKyBzaGFkb3dfb2Zmc2V0O1xuICAgIGZyb250X3RyYW5zbGF0ZSA9ICd0cmFuc2xhdGVaKCcgKyBoYWxmX2RlcHRoICsgJ3B4KSc7XG4gICAgYmFja190cmFuc2xhdGUgPSAndHJhbnNsYXRlWignICsgKC1oYWxmX2RlcHRoKSArICdweCknICsgJyAnICsgJ3JvdGF0ZVkoMTgwZGVnKSc7XG4gICAgdG9wX3RyYW5zbGF0ZSA9ICd0cmFuc2xhdGVZKCcgKyAoLWhhbGZfaGVpZ2h0KSArICdweCknICsgJyAnICsgJ3JvdGF0ZVgoOTBkZWcpJztcbiAgICBib3R0b21fdHJhbnNsYXRlID0gJ3RyYW5zbGF0ZVkoJyArIGhhbGZfaGVpZ2h0ICsgJ3B4KScgKyAnICcgKyAncm90YXRlWCgtOTBkZWcpJztcbiAgICBsZWZ0X3RyYW5zbGF0ZSA9ICd0cmFuc2xhdGVYKCcgKyAoLWhhbGZfd2lkdGgpICsgJ3B4KScgKyAnICcgKyAncm90YXRlWSgtOTBkZWcpJztcbiAgICByaWdodF90cmFuc2xhdGUgPSAndHJhbnNsYXRlWCgnICsgaGFsZl93aWR0aCArICdweCknICsgJyAnICsgJ3JvdGF0ZVkoOTBkZWcpJztcbiAgICBzaGRvd190cmFuc2xhdGUgPSAndHJhbnNsYXRlWSgnICsgYWRqdXN0X3NoYWRvd19vZmZzZXQgKyAncHgpJyArICcgJyArICdyb3RhdGVYKC05MGRlZyknO1xuICAgIGFkanVzdF92aWV3ID0gJ3RyYW5zbGF0ZVooJyArICgtaGFsZl9kZXB0aCkgKyAncHgpJztcbiAgICAkKFwiI2N1YmVCXCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogYWRqdXN0X3ZpZXdcbiAgICB9KTtcbiAgICAkKFwiI2Zyb250XCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogZnJvbnRfdHJhbnNsYXRlXG4gICAgfSk7XG4gICAgJChcIiNiYWNrXCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogYmFja190cmFuc2xhdGVcbiAgICB9KTtcbiAgICAkKFwiI3RvcFwiKS5jc3Moe1xuICAgICAgICB0cmFuc2Zvcm06IHRvcF90cmFuc2xhdGVcbiAgICB9KTtcbiAgICAkKFwiI2JvdHRvbVwiKS5jc3Moe1xuICAgICAgICB0cmFuc2Zvcm06IGJvdHRvbV90cmFuc2xhdGVcbiAgICB9KTtcbiAgICAkKFwiI2xlZnRcIikuY3NzKHtcbiAgICAgICAgdHJhbnNmb3JtOiBsZWZ0X3RyYW5zbGF0ZVxuICAgIH0pO1xuICAgICQoXCIjcmlnaHRcIikuY3NzKHtcbiAgICAgICAgdHJhbnNmb3JtOiByaWdodF90cmFuc2xhdGVcbiAgICB9KTtcbiAgICAkKFwiI3NoYWRvd1wiKS5jc3Moe1xuICAgICAgICB0cmFuc2Zvcm06IHNoZG93X3RyYW5zbGF0ZVxuICAgIH0pO1xuICAgIC8vIHZhciBoZWlnaHRfY29udGVudCA9IFxuICAgICQoXCIjdG9wXCIpLmNzcyhcImhlaWdodFwiLCB3aWR0aCk7XG4gICAgJChcIiNib3R0b21cIikuY3NzKFwiaGVpZ2h0XCIsIHdpZHRoKTtcbiAgICAkKFwiI3NoYWRvd1wiKS5jc3MoXCJoZWlnaHRcIiwgd2lkdGgpO1xuICAgIHZhciBhZGp1c3RfdG9wID0gTWF0aC5mbG9vcigtd2lkdGggLyAyKTtcbiAgICB2YXIgYWRqdXN0X2JvdHRvbSA9IE1hdGguZmxvb3IoaGVpZ2h0IC0gd2lkdGggLyAyKTtcbiAgICB0b3BfdHJhbnNsYXRlID0gJ3RyYW5zbGF0ZVkoJyArIChhZGp1c3RfdG9wKSArICdweCknICsgJyAnICsgJ3JvdGF0ZVgoOTBkZWcpJztcbiAgICBib3R0b21fdHJhbnNsYXRlID0gJ3RyYW5zbGF0ZVkoJyArIGFkanVzdF9ib3R0b20gKyAncHgpJyArICcgJyArICdyb3RhdGVYKC05MGRlZyknO1xuICAgICQoXCIjdG9wXCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogdG9wX3RyYW5zbGF0ZVxuICAgIH0pO1xuICAgIC8vICQoXCIjdG9wXCIpLmNzcyhcImhlaWdodFwiLCB3aWR0aCk7XG4gICAgYWRqdXN0X3NoYWRvd19vZmZzZXQgPSBhZGp1c3RfYm90dG9tICsgc2hhZG93X29mZnNldDtcbiAgICBzaGRvd190cmFuc2xhdGUgPSAndHJhbnNsYXRlWSgnICsgYWRqdXN0X3NoYWRvd19vZmZzZXQgKyAncHgpJyArICcgJyArICdyb3RhdGVYKC05MGRlZyknO1xuICAgICQoXCIjYm90dG9tXCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogYm90dG9tX3RyYW5zbGF0ZVxuICAgIH0pO1xuICAgICQoXCIjc2hhZG93XCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogc2hkb3dfdHJhbnNsYXRlXG4gICAgfSk7XG59XG5cbmZ1bmN0aW9uIGFkanVzdF9mdWxsX3dpZHRoKCkge1xuICAgIHdpZHRoID0gaGVpZ2h0ID0gZGVwdGggPSBNYXRoLmZsb29yKCQoXCIjZm9ybV9jb250YWludGVyXCIpLm91dGVySGVpZ2h0KCkgKyAzMCk7XG4gICAgXG4gICAgICQoXCIjdmlld3BvcnRcIikuY3NzKFwid2lkdGhcIiwgd2lkdGgpO1xuICAgICQoXCIjdmlld3BvcnRcIikuY3NzKFwiaGVpZ2h0XCIsIGhlaWdodCk7XG4gICAgJChcIiN2aWV3cG9ydFwiKS5hZGRDbGFzcyhcImNlbnRlcl92aWV3cG9ydFwiKTtcbiAgICAvL3dpZHRoID0gaGVpZ2h0ID0gZGVwdGggPSBERUZBVUxUX1dJRFRIO1xuICAgIHZhciBoYWxmX2RlcHRoID0gTWF0aC5mbG9vcihkZXB0aCAvIDIpO1xuICAgIHZhciBoYWxmX3dpZHRoID0gTWF0aC5mbG9vcih3aWR0aCAvIDIpO1xuICAgIHZhciBoYWxmX2hlaWdodCA9IE1hdGguZmxvb3IoaGVpZ2h0IC8gMik7XG4gICAgLy8gJChcIiN2aWV3cG9ydFwiKS5jc3MoXCJ0b3BcIiwgTWF0aC5mbG9vcigkKGRvY3VtZW50KS5oZWlnaHQoKSAvIDIgKSk7XG4gICAgLy8gJChcIiN2aWV3cG9ydFwiKS5jc3MoXCJsZWZ0XCIsIE1hdGguZmxvb3IoJChkb2N1bWVudCkud2lkdGgoKSAvIDIgKSk7XG4gICAgLypcbiAgICAgJChcIiN2aWV3cG9ydFwiKS5jc3MoXCJ0b3BcIiwgTWF0aC5mbG9vcigkKGRvY3VtZW50KS5oZWlnaHQoKSAvIDIgLSBoYWxmX2hlaWdodCkpO1xuICAgICAkKFwiI3ZpZXdwb3J0XCIpLmNzcyhcImxlZnRcIiwgTWF0aC5mbG9vcigkKGRvY3VtZW50KS53aWR0aCgpIC8gMiAtIGhhbGZfd2lkdGgpKTtcbiAgICAgKi9cbiAgICBhZGp1c3Rfc2hhZG93X29mZnNldCA9IGhhbGZfaGVpZ2h0ICsgc2hhZG93X29mZnNldDtcbiAgICBmcm9udF90cmFuc2xhdGUgPSAndHJhbnNsYXRlWignICsgaGFsZl9kZXB0aCArICdweCknO1xuICAgIGJhY2tfdHJhbnNsYXRlID0gJ3RyYW5zbGF0ZVooJyArICgtaGFsZl9kZXB0aCkgKyAncHgpJyArICcgJyArICdyb3RhdGVZKDE4MGRlZyknO1xuICAgIHRvcF90cmFuc2xhdGUgPSAndHJhbnNsYXRlWSgnICsgKC1oYWxmX2hlaWdodCkgKyAncHgpJyArICcgJyArICdyb3RhdGVYKDkwZGVnKSc7XG4gICAgYm90dG9tX3RyYW5zbGF0ZSA9ICd0cmFuc2xhdGVZKCcgKyBoYWxmX2hlaWdodCArICdweCknICsgJyAnICsgJ3JvdGF0ZVgoLTkwZGVnKSc7XG4gICAgbGVmdF90cmFuc2xhdGUgPSAndHJhbnNsYXRlWCgnICsgKC1oYWxmX3dpZHRoKSArICdweCknICsgJyAnICsgJ3JvdGF0ZVkoLTkwZGVnKSc7XG4gICAgcmlnaHRfdHJhbnNsYXRlID0gJ3RyYW5zbGF0ZVgoJyArIGhhbGZfd2lkdGggKyAncHgpJyArICcgJyArICdyb3RhdGVZKDkwZGVnKSc7XG4gICAgc2hkb3dfdHJhbnNsYXRlID0gJ3RyYW5zbGF0ZVkoJyArIGFkanVzdF9zaGFkb3dfb2Zmc2V0ICsgJ3B4KScgKyAnICcgKyAncm90YXRlWCgtOTBkZWcpJztcbiAgICBhZGp1c3RfdmlldyA9ICd0cmFuc2xhdGVaKCcgKyAoLWhhbGZfZGVwdGgpICsgJ3B4KSc7XG4gICAgJChcIiNjdWJlQlwiKS5jc3Moe1xuICAgICAgICB0cmFuc2Zvcm06IGFkanVzdF92aWV3XG4gICAgfSk7XG4gICAgJChcIiNmcm9udFwiKS5jc3Moe1xuICAgICAgICB0cmFuc2Zvcm06IGZyb250X3RyYW5zbGF0ZVxuICAgIH0pO1xuICAgICQoXCIjYmFja1wiKS5jc3Moe1xuICAgICAgICB0cmFuc2Zvcm06IGJhY2tfdHJhbnNsYXRlXG4gICAgfSk7XG4gICAgLypcbiAgICAkKFwiI3RvcFwiKS5jc3Moe1xuICAgICAgICB0cmFuc2Zvcm06IHRvcF90cmFuc2xhdGVcbiAgICB9KTtcbiAgICAkKFwiI2JvdHRvbVwiKS5jc3Moe1xuICAgICAgICB0cmFuc2Zvcm06IGJvdHRvbV90cmFuc2xhdGVcbiAgICB9KTsqL1xuICAgICQoXCIjbGVmdFwiKS5jc3Moe1xuICAgICAgICB0cmFuc2Zvcm06IGxlZnRfdHJhbnNsYXRlXG4gICAgfSk7XG4gICAgJChcIiNyaWdodFwiKS5jc3Moe1xuICAgICAgICB0cmFuc2Zvcm06IHJpZ2h0X3RyYW5zbGF0ZVxuICAgIH0pO1xuICAgIC8qXG4gICAgJChcIiNzaGFkb3dcIikuY3NzKHtcbiAgICAgICAgdHJhbnNmb3JtOiBzaGRvd190cmFuc2xhdGVcbiAgICB9KTsqL1xuICAgIC8vIHZhciBoZWlnaHRfY29udGVudCA9IFxuICAgIFxuICAgICAgIC8vIHZhciBoZWlnaHRfY29udGVudCA9IFxuICAgICQoXCIjdG9wXCIpLmNzcyhcImhlaWdodFwiLCB3aWR0aCk7XG4gICAgJChcIiNib3R0b21cIikuY3NzKFwiaGVpZ2h0XCIsIHdpZHRoKTtcbiAgICAkKFwiI3NoYWRvd1wiKS5jc3MoXCJoZWlnaHRcIiwgd2lkdGgpO1xuICAgIHZhciBhZGp1c3RfdG9wID0gTWF0aC5mbG9vcigtd2lkdGggLyAyKTtcbiAgICB2YXIgYWRqdXN0X2JvdHRvbSA9IE1hdGguZmxvb3IoaGVpZ2h0IC0gd2lkdGggLyAyKTtcbiAgICB0b3BfdHJhbnNsYXRlID0gJ3RyYW5zbGF0ZVkoJyArIChhZGp1c3RfdG9wKSArICdweCknICsgJyAnICsgJ3JvdGF0ZVgoOTBkZWcpJztcbiAgICBib3R0b21fdHJhbnNsYXRlID0gJ3RyYW5zbGF0ZVkoJyArIGFkanVzdF9ib3R0b20gKyAncHgpJyArICcgJyArICdyb3RhdGVYKC05MGRlZyknO1xuICAgICQoXCIjdG9wXCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogdG9wX3RyYW5zbGF0ZVxuICAgIH0pO1xuICAgIC8vICQoXCIjdG9wXCIpLmNzcyhcImhlaWdodFwiLCB3aWR0aCk7XG4gICAgYWRqdXN0X3NoYWRvd19vZmZzZXQgPSBhZGp1c3RfYm90dG9tICsgc2hhZG93X29mZnNldDtcbiAgICBzaGRvd190cmFuc2xhdGUgPSAndHJhbnNsYXRlWSgnICsgYWRqdXN0X3NoYWRvd19vZmZzZXQgKyAncHgpJyArICcgJyArICdyb3RhdGVYKC05MGRlZyknO1xuICAgICQoXCIjYm90dG9tXCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogYm90dG9tX3RyYW5zbGF0ZVxuICAgIH0pO1xuICAgICQoXCIjc2hhZG93XCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogc2hkb3dfdHJhbnNsYXRlXG4gICAgfSk7XG59XG5cbmZ1bmN0aW9uIHRlc3RfZnBzKCkge1xuICAgIHZhciByb3RfWCA9ICdyb3RhdGVYKCcgKyBjdXJyZW50X1ggKyAnZGVnKSc7XG4gICAgJChcIiNjdWJlXCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogcm90X1hcbiAgICB9KTtcbiAgICAkKFwiI1hcIikuaHRtbChjdXJyZW50X1gpO1xufVxuXG5mdW5jdGlvbiBrZXlib2FyZCgpIHtcbiAgICAvLyB5b3VyIGJhc2UuIEknbSBpbiBpdFxuICAgICQoZG9jdW1lbnQpLmtleWRvd24oZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgLypcbiAgICAgICAgIDM3IC0gbGVmdFxuICAgICAgICAgMzggLSB1cFxuICAgICAgICAgMzkgLSByaWdodFxuICAgICAgICAgNDAgLSBkb3duIFxuICAgICAgICAgKi9cbiAgICAgICAgdmFyIHJvdF9YWTtcbiAgICAgICAgaWYgKGUua2V5Q29kZSA9PSA0MCkge1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJsZWZ0IHByZXNzZWRcIik7XG4gICAgICAgICAgICAvLyBjdXJyZW50X1ggPSBjdXJyZW50X1ggLSBpbmNyZW1lbnQ7XG4gICAgICAgICAgICByb3RfWFkgPSAncm90YXRlWCgnICsgY3VycmVudF9YICsgJ2RlZyknICsgJyAnICsgJ3JvdGF0ZVkoJyArIGN1cnJlbnRfWSArICdkZWcpJztcbiAgICAgICAgICAgICQoXCIjY3ViZVwiKS5jc3Moe1xuICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogcm90X1hZXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICQoXCIjWFwiKS5odG1sKGN1cnJlbnRfWCk7XG4gICAgICAgICAgICAkKFwiI1lcIikuaHRtbChjdXJyZW50X1kpO1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIC8vcmlnaHRcbiAgICAgICAgaWYgKGUua2V5Q29kZSA9PSAzOCkge1xuICAgICAgICAgICAgLy8gICBjdXJyZW50X1ggPSBjdXJyZW50X1ggKyBpbmNyZW1lbnQ7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcInJpZ2h0IHByZXNzZWRcIik7XG4gICAgICAgICAgICByb3RfWFkgPSAncm90YXRlWCgnICsgY3VycmVudF9YICsgJ2RlZyknICsgJyAnICsgJ3JvdGF0ZVkoJyArIGN1cnJlbnRfWSArICdkZWcpJztcbiAgICAgICAgICAgICQoXCIjY3ViZVwiKS5jc3Moe1xuICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogcm90X1hZXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICQoXCIjWFwiKS5odG1sKGN1cnJlbnRfWCk7XG4gICAgICAgICAgICAkKFwiI1lcIikuaHRtbChjdXJyZW50X1kpO1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIGlmIChlLmtleUNvZGUgPT0gMzcpIHtcbiAgICAgICAgICAgIGN1cnJlbnRfWSA9IGN1cnJlbnRfWSAtIGluY3JlbWVudDtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwidXAgcHJlc3NlZFwiKTtcbiAgICAgICAgICAgIHJvdF9YWSA9ICdyb3RhdGVYKCcgKyBjdXJyZW50X1ggKyAnZGVnKScgKyAnICcgKyAncm90YXRlWSgnICsgY3VycmVudF9ZICsgJ2RlZyknO1xuICAgICAgICAgICAgJChcIiNjdWJlXCIpLmNzcyh7XG4gICAgICAgICAgICAgICAgdHJhbnNmb3JtOiByb3RfWFlcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgJChcIiNYXCIpLmh0bWwoY3VycmVudF9YKTtcbiAgICAgICAgICAgICQoXCIjWVwiKS5odG1sKGN1cnJlbnRfWSk7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgLy80MCBkb3duXG4gICAgICAgIGlmIChlLmtleUNvZGUgPT0gMzkpIHtcbiAgICAgICAgICAgIGN1cnJlbnRfWSA9IGN1cnJlbnRfWSArIGluY3JlbWVudDtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiZG93biBwcmVzc2VkXCIpO1xuICAgICAgICAgICAgcm90X1hZID0gJ3JvdGF0ZVgoJyArIGN1cnJlbnRfWCArICdkZWcpJyArICcgJyArICdyb3RhdGVZKCcgKyBjdXJyZW50X1kgKyAnZGVnKSc7XG4gICAgICAgICAgICAkKFwiI2N1YmVcIikuY3NzKHtcbiAgICAgICAgICAgICAgICB0cmFuc2Zvcm06IHJvdF9YWVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAkKFwiI1hcIikuaHRtbChjdXJyZW50X1gpO1xuICAgICAgICAgICAgJChcIiNZXCIpLmh0bWwoY3VycmVudF9ZKTtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuICAgIH0pO1xufVxuXG5cbiIsIiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcblxuLypcbiAkKCcjbG9naW5mb3JtZicpLnZhbGlkYXRlKHsvLyBpbml0aWFsaXplIHRoZSBwbHVnaW5cbiAgICAgICAgcnVsZXM6IHtcblxuICAgICAgICAgICAgZW1haWxfbmFtZTpcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgICAgICBlbWFpbDogdHJ1ZVxuXG5cbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHBhc3N3b3JkX25hbWU6XG4gICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICAgICAgbWlubGVuZ3RoOiAzXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgfSBcbiAgICAgICAgXG4gICAgfSk7XG5cblxuICAgICQoJyNsb2dpbmZvcm1sJykudmFsaWRhdGUoey8vIGluaXRpYWxpemUgdGhlIHBsdWdpblxuICAgICAgICBydWxlczoge1xuXG4gICAgICAgICAgICBlbWFpbF9uYW1lOlxuICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGVtYWlsOiB0cnVlXG5cblxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgcGFzc3dvcmRfbmFtZTpcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW5sZW5ndGg6IDNcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSk7XG5cbiovXG5cbn0pO1xuXG5cbiIsIiIsIndpbmRvdy5jb3VudEZQUyA9IChmdW5jdGlvbiAoKSB7XG4gIHZhciBsYXN0TG9vcCA9IChuZXcgRGF0ZSgpKS5nZXRNaWxsaXNlY29uZHMoKTtcbiAgdmFyIGNvdW50ID0gMTtcbiAgdmFyIGZwcyA9IDA7XG5cbiAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgY3VycmVudExvb3AgPSAobmV3IERhdGUoKSkuZ2V0TWlsbGlzZWNvbmRzKCk7XG4gICAgaWYgKGxhc3RMb29wID4gY3VycmVudExvb3ApIHtcbiAgICAgIGZwcyA9IGNvdW50O1xuICAgICAgY291bnQgPSAxO1xuICAgIH0gZWxzZSB7XG4gICAgICBjb3VudCArPSAxO1xuICAgIH1cbiAgICBsYXN0TG9vcCA9IGN1cnJlbnRMb29wO1xuICAgIHJldHVybiBmcHM7XG4gIH07XG59KCkpO1xuXG52YXIgJG91dCA9ICQoJyNGUFMnKTtcbihmdW5jdGlvbiBsb29wKCkge1xuICAgIHJlcXVlc3RBbmltYXRpb25GcmFtZShmdW5jdGlvbiAoKSB7XG4gICAgICAkb3V0Lmh0bWwoY291bnRGUFMoKSk7XG4gICAgICBsb29wKCk7XG4gICAgfSk7XG59KCkpOyJdfQ==

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFhX2N1YmUvYWFfY3ViZS5qcyIsImFhX2N1YmUvYWJfdmFsaWRhdGlvbi5qcyIsImFiX3N0YXR1cy9zdGF0cy5qcyIsImFiX3N0YXR1cy96RlBTLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUN2ZUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQzlDQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImFiX2xvZ2luX3BhZ2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgREVGQVVMVF9XSURUSCA9IDQyMDtcbnZhciBWSUVXUE9SVF9XSURUSF9GQUNUX01PQiA9IDAuODtcbnZhciBWSUVXUE9SVF9IRUlHSFRfRkFDVF9NT0IgPSAwLjc7XG52YXIgVklFV1BPUlRfSEVJR0hUX0ZBQ1QgPSAwLjc7XG52YXIgTU9CSUxFX1dJRFRIX0JSRUFLX0RPV04gPSA0OTg7XG52YXIgYWRqdXN0X3NoYWRvd19vZmZzZXQ7XG52YXIgY3VycmVudF9YID0gLTE7XG52YXIgY3VycmVudF9ZID0gMDtcbnZhciBpbmNyZW1lbnQgPSAxO1xudmFyIHdpZHRoX2hlaWdodDtcbnZhciBhZGp1c3RfdG9wO1xudmFyIGFkanVzdF9ib3R0b207XG52YXIgaGVpZ2h0O1xudmFyIHdpZHRoO1xudmFyIGRlcHRoO1xudmFyIEhFSUdIVF9XSURUSF9SQVRJT19NT0JJTEUgPSAxLjQ7XG52YXIgSEVJR0hUX1dJRFRIX1JBVElPID0gMS40O1xudmFyIHNoYWRvd19vZmZzZXQgPSA0MDtcbnZhciBmcm9udF90cmFuc2xhdGU7XG52YXIgYmFja190cmFuc2xhdGU7XG52YXIgdG9wX3RyYW5zbGF0ZTtcbnZhciBib3R0b21fdHJhbnNsYXRlO1xudmFyIGxlZnRfdHJhbnNsYXRlO1xudmFyIHJpZ2h0X3RyYW5zbGF0ZTtcbnZhciBhbmdsZSA9IDA7XG52YXIgdHJhbnNpdGlvbkV2ZW50O1xudmFyIGJ1dHRvbl9mbGFnO1xuXG5mdW5jdGlvbiB3aGljaFRyYW5zaXRpb25FdmVudCgpIHtcbiAgICB2YXIgdCwgZWwgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZmFrZWVsZW1lbnRcIik7XG4gICAgdmFyIHRyYW5zaXRpb25zID0ge1xuICAgICAgICBcInRyYW5zaXRpb25cIjogXCJ0cmFuc2l0aW9uZW5kXCIsXG4gICAgICAgIFwiT1RyYW5zaXRpb25cIjogXCJvVHJhbnNpdGlvbkVuZFwiLFxuICAgICAgICBcIk1velRyYW5zaXRpb25cIjogXCJ0cmFuc2l0aW9uZW5kXCIsXG4gICAgICAgIFwiV2Via2l0VHJhbnNpdGlvblwiOiBcIndlYmtpdFRyYW5zaXRpb25FbmRcIlxuICAgIH07XG4gICAgZm9yICh0IGluIHRyYW5zaXRpb25zKSB7XG4gICAgICAgIGlmIChlbC5zdHlsZVt0XSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICByZXR1cm4gdHJhbnNpdGlvbnNbdF07XG4gICAgICAgIH1cbiAgICB9XG59XG4vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vIFxuZnVuY3Rpb24gcm90YXRlX2FsaWduX3Jlc2V0KGFuZ2xlX3ZhbF9yZXNldCwgcm90YXRlKSB7XG4gICAgJChcIiNjdWJlUlwiKS5yZW1vdmVDbGFzcygncm90YXRlX3RyYW5zaXRpb24nKTtcbiAgICByb3RfWSA9ICdyb3RhdGVZKCcgKyAwICsgJ2RlZyknO1xuICAgICQoXCIjY3ViZVJcIikuY3NzKHtcbiAgICAgICAgdHJhbnNmb3JtOiByb3RfWVxuICAgIH0pO1xuICAgIHJvdF9ZID0gJ3JvdGF0ZVkoJyArIGFuZ2xlX3ZhbF9yZXNldCArICdkZWcpJztcbiAgICAkKFwiI2N1YmVcIikuY3NzKHtcbiAgICAgICAgdHJhbnNmb3JtOiByb3RfWVxuICAgIH0pLnByb21pc2UoKS5kb25lKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICByb3RhdGVfYWxpZ25fcmVzZXRDKHJvdGF0ZSk7XG4gICAgICAgIH0sIDIwMCk7XG4gICAgfSk7XG59XG5cbmZ1bmN0aW9uIHJvdGF0ZV9hbGlnbl9yZXNldEMocm90YXRlKSB7XG4gICAgJChcIiNjdWJlUlwiKS5hZGRDbGFzcyhcInJvdGF0ZV90cmFuc2l0aW9uXCIpO1xuICAgIHZhciByb3RfWSA9ICdyb3RhdGVZKCcgKyByb3RhdGUgKyAnZGVnKSc7XG4gICAgJChcIiNjdWJlUlwiKS5jc3Moe1xuICAgICAgICB0cmFuc2Zvcm06IHJvdF9ZXG4gICAgfSk7XG4gICAgJChcIiNjdWJlUlwiKS5vbmUodHJhbnNpdGlvbkV2ZW50LCBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgLy9hbGVydChcIlRoZSB0cmFuc2l0aW9uIGhhcyBlbmRlZCFcIik7IFxuICAgICAgICBidXR0b25fZmxhZyA9IHRydWU7XG4gICAgICAgIC8vICAkKFwiI2N1YmVcIikuYWRkQ2xhc3MoXCJyb3RhdGVfdHJhbnNpdGlvblwiKTtcbiAgICB9KTtcbn1cblxuZnVuY3Rpb24gcm90YXRlX2FsaWduX3Jlc2V0aShhbmdsZV92YWxfcmVzZXQsIHJvdGF0ZSkge1xuICAgIHJvdGF0ZV9hbGlnbl9yZXNldFIoYW5nbGVfdmFsX3Jlc2V0KTtcbiAgICAvLyAgIHJvdGF0ZV9hbGlnbl9yZXNldEMocm90YXRlKTtcbn1cblxuLy8gLmxvYWQoKSBtZXRob2QgZGVwcmVjYXRlZCBmcm9tIGpRdWVyeSAxLjggb253YXJkXG4kKHdpbmRvdykub24oXCJsb2FkXCIsIGZ1bmN0aW9uKCkge1xuXG4gICAgICAgIHRyYW5zaXRpb25FdmVudCA9IHdoaWNoVHJhbnNpdGlvbkV2ZW50KCk7XG4gICAgYnV0dG9uX2ZsYWcgPSB0cnVlO1xuICAgIC8vLyBmcm9udFxuICAgICQoXCIjci1iYWNrLWZcIikuY2xpY2soZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAoYnV0dG9uX2ZsYWcpIHtcbiAgICAgICAgICAgIGJ1dHRvbl9mbGFnID0gZmFsc2U7XG4gICAgICAgICAgICByb3RhdGVfYWxpZ25fcmVzZXQoMCwgOTApO1xuICAgICAgICB9XG4gICAgfSk7XG4gICAgLy8gTGluayBhZG1pblxuICAgIC8vcHJldmlld1xuICAgICQoXCIjci1uZXh0LWZcIikuY2xpY2soZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAoYnV0dG9uX2ZsYWcpIHtcbiAgICAgICAgICAgIGJ1dHRvbl9mbGFnID0gZmFsc2U7XG4gICAgICAgICAgICByb3RhdGVfYWxpZ25fcmVzZXQoMCwgLTkwKTtcbiAgICAgICAgfVxuICAgIH0pO1xuICAgIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vICBcbiAgICAvL2xlZnRcbiAgICAkKFwiI3ItYmFja2xcIikuY2xpY2soZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAoYnV0dG9uX2ZsYWcpIHtcbiAgICAgICAgICAgIGJ1dHRvbl9mbGFnID0gZmFsc2U7XG4gICAgICAgICAgICByb3RhdGVfYWxpZ25fcmVzZXQoOTAsIDkwKTtcbiAgICAgICAgICAgIC8vcm90YXRlX2FsaWduX3Jlc2V0Uig5MCk7XG4gICAgICAgIH0gLy8gcm90YXRlX2FsaWduX3Jlc2V0Uig5MCk7XG4gICAgICAgIC8vIHJvdGF0ZV9hbGlnbl9yZXNldEMoOTApO1xuICAgIH0pO1xuICAgICQoXCIjci1uZXh0bFwiKS5jbGljayhmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmIChidXR0b25fZmxhZykge1xuICAgICAgICAgICAgYnV0dG9uX2ZsYWcgPSBmYWxzZTtcbiAgICAgICAgICAgIHJvdGF0ZV9hbGlnbl9yZXNldCg5MCwgLTkwKTtcbiAgICAgICAgfVxuICAgICAgICAvLyBhbGVydChcImhleVwiKTtcbiAgICB9KTtcbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8gICBcbiAgICAvL2JhY2sgICAgIFxuICAgICQoXCIjci1iYWNrYlwiKS5jbGljayhmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmIChidXR0b25fZmxhZykge1xuICAgICAgICAgICAgYnV0dG9uX2ZsYWcgPSBmYWxzZTtcbiAgICAgICAgICAgIHJvdGF0ZV9hbGlnbl9yZXNldCgxODAsIDkwKTtcbiAgICAgICAgfSAvL2FsZXJ0KFwiaGV5XCIpO1xuICAgIH0pO1xuICAgIC8vdXNlciBsb2dpblxuICAgICQoXCIjci1uZXh0YlwiKS5jbGljayhmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmIChidXR0b25fZmxhZykge1xuICAgICAgICAgICAgYnV0dG9uX2ZsYWcgPSBmYWxzZTtcbiAgICAgICAgICAgIHJvdGF0ZV9hbGlnbl9yZXNldCgxODAsIC05MCk7XG4gICAgICAgIH1cbiAgICB9KTtcbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vICAgIFxuICAgIC8vcmlnaHRcbiAgICAkKFwiI3ItYmFja3ZcIikuY2xpY2soZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAoYnV0dG9uX2ZsYWcpIHtcbiAgICAgICAgICAgIGJ1dHRvbl9mbGFnID0gZmFsc2U7XG4gICAgICAgICAgICByb3RhdGVfYWxpZ25fcmVzZXQoMjcwLCA5MCk7XG4gICAgICAgIH1cbiAgICB9KTtcbiAgICAvL3VzZXIgbG9naW5cbiAgICAkKFwiI3ItbmV4dHZcIikuY2xpY2soZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAoYnV0dG9uX2ZsYWcpIHtcbiAgICAgICAgICAgIGJ1dHRvbl9mbGFnID0gZmFsc2U7XG4gICAgICAgICAgICByb3RhdGVfYWxpZ25fcmVzZXQoMjcwLCAtOTApO1xuICAgICAgICB9XG4gICAgfSk7XG4gICAgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLyBMRUZUIFBBTkVMIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG4gICAgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLyBCQUNLICBQQU5FTCAgQUREUkVTUyAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLyAgXG4gICAgLy9jb250YWN0XG4gICAgdGVzdF9mcHMoKTtcbiAgICBtYWtlX2N1YmUoKTtcbiAgICAvLyBhZGp1c3QoKTtcbiAgICBrZXlib2FyZCgpO1xuICAgICQod2luZG93KS5yZXNpemUoZnVuY3Rpb24gKCkgXG4gICAge1xuICAgICAgICAvLyBhbGVydChcIlRlc3RcIik7XG4gICAgICAgIG1ha2VfY3ViZSgpO1xuICAgIH0pO1xuICBcbn0pO1xuXG5cbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcbiAgICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8gRlJPTlQgUEFORUwgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8gIFxuICAgICAgIGhpZGVfY3ViZSh0cnVlKTtcbn0pO1xuXG5mdW5jdGlvbiBoaWRlX2N1YmUoaGlkZSkge1xuICAgIGlmIChoaWRlID09IHRydWUpIHtcbiAgICAgICAgJChcIiN0b3BcIikuY3NzKFwidmlzaWJpbGl0eVwiLCBcImhpZGRlblwiKTtcbiAgICAgICAgJChcIiNib3R0b21cIikuY3NzKFwidmlzaWJpbGl0eVwiLCBcImhpZGRlblwiKTtcbiAgICAgICAgJChcIiNsZWZ0XCIpLmNzcyhcInZpc2liaWxpdHlcIiwgXCJoaWRkZW5cIik7XG4gICAgICAgICQoXCIjcmlnaHRcIikuY3NzKFwidmlzaWJpbGl0eVwiLCBcImhpZGRlblwiKTtcbiAgICAgICAgJChcIiNmcm9udFwiKS5jc3MoXCJ2aXNpYmlsaXR5XCIsIFwiaGlkZGVuXCIpO1xuICAgICAgICAkKFwiI2JhY2tcIikuY3NzKFwidmlzaWJpbGl0eVwiLCBcImhpZGRlblwiKTtcbiAgICAgICAgJChcIiNzaGFkb3dcIikuY3NzKFwidmlzaWJpbGl0eVwiLCBcImhpZGRlblwiKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICAkKFwiI3RvcFwiKS5jc3MoXCJ2aXNpYmlsaXR5XCIsIFwidmlzaWJsZVwiKTtcbiAgICAgICAgJChcIiNib3R0b21cIikuY3NzKFwidmlzaWJpbGl0eVwiLCBcInZpc2libGVcIik7XG4gICAgICAgICQoXCIjbGVmdFwiKS5jc3MoXCJ2aXNpYmlsaXR5XCIsIFwidmlzaWJsZVwiKTtcbiAgICAgICAgJChcIiNyaWdodFwiKS5jc3MoXCJ2aXNpYmlsaXR5XCIsIFwidmlzaWJsZVwiKTtcbiAgICAgICAgJChcIiNmcm9udFwiKS5jc3MoXCJ2aXNpYmlsaXR5XCIsIFwidmlzaWJsZVwiKTtcbiAgICAgICAgJChcIiNiYWNrXCIpLmNzcyhcInZpc2liaWxpdHlcIiwgXCJ2aXNpYmxlXCIpO1xuICAgICAgICAkKFwiI3NoYWRvd1wiKS5jc3MoXCJ2aXNpYmlsaXR5XCIsIFwidmlzaWJsZVwiKTtcbiAgICB9XG59XG5cbmZ1bmN0aW9uIG1ha2VfY3ViZSgpIHtcbiAgICBoaWRlX2N1YmUodHJ1ZSk7XG4gICAgaWYgKCQod2luZG93KS53aWR0aCgpID49IE1PQklMRV9XSURUSF9CUkVBS19ET1dOKSB7XG4gICAgICAgIGFkanVzdF9mdWxsX3dpZHRoKCk7XG4gICAgICAgICAvLyBhbGVydChcImRlc2t0b3BcIik7XG4gICAgfSBlbHNlIHtcbiAgICAgICAvLyAgYWxlcnQoXCJtb2JpbGVcIik7XG4gICAgICAgIGFkanVzdF9tb2JpbGVfd2lkdGgoKTtcbiAgICB9XG4gICAgaGlkZV9jdWJlKGZhbHNlKTtcbn1cblxuZnVuY3Rpb24gYWRqdXN0X21vYmlsZV93aWR0aCgpIHtcbiAgICBkZXB0aCA9IHdpZHRoID0gTWF0aC5mbG9vcigkKHdpbmRvdykud2lkdGgoKSAqIFZJRVdQT1JUX1dJRFRIX0ZBQ1RfTU9CKTtcbiAgICBoZWlnaHQgPSBNYXRoLmZsb29yKCQoXCIjZm9ybV9jb250YWludGVyXCIpLm91dGVySGVpZ2h0KCkgKyAzMCk7XG4gICAgJChcIiN2aWV3cG9ydFwiKS5jc3MoXCJ3aWR0aFwiLCB3aWR0aCk7XG4gICAgJChcIiN2aWV3cG9ydFwiKS5jc3MoXCJoZWlnaHRcIiwgaGVpZ2h0KTtcbiAgICAkKFwiI3ZpZXdwb3J0XCIpLmFkZENsYXNzKFwiY2VudGVyX3ZpZXdwb3J0XCIpO1xuICAgIFxuICAgIFxuICAgIFxuICAgIC8vd2lkdGggPSBoZWlnaHQgPSBkZXB0aCA9IERFRkFVTFRfV0lEVEg7XG4gICAgdmFyIGhhbGZfZGVwdGggPSBNYXRoLmZsb29yKGRlcHRoIC8gMik7XG4gICAgdmFyIGhhbGZfd2lkdGggPSBNYXRoLmZsb29yKHdpZHRoIC8gMik7XG4gICAgdmFyIGhhbGZfaGVpZ2h0ID0gTWF0aC5mbG9vcihoZWlnaHQgLyAyKTtcbiAgICAvLyAkKFwiI3ZpZXdwb3J0XCIpLmNzcyhcInRvcFwiLCBNYXRoLmZsb29yKCQoZG9jdW1lbnQpLmhlaWdodCgpIC8gMiApKTtcbiAgICAvLyAkKFwiI3ZpZXdwb3J0XCIpLmNzcyhcImxlZnRcIiwgTWF0aC5mbG9vcigkKGRvY3VtZW50KS53aWR0aCgpIC8gMiApKTtcbiAgICAvKlxuICAgICAkKFwiI3ZpZXdwb3J0XCIpLmNzcyhcInRvcFwiLCBNYXRoLmZsb29yKCQoZG9jdW1lbnQpLmhlaWdodCgpIC8gMiAtIGhhbGZfaGVpZ2h0KSk7XG4gICAgICQoXCIjdmlld3BvcnRcIikuY3NzKFwibGVmdFwiLCBNYXRoLmZsb29yKCQoZG9jdW1lbnQpLndpZHRoKCkgLyAyIC0gaGFsZl93aWR0aCkpO1xuICAgICAqL1xuICAgIGFkanVzdF9zaGFkb3dfb2Zmc2V0ID0gaGFsZl9oZWlnaHQgKyBzaGFkb3dfb2Zmc2V0O1xuICAgIGZyb250X3RyYW5zbGF0ZSA9ICd0cmFuc2xhdGVaKCcgKyBoYWxmX2RlcHRoICsgJ3B4KSc7XG4gICAgYmFja190cmFuc2xhdGUgPSAndHJhbnNsYXRlWignICsgKC1oYWxmX2RlcHRoKSArICdweCknICsgJyAnICsgJ3JvdGF0ZVkoMTgwZGVnKSc7XG4gICAgdG9wX3RyYW5zbGF0ZSA9ICd0cmFuc2xhdGVZKCcgKyAoLWhhbGZfaGVpZ2h0KSArICdweCknICsgJyAnICsgJ3JvdGF0ZVgoOTBkZWcpJztcbiAgICBib3R0b21fdHJhbnNsYXRlID0gJ3RyYW5zbGF0ZVkoJyArIGhhbGZfaGVpZ2h0ICsgJ3B4KScgKyAnICcgKyAncm90YXRlWCgtOTBkZWcpJztcbiAgICBsZWZ0X3RyYW5zbGF0ZSA9ICd0cmFuc2xhdGVYKCcgKyAoLWhhbGZfd2lkdGgpICsgJ3B4KScgKyAnICcgKyAncm90YXRlWSgtOTBkZWcpJztcbiAgICByaWdodF90cmFuc2xhdGUgPSAndHJhbnNsYXRlWCgnICsgaGFsZl93aWR0aCArICdweCknICsgJyAnICsgJ3JvdGF0ZVkoOTBkZWcpJztcbiAgICBzaGRvd190cmFuc2xhdGUgPSAndHJhbnNsYXRlWSgnICsgYWRqdXN0X3NoYWRvd19vZmZzZXQgKyAncHgpJyArICcgJyArICdyb3RhdGVYKC05MGRlZyknO1xuICAgIGFkanVzdF92aWV3ID0gJ3RyYW5zbGF0ZVooJyArICgtaGFsZl9kZXB0aCkgKyAncHgpJztcbiAgICAkKFwiI2N1YmVCXCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogYWRqdXN0X3ZpZXdcbiAgICB9KTtcbiAgICAkKFwiI2Zyb250XCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogZnJvbnRfdHJhbnNsYXRlXG4gICAgfSk7XG4gICAgJChcIiNiYWNrXCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogYmFja190cmFuc2xhdGVcbiAgICB9KTtcbiAgICAkKFwiI3RvcFwiKS5jc3Moe1xuICAgICAgICB0cmFuc2Zvcm06IHRvcF90cmFuc2xhdGVcbiAgICB9KTtcbiAgICAkKFwiI2JvdHRvbVwiKS5jc3Moe1xuICAgICAgICB0cmFuc2Zvcm06IGJvdHRvbV90cmFuc2xhdGVcbiAgICB9KTtcbiAgICAkKFwiI2xlZnRcIikuY3NzKHtcbiAgICAgICAgdHJhbnNmb3JtOiBsZWZ0X3RyYW5zbGF0ZVxuICAgIH0pO1xuICAgICQoXCIjcmlnaHRcIikuY3NzKHtcbiAgICAgICAgdHJhbnNmb3JtOiByaWdodF90cmFuc2xhdGVcbiAgICB9KTtcbiAgICAkKFwiI3NoYWRvd1wiKS5jc3Moe1xuICAgICAgICB0cmFuc2Zvcm06IHNoZG93X3RyYW5zbGF0ZVxuICAgIH0pO1xuICAgIC8vIHZhciBoZWlnaHRfY29udGVudCA9IFxuICAgICQoXCIjdG9wXCIpLmNzcyhcImhlaWdodFwiLCB3aWR0aCk7XG4gICAgJChcIiNib3R0b21cIikuY3NzKFwiaGVpZ2h0XCIsIHdpZHRoKTtcbiAgICAkKFwiI3NoYWRvd1wiKS5jc3MoXCJoZWlnaHRcIiwgd2lkdGgpO1xuICAgIHZhciBhZGp1c3RfdG9wID0gTWF0aC5mbG9vcigtd2lkdGggLyAyKTtcbiAgICB2YXIgYWRqdXN0X2JvdHRvbSA9IE1hdGguZmxvb3IoaGVpZ2h0IC0gd2lkdGggLyAyKTtcbiAgICB0b3BfdHJhbnNsYXRlID0gJ3RyYW5zbGF0ZVkoJyArIChhZGp1c3RfdG9wKSArICdweCknICsgJyAnICsgJ3JvdGF0ZVgoOTBkZWcpJztcbiAgICBib3R0b21fdHJhbnNsYXRlID0gJ3RyYW5zbGF0ZVkoJyArIGFkanVzdF9ib3R0b20gKyAncHgpJyArICcgJyArICdyb3RhdGVYKC05MGRlZyknO1xuICAgICQoXCIjdG9wXCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogdG9wX3RyYW5zbGF0ZVxuICAgIH0pO1xuICAgIC8vICQoXCIjdG9wXCIpLmNzcyhcImhlaWdodFwiLCB3aWR0aCk7XG4gICAgYWRqdXN0X3NoYWRvd19vZmZzZXQgPSBhZGp1c3RfYm90dG9tICsgc2hhZG93X29mZnNldDtcbiAgICBzaGRvd190cmFuc2xhdGUgPSAndHJhbnNsYXRlWSgnICsgYWRqdXN0X3NoYWRvd19vZmZzZXQgKyAncHgpJyArICcgJyArICdyb3RhdGVYKC05MGRlZyknO1xuICAgICQoXCIjYm90dG9tXCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogYm90dG9tX3RyYW5zbGF0ZVxuICAgIH0pO1xuICAgICQoXCIjc2hhZG93XCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogc2hkb3dfdHJhbnNsYXRlXG4gICAgfSk7XG59XG5mdW5jdGlvbiBhZGp1c3RfbW9iaWxlX3dpZHRoWCgpIHtcbiAgICBkZXB0aCA9IHdpZHRoID0gTWF0aC5mbG9vcigkKHdpbmRvdykud2lkdGgoKSAqIFZJRVdQT1JUX1dJRFRIX0ZBQ1RfTU9CKTtcbiAgICBoZWlnaHQgPSBNYXRoLmZsb29yKCQoXCIjZm9ybV9jb250YWludGVyXCIpLm91dGVySGVpZ2h0KCkgKyAzMCk7XG4gICAgJChcIiN2aWV3cG9ydFwiKS5jc3MoXCJ3aWR0aFwiLCB3aWR0aCk7XG4gICAgJChcIiN2aWV3cG9ydFwiKS5jc3MoXCJoZWlnaHRcIiwgaGVpZ2h0KTtcbiAgICAkKFwiI3ZpZXdwb3J0XCIpLmFkZENsYXNzKFwiY2VudGVyX3ZpZXdwb3J0XCIpO1xuICAgIFxuICAgIFxuICAgIFxuICAgIC8vd2lkdGggPSBoZWlnaHQgPSBkZXB0aCA9IERFRkFVTFRfV0lEVEg7XG4gICAgdmFyIGhhbGZfZGVwdGggPSBNYXRoLmZsb29yKGRlcHRoIC8gMik7XG4gICAgdmFyIGhhbGZfd2lkdGggPSBNYXRoLmZsb29yKHdpZHRoIC8gMik7XG4gICAgdmFyIGhhbGZfaGVpZ2h0ID0gTWF0aC5mbG9vcihoZWlnaHQgLyAyKTtcbiAgICAvLyAkKFwiI3ZpZXdwb3J0XCIpLmNzcyhcInRvcFwiLCBNYXRoLmZsb29yKCQoZG9jdW1lbnQpLmhlaWdodCgpIC8gMiApKTtcbiAgICAvLyAkKFwiI3ZpZXdwb3J0XCIpLmNzcyhcImxlZnRcIiwgTWF0aC5mbG9vcigkKGRvY3VtZW50KS53aWR0aCgpIC8gMiApKTtcbiAgICAvKlxuICAgICAkKFwiI3ZpZXdwb3J0XCIpLmNzcyhcInRvcFwiLCBNYXRoLmZsb29yKCQoZG9jdW1lbnQpLmhlaWdodCgpIC8gMiAtIGhhbGZfaGVpZ2h0KSk7XG4gICAgICQoXCIjdmlld3BvcnRcIikuY3NzKFwibGVmdFwiLCBNYXRoLmZsb29yKCQoZG9jdW1lbnQpLndpZHRoKCkgLyAyIC0gaGFsZl93aWR0aCkpO1xuICAgICAqL1xuICAgIGFkanVzdF9zaGFkb3dfb2Zmc2V0ID0gaGFsZl9oZWlnaHQgKyBzaGFkb3dfb2Zmc2V0O1xuICAgIGZyb250X3RyYW5zbGF0ZSA9ICd0cmFuc2xhdGVaKCcgKyBoYWxmX2RlcHRoICsgJ3B4KSc7XG4gICAgYmFja190cmFuc2xhdGUgPSAndHJhbnNsYXRlWignICsgKC1oYWxmX2RlcHRoKSArICdweCknICsgJyAnICsgJ3JvdGF0ZVkoMTgwZGVnKSc7XG4gICAgdG9wX3RyYW5zbGF0ZSA9ICd0cmFuc2xhdGVZKCcgKyAoLWhhbGZfaGVpZ2h0KSArICdweCknICsgJyAnICsgJ3JvdGF0ZVgoOTBkZWcpJztcbiAgICBib3R0b21fdHJhbnNsYXRlID0gJ3RyYW5zbGF0ZVkoJyArIGhhbGZfaGVpZ2h0ICsgJ3B4KScgKyAnICcgKyAncm90YXRlWCgtOTBkZWcpJztcbiAgICBsZWZ0X3RyYW5zbGF0ZSA9ICd0cmFuc2xhdGVYKCcgKyAoLWhhbGZfd2lkdGgpICsgJ3B4KScgKyAnICcgKyAncm90YXRlWSgtOTBkZWcpJztcbiAgICByaWdodF90cmFuc2xhdGUgPSAndHJhbnNsYXRlWCgnICsgaGFsZl93aWR0aCArICdweCknICsgJyAnICsgJ3JvdGF0ZVkoOTBkZWcpJztcbiAgICBzaGRvd190cmFuc2xhdGUgPSAndHJhbnNsYXRlWSgnICsgYWRqdXN0X3NoYWRvd19vZmZzZXQgKyAncHgpJyArICcgJyArICdyb3RhdGVYKC05MGRlZyknO1xuICAgIGFkanVzdF92aWV3ID0gJ3RyYW5zbGF0ZVooJyArICgtaGFsZl9kZXB0aCkgKyAncHgpJztcbiAgICAkKFwiI2N1YmVCXCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogYWRqdXN0X3ZpZXdcbiAgICB9KTtcbiAgICAkKFwiI2Zyb250XCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogZnJvbnRfdHJhbnNsYXRlXG4gICAgfSk7XG4gICAgJChcIiNiYWNrXCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogYmFja190cmFuc2xhdGVcbiAgICB9KTtcbiAgICAkKFwiI3RvcFwiKS5jc3Moe1xuICAgICAgICB0cmFuc2Zvcm06IHRvcF90cmFuc2xhdGVcbiAgICB9KTtcbiAgICAkKFwiI2JvdHRvbVwiKS5jc3Moe1xuICAgICAgICB0cmFuc2Zvcm06IGJvdHRvbV90cmFuc2xhdGVcbiAgICB9KTtcbiAgICAkKFwiI2xlZnRcIikuY3NzKHtcbiAgICAgICAgdHJhbnNmb3JtOiBsZWZ0X3RyYW5zbGF0ZVxuICAgIH0pO1xuICAgICQoXCIjcmlnaHRcIikuY3NzKHtcbiAgICAgICAgdHJhbnNmb3JtOiByaWdodF90cmFuc2xhdGVcbiAgICB9KTtcbiAgICAkKFwiI3NoYWRvd1wiKS5jc3Moe1xuICAgICAgICB0cmFuc2Zvcm06IHNoZG93X3RyYW5zbGF0ZVxuICAgIH0pO1xuICAgIC8vIHZhciBoZWlnaHRfY29udGVudCA9IFxuICAgICQoXCIjdG9wXCIpLmNzcyhcImhlaWdodFwiLCB3aWR0aCk7XG4gICAgJChcIiNib3R0b21cIikuY3NzKFwiaGVpZ2h0XCIsIHdpZHRoKTtcbiAgICAkKFwiI3NoYWRvd1wiKS5jc3MoXCJoZWlnaHRcIiwgd2lkdGgpO1xuICAgIHZhciBhZGp1c3RfdG9wID0gTWF0aC5mbG9vcigtd2lkdGggLyAyKTtcbiAgICB2YXIgYWRqdXN0X2JvdHRvbSA9IE1hdGguZmxvb3IoaGVpZ2h0IC0gd2lkdGggLyAyKTtcbiAgICB0b3BfdHJhbnNsYXRlID0gJ3RyYW5zbGF0ZVkoJyArIChhZGp1c3RfdG9wKSArICdweCknICsgJyAnICsgJ3JvdGF0ZVgoOTBkZWcpJztcbiAgICBib3R0b21fdHJhbnNsYXRlID0gJ3RyYW5zbGF0ZVkoJyArIGFkanVzdF9ib3R0b20gKyAncHgpJyArICcgJyArICdyb3RhdGVYKC05MGRlZyknO1xuICAgICQoXCIjdG9wXCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogdG9wX3RyYW5zbGF0ZVxuICAgIH0pO1xuICAgIC8vICQoXCIjdG9wXCIpLmNzcyhcImhlaWdodFwiLCB3aWR0aCk7XG4gICAgYWRqdXN0X3NoYWRvd19vZmZzZXQgPSBhZGp1c3RfYm90dG9tICsgc2hhZG93X29mZnNldDtcbiAgICBzaGRvd190cmFuc2xhdGUgPSAndHJhbnNsYXRlWSgnICsgYWRqdXN0X3NoYWRvd19vZmZzZXQgKyAncHgpJyArICcgJyArICdyb3RhdGVYKC05MGRlZyknO1xuICAgICQoXCIjYm90dG9tXCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogYm90dG9tX3RyYW5zbGF0ZVxuICAgIH0pO1xuICAgICQoXCIjc2hhZG93XCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogc2hkb3dfdHJhbnNsYXRlXG4gICAgfSk7XG59XG5cbmZ1bmN0aW9uIGFkanVzdF9mdWxsX3dpZHRoKCkge1xuICAgIHdpZHRoID0gaGVpZ2h0ID0gZGVwdGggPSBNYXRoLmZsb29yKCQoXCIjZm9ybV9jb250YWludGVyXCIpLm91dGVySGVpZ2h0KCkgKyAzMCk7XG4gICAgXG4gICAgICQoXCIjdmlld3BvcnRcIikuY3NzKFwid2lkdGhcIiwgd2lkdGgpO1xuICAgICQoXCIjdmlld3BvcnRcIikuY3NzKFwiaGVpZ2h0XCIsIGhlaWdodCk7XG4gICAgJChcIiN2aWV3cG9ydFwiKS5hZGRDbGFzcyhcImNlbnRlcl92aWV3cG9ydFwiKTtcbiAgICAvL3dpZHRoID0gaGVpZ2h0ID0gZGVwdGggPSBERUZBVUxUX1dJRFRIO1xuICAgIHZhciBoYWxmX2RlcHRoID0gTWF0aC5mbG9vcihkZXB0aCAvIDIpO1xuICAgIHZhciBoYWxmX3dpZHRoID0gTWF0aC5mbG9vcih3aWR0aCAvIDIpO1xuICAgIHZhciBoYWxmX2hlaWdodCA9IE1hdGguZmxvb3IoaGVpZ2h0IC8gMik7XG4gICAgLy8gJChcIiN2aWV3cG9ydFwiKS5jc3MoXCJ0b3BcIiwgTWF0aC5mbG9vcigkKGRvY3VtZW50KS5oZWlnaHQoKSAvIDIgKSk7XG4gICAgLy8gJChcIiN2aWV3cG9ydFwiKS5jc3MoXCJsZWZ0XCIsIE1hdGguZmxvb3IoJChkb2N1bWVudCkud2lkdGgoKSAvIDIgKSk7XG4gICAgLypcbiAgICAgJChcIiN2aWV3cG9ydFwiKS5jc3MoXCJ0b3BcIiwgTWF0aC5mbG9vcigkKGRvY3VtZW50KS5oZWlnaHQoKSAvIDIgLSBoYWxmX2hlaWdodCkpO1xuICAgICAkKFwiI3ZpZXdwb3J0XCIpLmNzcyhcImxlZnRcIiwgTWF0aC5mbG9vcigkKGRvY3VtZW50KS53aWR0aCgpIC8gMiAtIGhhbGZfd2lkdGgpKTtcbiAgICAgKi9cbiAgICBhZGp1c3Rfc2hhZG93X29mZnNldCA9IGhhbGZfaGVpZ2h0ICsgc2hhZG93X29mZnNldDtcbiAgICBmcm9udF90cmFuc2xhdGUgPSAndHJhbnNsYXRlWignICsgaGFsZl9kZXB0aCArICdweCknO1xuICAgIGJhY2tfdHJhbnNsYXRlID0gJ3RyYW5zbGF0ZVooJyArICgtaGFsZl9kZXB0aCkgKyAncHgpJyArICcgJyArICdyb3RhdGVZKDE4MGRlZyknO1xuICAgIHRvcF90cmFuc2xhdGUgPSAndHJhbnNsYXRlWSgnICsgKC1oYWxmX2hlaWdodCkgKyAncHgpJyArICcgJyArICdyb3RhdGVYKDkwZGVnKSc7XG4gICAgYm90dG9tX3RyYW5zbGF0ZSA9ICd0cmFuc2xhdGVZKCcgKyBoYWxmX2hlaWdodCArICdweCknICsgJyAnICsgJ3JvdGF0ZVgoLTkwZGVnKSc7XG4gICAgbGVmdF90cmFuc2xhdGUgPSAndHJhbnNsYXRlWCgnICsgKC1oYWxmX3dpZHRoKSArICdweCknICsgJyAnICsgJ3JvdGF0ZVkoLTkwZGVnKSc7XG4gICAgcmlnaHRfdHJhbnNsYXRlID0gJ3RyYW5zbGF0ZVgoJyArIGhhbGZfd2lkdGggKyAncHgpJyArICcgJyArICdyb3RhdGVZKDkwZGVnKSc7XG4gICAgc2hkb3dfdHJhbnNsYXRlID0gJ3RyYW5zbGF0ZVkoJyArIGFkanVzdF9zaGFkb3dfb2Zmc2V0ICsgJ3B4KScgKyAnICcgKyAncm90YXRlWCgtOTBkZWcpJztcbiAgICBhZGp1c3RfdmlldyA9ICd0cmFuc2xhdGVaKCcgKyAoLWhhbGZfZGVwdGgpICsgJ3B4KSc7XG4gICAgJChcIiNjdWJlQlwiKS5jc3Moe1xuICAgICAgICB0cmFuc2Zvcm06IGFkanVzdF92aWV3XG4gICAgfSk7XG4gICAgJChcIiNmcm9udFwiKS5jc3Moe1xuICAgICAgICB0cmFuc2Zvcm06IGZyb250X3RyYW5zbGF0ZVxuICAgIH0pO1xuICAgICQoXCIjYmFja1wiKS5jc3Moe1xuICAgICAgICB0cmFuc2Zvcm06IGJhY2tfdHJhbnNsYXRlXG4gICAgfSk7XG4gICAgLypcbiAgICAkKFwiI3RvcFwiKS5jc3Moe1xuICAgICAgICB0cmFuc2Zvcm06IHRvcF90cmFuc2xhdGVcbiAgICB9KTtcbiAgICAkKFwiI2JvdHRvbVwiKS5jc3Moe1xuICAgICAgICB0cmFuc2Zvcm06IGJvdHRvbV90cmFuc2xhdGVcbiAgICB9KTsqL1xuICAgICQoXCIjbGVmdFwiKS5jc3Moe1xuICAgICAgICB0cmFuc2Zvcm06IGxlZnRfdHJhbnNsYXRlXG4gICAgfSk7XG4gICAgJChcIiNyaWdodFwiKS5jc3Moe1xuICAgICAgICB0cmFuc2Zvcm06IHJpZ2h0X3RyYW5zbGF0ZVxuICAgIH0pO1xuICAgIC8qXG4gICAgJChcIiNzaGFkb3dcIikuY3NzKHtcbiAgICAgICAgdHJhbnNmb3JtOiBzaGRvd190cmFuc2xhdGVcbiAgICB9KTsqL1xuICAgIC8vIHZhciBoZWlnaHRfY29udGVudCA9IFxuICAgIFxuICAgICAgIC8vIHZhciBoZWlnaHRfY29udGVudCA9IFxuICAgICQoXCIjdG9wXCIpLmNzcyhcImhlaWdodFwiLCB3aWR0aCk7XG4gICAgJChcIiNib3R0b21cIikuY3NzKFwiaGVpZ2h0XCIsIHdpZHRoKTtcbiAgICAkKFwiI3NoYWRvd1wiKS5jc3MoXCJoZWlnaHRcIiwgd2lkdGgpO1xuICAgIHZhciBhZGp1c3RfdG9wID0gTWF0aC5mbG9vcigtd2lkdGggLyAyKTtcbiAgICB2YXIgYWRqdXN0X2JvdHRvbSA9IE1hdGguZmxvb3IoaGVpZ2h0IC0gd2lkdGggLyAyKTtcbiAgICB0b3BfdHJhbnNsYXRlID0gJ3RyYW5zbGF0ZVkoJyArIChhZGp1c3RfdG9wKSArICdweCknICsgJyAnICsgJ3JvdGF0ZVgoOTBkZWcpJztcbiAgICBib3R0b21fdHJhbnNsYXRlID0gJ3RyYW5zbGF0ZVkoJyArIGFkanVzdF9ib3R0b20gKyAncHgpJyArICcgJyArICdyb3RhdGVYKC05MGRlZyknO1xuICAgICQoXCIjdG9wXCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogdG9wX3RyYW5zbGF0ZVxuICAgIH0pO1xuICAgIC8vICQoXCIjdG9wXCIpLmNzcyhcImhlaWdodFwiLCB3aWR0aCk7XG4gICAgYWRqdXN0X3NoYWRvd19vZmZzZXQgPSBhZGp1c3RfYm90dG9tICsgc2hhZG93X29mZnNldDtcbiAgICBzaGRvd190cmFuc2xhdGUgPSAndHJhbnNsYXRlWSgnICsgYWRqdXN0X3NoYWRvd19vZmZzZXQgKyAncHgpJyArICcgJyArICdyb3RhdGVYKC05MGRlZyknO1xuICAgICQoXCIjYm90dG9tXCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogYm90dG9tX3RyYW5zbGF0ZVxuICAgIH0pO1xuICAgICQoXCIjc2hhZG93XCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogc2hkb3dfdHJhbnNsYXRlXG4gICAgfSk7XG59XG5cbmZ1bmN0aW9uIHRlc3RfZnBzKCkge1xuICAgIHZhciByb3RfWCA9ICdyb3RhdGVYKCcgKyBjdXJyZW50X1ggKyAnZGVnKSc7XG4gICAgJChcIiNjdWJlXCIpLmNzcyh7XG4gICAgICAgIHRyYW5zZm9ybTogcm90X1hcbiAgICB9KTtcbiAgICAkKFwiI1hcIikuaHRtbChjdXJyZW50X1gpO1xufVxuXG5mdW5jdGlvbiBrZXlib2FyZCgpIHtcbiAgICAvLyB5b3VyIGJhc2UuIEknbSBpbiBpdFxuICAgICQoZG9jdW1lbnQpLmtleWRvd24oZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgLypcbiAgICAgICAgIDM3IC0gbGVmdFxuICAgICAgICAgMzggLSB1cFxuICAgICAgICAgMzkgLSByaWdodFxuICAgICAgICAgNDAgLSBkb3duIFxuICAgICAgICAgKi9cbiAgICAgICAgdmFyIHJvdF9YWTtcbiAgICAgICAgaWYgKGUua2V5Q29kZSA9PSA0MCkge1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJsZWZ0IHByZXNzZWRcIik7XG4gICAgICAgICAgICAvLyBjdXJyZW50X1ggPSBjdXJyZW50X1ggLSBpbmNyZW1lbnQ7XG4gICAgICAgICAgICByb3RfWFkgPSAncm90YXRlWCgnICsgY3VycmVudF9YICsgJ2RlZyknICsgJyAnICsgJ3JvdGF0ZVkoJyArIGN1cnJlbnRfWSArICdkZWcpJztcbiAgICAgICAgICAgICQoXCIjY3ViZVwiKS5jc3Moe1xuICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogcm90X1hZXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICQoXCIjWFwiKS5odG1sKGN1cnJlbnRfWCk7XG4gICAgICAgICAgICAkKFwiI1lcIikuaHRtbChjdXJyZW50X1kpO1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIC8vcmlnaHRcbiAgICAgICAgaWYgKGUua2V5Q29kZSA9PSAzOCkge1xuICAgICAgICAgICAgLy8gICBjdXJyZW50X1ggPSBjdXJyZW50X1ggKyBpbmNyZW1lbnQ7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcInJpZ2h0IHByZXNzZWRcIik7XG4gICAgICAgICAgICByb3RfWFkgPSAncm90YXRlWCgnICsgY3VycmVudF9YICsgJ2RlZyknICsgJyAnICsgJ3JvdGF0ZVkoJyArIGN1cnJlbnRfWSArICdkZWcpJztcbiAgICAgICAgICAgICQoXCIjY3ViZVwiKS5jc3Moe1xuICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogcm90X1hZXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICQoXCIjWFwiKS5odG1sKGN1cnJlbnRfWCk7XG4gICAgICAgICAgICAkKFwiI1lcIikuaHRtbChjdXJyZW50X1kpO1xuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICB9XG4gICAgICAgIGlmIChlLmtleUNvZGUgPT0gMzcpIHtcbiAgICAgICAgICAgIGN1cnJlbnRfWSA9IGN1cnJlbnRfWSAtIGluY3JlbWVudDtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwidXAgcHJlc3NlZFwiKTtcbiAgICAgICAgICAgIHJvdF9YWSA9ICdyb3RhdGVYKCcgKyBjdXJyZW50X1ggKyAnZGVnKScgKyAnICcgKyAncm90YXRlWSgnICsgY3VycmVudF9ZICsgJ2RlZyknO1xuICAgICAgICAgICAgJChcIiNjdWJlXCIpLmNzcyh7XG4gICAgICAgICAgICAgICAgdHJhbnNmb3JtOiByb3RfWFlcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgJChcIiNYXCIpLmh0bWwoY3VycmVudF9YKTtcbiAgICAgICAgICAgICQoXCIjWVwiKS5odG1sKGN1cnJlbnRfWSk7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgLy80MCBkb3duXG4gICAgICAgIGlmIChlLmtleUNvZGUgPT0gMzkpIHtcbiAgICAgICAgICAgIGN1cnJlbnRfWSA9IGN1cnJlbnRfWSArIGluY3JlbWVudDtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiZG93biBwcmVzc2VkXCIpO1xuICAgICAgICAgICAgcm90X1hZID0gJ3JvdGF0ZVgoJyArIGN1cnJlbnRfWCArICdkZWcpJyArICcgJyArICdyb3RhdGVZKCcgKyBjdXJyZW50X1kgKyAnZGVnKSc7XG4gICAgICAgICAgICAkKFwiI2N1YmVcIikuY3NzKHtcbiAgICAgICAgICAgICAgICB0cmFuc2Zvcm06IHJvdF9YWVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAkKFwiI1hcIikuaHRtbChjdXJyZW50X1gpO1xuICAgICAgICAgICAgJChcIiNZXCIpLmh0bWwoY3VycmVudF9ZKTtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuICAgIH0pO1xufVxuXG5cbiIsIiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcblxuLypcbiAkKCcjbG9naW5mb3JtZicpLnZhbGlkYXRlKHsvLyBpbml0aWFsaXplIHRoZSBwbHVnaW5cbiAgICAgICAgcnVsZXM6IHtcblxuICAgICAgICAgICAgZW1haWxfbmFtZTpcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgICAgICBlbWFpbDogdHJ1ZVxuXG5cbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHBhc3N3b3JkX25hbWU6XG4gICAgICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICAgICAgbWlubGVuZ3RoOiAzXG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgfSBcbiAgICAgICAgXG4gICAgfSk7XG5cblxuICAgICQoJyNsb2dpbmZvcm1sJykudmFsaWRhdGUoey8vIGluaXRpYWxpemUgdGhlIHBsdWdpblxuICAgICAgICBydWxlczoge1xuXG4gICAgICAgICAgICBlbWFpbF9uYW1lOlxuICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGVtYWlsOiB0cnVlXG5cblxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgcGFzc3dvcmRfbmFtZTpcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW5sZW5ndGg6IDNcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSk7XG5cbiovXG5cbn0pO1xuXG5cbiIsIiIsIndpbmRvdy5jb3VudEZQUyA9IChmdW5jdGlvbiAoKSB7XG4gIHZhciBsYXN0TG9vcCA9IChuZXcgRGF0ZSgpKS5nZXRNaWxsaXNlY29uZHMoKTtcbiAgdmFyIGNvdW50ID0gMTtcbiAgdmFyIGZwcyA9IDA7XG5cbiAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgY3VycmVudExvb3AgPSAobmV3IERhdGUoKSkuZ2V0TWlsbGlzZWNvbmRzKCk7XG4gICAgaWYgKGxhc3RMb29wID4gY3VycmVudExvb3ApIHtcbiAgICAgIGZwcyA9IGNvdW50O1xuICAgICAgY291bnQgPSAxO1xuICAgIH0gZWxzZSB7XG4gICAgICBjb3VudCArPSAxO1xuICAgIH1cbiAgICBsYXN0TG9vcCA9IGN1cnJlbnRMb29wO1xuICAgIHJldHVybiBmcHM7XG4gIH07XG59KCkpO1xuXG52YXIgJG91dCA9ICQoJyNGUFMnKTtcbihmdW5jdGlvbiBsb29wKCkge1xuICAgIHJlcXVlc3RBbmltYXRpb25GcmFtZShmdW5jdGlvbiAoKSB7XG4gICAgICAkb3V0Lmh0bWwoY291bnRGUFMoKSk7XG4gICAgICBsb29wKCk7XG4gICAgfSk7XG59KCkpOyJdfQ==
