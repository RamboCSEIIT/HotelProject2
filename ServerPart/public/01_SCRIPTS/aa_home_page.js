  $('.owl-carousel').owlCarousel({
      // CSS Styles
      baseClass: "owl-carousel",
      theme: "owl-theme",
      loop: true,
      autoplay: true,
      margin: 10,
      responsiveClass: true,
      nav: true,
      rewind: true,
      responsive: {
          0: {
              items: 1,
              nav: true
          },
          600: {
              items: 3,
              nav: false
          },
          1000: {
              items: 3,
              nav: true,
              loop: false
          }
      }
  });
  $(window).scroll(function() {
      $(".slideanim").each(function() {
          var pos = $(this).offset().top;
          var winTop = $(window).scrollTop();
          if (pos < winTop + 600) {
              $(this).addClass("slide");
          }
      });
  });
  $(document).ready(function() {
      // Add smooth scrolling to all links in navbar + footer link
      $(".navbar a,#myFooter a[href='#package'],#myFooter a[href='#contact'],#myFooter a[href='#topnavid'], #myFooter a[href='#wayanad'], #myFooter a[href='#profile'] ").on('click', function(event) {
          // Make sure this.hash has a value before overriding default behavior
          if (this.hash !== "") {
              // Prevent default anchor click behavior
              event.preventDefault();
              // Store hash
              var hash = this.hash;
              // Using jQuery's animate() method to add smooth page scroll
              // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
              $('html, body').animate({
                  scrollTop: $(hash).offset().top
              }, 900, function() {
                  // Add hash (#) to URL when done scrolling (default click behavior)
                  window.location.hash = hash;
              });
          } // End if
      });
  });
  $(document).ready(function() {
      function myMap() {
          var myCenter = new google.maps.LatLng(11.6700, 75.9578);
          var mapProp = {
              center: myCenter,
              zoom: 12,
              scrollwheel: false,
              draggable: false,
              mapTypeId: google.maps.MapTypeId.ROADMAP
          };
          var map = new google.maps.Map(document.getElementById("map"), mapProp);
          var marker = new google.maps.Marker({
              position: myCenter
          });
          marker.setMap(map);
      }
      google.maps.event.addDomListener(window, 'load', initMap, {
          passive: true
      });
  });

  function initMap() {
      // Create a new StyledMapType object, passing it an array of styles,
      // and the name to be displayed on the map type control.
      var styledMapType = new google.maps.StyledMapType(
          [{
                  elementType: 'geometry',
                  stylers: [{
                      color: '#ebe3cd'
                  }]
              },
              {
                  elementType: 'labels.text.fill',
                  stylers: [{
                      color: '#523735'
                  }]
              },
              {
                  elementType: 'labels.text.stroke',
                  stylers: [{
                      color: '#f5f1e6'
                  }]
              },
              {
                  featureType: 'administrative',
                  elementType: 'geometry.stroke',
                  stylers: [{
                      color: '#c9b2a6'
                  }]
              },
              {
                  featureType: 'administrative.land_parcel',
                  elementType: 'geometry.stroke',
                  stylers: [{
                      color: '#dcd2be'
                  }]
              },
              {
                  featureType: 'administrative.land_parcel',
                  elementType: 'labels.text.fill',
                  stylers: [{
                      color: '#ae9e90'
                  }]
              },
              {
                  featureType: 'landscape.natural',
                  elementType: 'geometry',
                  stylers: [{
                      color: '#dfd2ae'
                  }]
              },
              {
                  featureType: 'poi',
                  elementType: 'geometry',
                  stylers: [{
                      color: '#dfd2ae'
                  }]
              },
              {
                  featureType: 'poi',
                  elementType: 'labels.text.fill',
                  stylers: [{
                      color: '#93817c'
                  }]
              },
              {
                  featureType: 'poi.park',
                  elementType: 'geometry.fill',
                  stylers: [{
                      color: '#a5b076'
                  }]
              },
              {
                  featureType: 'poi.park',
                  elementType: 'labels.text.fill',
                  stylers: [{
                      color: '#447530'
                  }]
              },
              {
                  featureType: 'road',
                  elementType: 'geometry',
                  stylers: [{
                      color: '#f5f1e6'
                  }]
              },
              {
                  featureType: 'road.arterial',
                  elementType: 'geometry',
                  stylers: [{
                      color: '#fdfcf8'
                  }]
              },
              {
                  featureType: 'road.highway',
                  elementType: 'geometry',
                  stylers: [{
                      color: '#f8c967'
                  }]
              },
              {
                  featureType: 'road.highway',
                  elementType: 'geometry.stroke',
                  stylers: [{
                      color: '#e9bc62'
                  }]
              },
              {
                  featureType: 'road.highway.controlled_access',
                  elementType: 'geometry',
                  stylers: [{
                      color: '#e98d58'
                  }]
              },
              {
                  featureType: 'road.highway.controlled_access',
                  elementType: 'geometry.stroke',
                  stylers: [{
                      color: '#db8555'
                  }]
              },
              {
                  featureType: 'road.local',
                  elementType: 'labels.text.fill',
                  stylers: [{
                      color: '#806b63'
                  }]
              },
              {
                  featureType: 'transit.line',
                  elementType: 'geometry',
                  stylers: [{
                      color: '#dfd2ae'
                  }]
              },
              {
                  featureType: 'transit.line',
                  elementType: 'labels.text.fill',
                  stylers: [{
                      color: '#8f7d77'
                  }]
              },
              {
                  featureType: 'transit.line',
                  elementType: 'labels.text.stroke',
                  stylers: [{
                      color: '#ebe3cd'
                  }]
              },
              {
                  featureType: 'transit.station',
                  elementType: 'geometry',
                  stylers: [{
                      color: '#dfd2ae'
                  }]
              },
              {
                  featureType: 'water',
                  elementType: 'geometry.fill',
                  stylers: [{
                      color: '#b9d3c2'
                  }]
              },
              {
                  featureType: 'water',
                  elementType: 'labels.text.fill',
                  stylers: [{
                      color: '#92998d'
                  }]
              }
          ], {
              name: 'Styled Map'
          });
      // Create a map object, and include the MapTypeId to add
      // to the map type control.
      var map = new google.maps.Map(document.getElementById('map'), {
          center: {
              lat: 11.6700,
              lng: 75.9578
          },
          zoom: 11,
          mapTypeControlOptions: {
              mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain',
                  'styled_map'
              ]
          }
      });
      //Associate the styled map with the MapTypeId and set it to display.
      map.mapTypes.set('styled_map', styledMapType);
      map.setMapTypeId('styled_map');
      var myCenter = new google.maps.LatLng(11.6700, 75.9578);
      var marker = new google.maps.Marker({
          position: myCenter
      });
      marker.setMap(map);
  }
  $('.navbar-nav>li>a').on('click', function() {
      var id = $(this).attr('id');
      // alert(id);
      if (id != "admin_navdrop")
          $('.navbar-collapse').collapse('hide');
  });
  $('.navbar-collapse').on('shown.bs.collapse', function() {
      //alert("hello");
      $('#tag').html("Menu");
      $('#tag').removeClass("fa fa-bars");
      $('#tag').addClass("fa fa-times");
  });
  $('.navbar-collapse').on('hidden.bs.collapse', function() {
      // $('#tag').html("O");
      $('#tag').html("Menu");
      $('#tag').removeClass("fa fa-times");
      $('#tag').addClass("fa fa-bars");
  });
  // magic.js
  $(document).ready(function() {
      // process the form
      $('#formsub').submit(function(event) {
          // get the form data
          // there are many ways to get this data using jQuery (you can use the class or id also)
          var formData = {
              //   'name': $('input[name=name]').val(),
              'email': $('input[name=email]').val(),
              //  'superheroAlias': $('input[name=superheroAlias]').val()
          };
          // process the form
          $.ajax({
                  type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                  url: '/subs', // the url where we want to POST
                  data: formData, // our data object
                  dataType: 'json', // what type of data do we expect back from the server
                  encode: true
              })
              // using the done promise callback
              .done(function(data) {
                  // log data to the console so we can see
                  console.log(data);
                  // here we will handle errors and validation messages
                  if (!data.success) {
                      $("#submsg").html(data.errors.email);
                      /*                        // handle errors for name ---------------
                                              if (data.errors.name) {
                                                  $('#name-group').addClass('has-error'); // add the error class to show red input
                                                  $('#name-group').append('<div class="help-block">' + data.errors.name + '</div>'); // add the actual error message under our input
                                              }
                                              // handle errors for email ---------------
                                              if (data.errors.email) {
                                                  $('#email-group').addClass('has-error'); // add the error class to show red input
                                                  $('#email-group').append('<div class="help-block">' + data.errors.email + '</div>'); // add the actual error message under our input
                                              }
                                              // handle errors for superhero alias ---------------
                                              if (data.errors.superheroAlias) {
                                                  $('#superhero-group').addClass('has-error'); // add the error class to show red input
                                                  $('#superhero-group').append('<div class="help-block">' + data.errors.superheroAlias + '</div>'); // add the actual error message under our input
                                              }*/
                  } else {
                      // ALL GOOD! just show the success message!
                      // $('form').append('<div class="alert alert-success">' + data.message + '</div>');
                      // usually after form submission, you'll want to redirect
                      // window.location = '/thank-you'; // redirect a user to another page
                      //  alert('success'+data.message); // for now we'll just alert the user
                      $("#submsg").html(data.message);
                  }
              });
          // stop the form from submitting the normal way and refreshing the page
          event.preventDefault();
      });
  });
  // magic.js
  $(document).ready(function() {
      // process the form
      $('#contactId').submit(function(event) {
          // get the form data
          // there are many ways to get this data using jQuery (you can use the class or id also)
          var formData = {
              'ame': $('input[name=name]').val(),
              'email': $('input[name=email]').val(),
              'comments': $('input[name=comments]').val()
          };
          // process the form
          $.ajax({
                  type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                  url: '/contact', // the url where we want to POST
                  data: formData, // our data object
                  dataType: 'json', // what type of data do we expect back from the server
                  encode: true
              })
              // using the done promise callback
              .done(function(data) {
                  // log data to the console so we can see
                  console.log(data);
                  // here we will handle errors and validation messages
                  if (!data.success) {
                      $("#contactmsg").html(data.errors.email);
                      /*                        // handle errors for name ---------------
                       if (data.errors.name) {
                       $('#name-group').addClass('has-error'); // add the error class to show red input
                       $('#name-group').append('<div class="help-block">' + data.errors.name + '</div>'); // add the actual error message under our input
                       }
                       // handle errors for email ---------------
                       if (data.errors.email) {
                       $('#email-group').addClass('has-error'); // add the error class to show red input
                       $('#email-group').append('<div class="help-block">' + data.errors.email + '</div>'); // add the actual error message under our input
                       }
                       // handle errors for superhero alias ---------------
                       if (data.errors.superheroAlias) {
                       $('#superhero-group').addClass('has-error'); // add the error class to show red input
                       $('#superhero-group').append('<div class="help-block">' + data.errors.superheroAlias + '</div>'); // add the actual error message under our input
                       }*/
                  } else {
                      // ALL GOOD! just show the success message!
                      // $('form').append('<div class="alert alert-success">' + data.message + '</div>');
                      // usually after form submission, you'll want to redirect
                      // window.location = '/thank-you'; // redirect a user to another page
                      //  alert('success'+data.message); // for now we'll just alert the user
                      $("#contactmsg").html(data.message);
                  }
              });
          // stop the form from submitting the normal way and refreshing the page
          event.preventDefault();
      });
      $('#enquiryform').submit(function(event) {
          //  var result = $('#enqid').val();
          var name = $('#name').val();
          var email = $('#email').val();
          var mobile = $('#mobile').val();
          var comments = $('#comments').val();
          // alert("hai");
          // get the form data
          // there are many ways to get this data using jQuery (you can use the class or id also)
          var formData = {
              'name': name,
              'email': email,
              'comments': comments,
              'mobile': mobile
          };
          // console.log("Hello :: "+formData);
          // 
          // 
          // process the form
          // alert(formData.name+"::"+formData.email +"::"+formData.mobile+"::"+formData.comments);
          $.ajax({
                  type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                  url: '/contact', // the url where we want to POST
                  data: formData, // our data object
                  dataType: 'json', // what type of data do we expect back from the server
                  encode: true
              })
              // using the done promise callback
              .done(function(data) {
                  // log data to the console so we can see
                  console.log(data);
                  // here we will handle errors and validation messages
                  if (!data.success) {
                      $("#contactmsg").html(data.message);
                      /*                        // handle errors for name ---------------
                       if (data.errors.name) {
                       $('#name-group').addClass('has-error'); // add the error class to show red input
                       $('#name-group').append('<div class="help-block">' + data.errors.name + '</div>'); // add the actual error message under our input
                       }
                       // handle errors for email ---------------
                       if (data.errors.email) {
                       $('#email-group').addClass('has-error'); // add the error class to show red input
                       $('#email-group').append('<div class="help-block">' + data.errors.email + '</div>'); // add the actual error message under our input
                       }
                       // handle errors for superhero alias ---------------
                       if (data.errors.superheroAlias) {
                       $('#superhero-group').addClass('has-error'); // add the error class to show red input
                       $('#superhero-group').append('<div class="help-block">' + data.errors.superheroAlias + '</div>'); // add the actual error message under our input
                       }*/
                  } else {
                      // ALL GOOD! just show the success message!
                      // $('form').append('<div class="alert alert-success">' + data.message + '</div>');
                      // usually after form submission, you'll want to redirect
                      // window.location = '/thank-you'; // redirect a user to another page
                      //  alert('success'+data.message); // for now we'll just alert the user
                      $("#contactmsg").html(data.message);
                  }
              });
          // stop the form from submitting the normal way and refreshing the page
          event.preventDefault();
      });
  });
  // A $( document ).ready() block.
  $(document).ready(function() {
      $("#wayanad_1").addClass("active");
  });
  $(document).ready(function() {
      for (var i = 0; i < 9; i++) {
          /*
          if(i%3==0)
          $("#package_"+i).addClass("cardSet0");
          if(i%3==1)
          $("#package_"+i).addClass("cardSet1");
           if(i%3==2)
          $("#package_"+i).addClass("cardSet2");
*/
      }
  });
  $(document).ready(function() {
      $(".card-text").addClass("text-justify");
  });
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFhX293bF9jYXJvdXNlbC9vd2xfY2Fyb3VzZWwuanMiLCJhYl9TbGlkZV9Gcm9tX0JvdHRvbS9zbGlkZV9ib3R0b20uanMiLCJhY19tb3ZlX2FuaW0vbW92ZV9hbmltLmpzIiwiYWRfZ29vZ2xlX21hcC9nb29nbGUtbWFwLmpzIiwiYWVfbW9iaWxlX21lbnUvbWVudV9tb2JpbGUuanMiLCJhZl9zdWJzY3JpcHRpb24vc3Vic2NyaXB0aW9uLmpzIiwiYWdfY29udGFjdC9jb250YWN0LmpzIiwiYWhfd2F5YW5hZC93YXlhbmFkLmpzIiwiYWlfcGFja2FnZS9wYWNrYWdlLmpzIiwiYWlfdGV4dGJhbGFuY2UvYmFsYW5jZS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUM3QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDWEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDaENBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDcktBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUN6QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3RFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNqS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ0pBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNuQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJhYV9ob21lX3BhZ2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgIFxuICAkKCcub3dsLWNhcm91c2VsJykub3dsQ2Fyb3VzZWwoe1xuICBcbiAgICAgLy8gQ1NTIFN0eWxlc1xuICAgIGJhc2VDbGFzcyA6IFwib3dsLWNhcm91c2VsXCIsXG4gICAgdGhlbWUgOiBcIm93bC10aGVtZVwiLFxuICAgIGxvb3A6dHJ1ZSxcbiAgICBhdXRvcGxheTp0cnVlLCAgICAgICAgXG4gICAgbWFyZ2luOjEwLFxuICAgIHJlc3BvbnNpdmVDbGFzczp0cnVlLFxuICAgIG5hdjp0cnVlLFxuICAgIHJld2luZDp0cnVlLFxuICAgIHJlc3BvbnNpdmU6e1xuICAgICAgICAwOntcbiAgICAgICAgICAgIGl0ZW1zOjEsXG4gICAgICAgICAgICBuYXY6dHJ1ZVxuICAgICAgICB9LFxuICAgICAgICA2MDA6e1xuICAgICAgICAgICAgaXRlbXM6MyxcbiAgICAgICAgICAgIG5hdjpmYWxzZVxuICAgICAgICB9LFxuICAgICAgICAxMDAwOntcbiAgICAgICAgICAgIGl0ZW1zOjMsXG4gICAgICAgICAgICBuYXY6dHJ1ZSxcbiAgICAgICAgICAgIGxvb3A6ZmFsc2VcbiAgICAgICAgfVxuICAgIH1cbn0pO1xuXG4iLCIgICQod2luZG93KS5zY3JvbGwoZnVuY3Rpb24oKSB7XG4gICQoXCIuc2xpZGVhbmltXCIpLmVhY2goZnVuY3Rpb24oKXtcbiAgICB2YXIgcG9zID0gJCh0aGlzKS5vZmZzZXQoKS50b3A7XG5cbiAgICB2YXIgd2luVG9wID0gJCh3aW5kb3cpLnNjcm9sbFRvcCgpO1xuICAgIGlmIChwb3MgPCB3aW5Ub3AgKyA2MDApIHtcbiAgICAgICQodGhpcykuYWRkQ2xhc3MoXCJzbGlkZVwiKTtcbiAgICB9XG4gIH0pO1xufSk7ICAgICAgICAgXG4gXG4iLCIkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKVxue1xuICAgIC8vIEFkZCBzbW9vdGggc2Nyb2xsaW5nIHRvIGFsbCBsaW5rcyBpbiBuYXZiYXIgKyBmb290ZXIgbGlua1xuICAgICQoXCIubmF2YmFyIGEsI215Rm9vdGVyIGFbaHJlZj0nI3BhY2thZ2UnXSwjbXlGb290ZXIgYVtocmVmPScjY29udGFjdCddLCNteUZvb3RlciBhW2hyZWY9JyN0b3BuYXZpZCddLCAjbXlGb290ZXIgYVtocmVmPScjd2F5YW5hZCddLCAjbXlGb290ZXIgYVtocmVmPScjcHJvZmlsZSddIFwiKS5vbignY2xpY2snLCBmdW5jdGlvbiAoZXZlbnQpIHtcblxuICAgICAgICAvLyBNYWtlIHN1cmUgdGhpcy5oYXNoIGhhcyBhIHZhbHVlIGJlZm9yZSBvdmVycmlkaW5nIGRlZmF1bHQgYmVoYXZpb3JcbiAgICAgICAgaWYgKHRoaXMuaGFzaCAhPT0gXCJcIilcbiAgICAgICAge1xuXG4gICAgICAgICAgICAvLyBQcmV2ZW50IGRlZmF1bHQgYW5jaG9yIGNsaWNrIGJlaGF2aW9yXG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgICAgICAvLyBTdG9yZSBoYXNoXG4gICAgICAgICAgICB2YXIgaGFzaCA9IHRoaXMuaGFzaDtcblxuICAgICAgICAgICAgLy8gVXNpbmcgalF1ZXJ5J3MgYW5pbWF0ZSgpIG1ldGhvZCB0byBhZGQgc21vb3RoIHBhZ2Ugc2Nyb2xsXG4gICAgICAgICAgICAvLyBUaGUgb3B0aW9uYWwgbnVtYmVyICg5MDApIHNwZWNpZmllcyB0aGUgbnVtYmVyIG9mIG1pbGxpc2Vjb25kcyBpdCB0YWtlcyB0byBzY3JvbGwgdG8gdGhlIHNwZWNpZmllZCBhcmVhXG4gICAgICAgICAgICAkKCdodG1sLCBib2R5JykuYW5pbWF0ZSh7XG4gICAgICAgICAgICAgICAgc2Nyb2xsVG9wOiAkKGhhc2gpLm9mZnNldCgpLnRvcFxuICAgICAgICAgICAgfSwgOTAwLCBmdW5jdGlvbiAoKSB7XG5cbiAgICAgICAgICAgICAgICAvLyBBZGQgaGFzaCAoIykgdG8gVVJMIHdoZW4gZG9uZSBzY3JvbGxpbmcgKGRlZmF1bHQgY2xpY2sgYmVoYXZpb3IpXG4gICAgICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLmhhc2ggPSBoYXNoO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0gLy8gRW5kIGlmXG4gICAgfSk7XG5cblxuXG5cblxuXG59KTsiLCJcbiBcblxuXG5cbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpXG57XG5cblxuICAgIGZ1bmN0aW9uIG15TWFwKCkge1xuXG4gICAgICAgIHZhciBteUNlbnRlciA9IG5ldyBnb29nbGUubWFwcy5MYXRMbmcoMTEuNjcwMCwgNzUuOTU3OCk7XG4gICAgICAgIHZhciBtYXBQcm9wID0ge2NlbnRlcjogbXlDZW50ZXIsIHpvb206IDEyLCBzY3JvbGx3aGVlbDogZmFsc2UsIGRyYWdnYWJsZTogZmFsc2UsIG1hcFR5cGVJZDogZ29vZ2xlLm1hcHMuTWFwVHlwZUlkLlJPQURNQVB9O1xuICAgICAgICB2YXIgbWFwID0gbmV3IGdvb2dsZS5tYXBzLk1hcChkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIm1hcFwiKSwgbWFwUHJvcCk7XG4gICAgICAgIHZhciBtYXJrZXIgPSBuZXcgZ29vZ2xlLm1hcHMuTWFya2VyKHtwb3NpdGlvbjogbXlDZW50ZXJ9KTtcbiAgICAgICAgbWFya2VyLnNldE1hcChtYXApO1xuICAgIH1cblxuICAgIGdvb2dsZS5tYXBzLmV2ZW50LmFkZERvbUxpc3RlbmVyKHdpbmRvdywgJ2xvYWQnLCBpbml0TWFwLCB7cGFzc2l2ZTogdHJ1ZX0pO1xuXG5cblxuXG59KTtcblxuIFxuZnVuY3Rpb24gaW5pdE1hcCgpIHtcblxuICAgIC8vIENyZWF0ZSBhIG5ldyBTdHlsZWRNYXBUeXBlIG9iamVjdCwgcGFzc2luZyBpdCBhbiBhcnJheSBvZiBzdHlsZXMsXG4gICAgLy8gYW5kIHRoZSBuYW1lIHRvIGJlIGRpc3BsYXllZCBvbiB0aGUgbWFwIHR5cGUgY29udHJvbC5cbiAgICB2YXIgc3R5bGVkTWFwVHlwZSA9IG5ldyBnb29nbGUubWFwcy5TdHlsZWRNYXBUeXBlKFxuICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgIHtlbGVtZW50VHlwZTogJ2dlb21ldHJ5Jywgc3R5bGVyczogW3tjb2xvcjogJyNlYmUzY2QnfV19LFxuICAgICAgICAgICAgICAgIHtlbGVtZW50VHlwZTogJ2xhYmVscy50ZXh0LmZpbGwnLCBzdHlsZXJzOiBbe2NvbG9yOiAnIzUyMzczNSd9XX0sXG4gICAgICAgICAgICAgICAge2VsZW1lbnRUeXBlOiAnbGFiZWxzLnRleHQuc3Ryb2tlJywgc3R5bGVyczogW3tjb2xvcjogJyNmNWYxZTYnfV19LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICdhZG1pbmlzdHJhdGl2ZScsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnZ2VvbWV0cnkuc3Ryb2tlJyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNjOWIyYTYnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICdhZG1pbmlzdHJhdGl2ZS5sYW5kX3BhcmNlbCcsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnZ2VvbWV0cnkuc3Ryb2tlJyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNkY2QyYmUnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICdhZG1pbmlzdHJhdGl2ZS5sYW5kX3BhcmNlbCcsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnbGFiZWxzLnRleHQuZmlsbCcsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjYWU5ZTkwJ31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAnbGFuZHNjYXBlLm5hdHVyYWwnLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2dlb21ldHJ5JyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNkZmQyYWUnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICdwb2knLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2dlb21ldHJ5JyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNkZmQyYWUnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICdwb2knLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2xhYmVscy50ZXh0LmZpbGwnLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnIzkzODE3Yyd9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ3BvaS5wYXJrJyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdnZW9tZXRyeS5maWxsJyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNhNWIwNzYnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICdwb2kucGFyaycsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnbGFiZWxzLnRleHQuZmlsbCcsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjNDQ3NTMwJ31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAncm9hZCcsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnZ2VvbWV0cnknLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnI2Y1ZjFlNid9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ3JvYWQuYXJ0ZXJpYWwnLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2dlb21ldHJ5JyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNmZGZjZjgnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICdyb2FkLmhpZ2h3YXknLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2dlb21ldHJ5JyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNmOGM5NjcnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICdyb2FkLmhpZ2h3YXknLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2dlb21ldHJ5LnN0cm9rZScsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjZTliYzYyJ31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAncm9hZC5oaWdod2F5LmNvbnRyb2xsZWRfYWNjZXNzJyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdnZW9tZXRyeScsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjZTk4ZDU4J31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAncm9hZC5oaWdod2F5LmNvbnRyb2xsZWRfYWNjZXNzJyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdnZW9tZXRyeS5zdHJva2UnLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnI2RiODU1NSd9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ3JvYWQubG9jYWwnLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2xhYmVscy50ZXh0LmZpbGwnLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnIzgwNmI2Myd9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ3RyYW5zaXQubGluZScsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnZ2VvbWV0cnknLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnI2RmZDJhZSd9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ3RyYW5zaXQubGluZScsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnbGFiZWxzLnRleHQuZmlsbCcsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjOGY3ZDc3J31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAndHJhbnNpdC5saW5lJyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdsYWJlbHMudGV4dC5zdHJva2UnLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnI2ViZTNjZCd9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ3RyYW5zaXQuc3RhdGlvbicsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnZ2VvbWV0cnknLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnI2RmZDJhZSd9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ3dhdGVyJyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdnZW9tZXRyeS5maWxsJyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNiOWQzYzInfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICd3YXRlcicsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnbGFiZWxzLnRleHQuZmlsbCcsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjOTI5OThkJ31dXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIHtuYW1lOiAnU3R5bGVkIE1hcCd9KTtcblxuICAgIC8vIENyZWF0ZSBhIG1hcCBvYmplY3QsIGFuZCBpbmNsdWRlIHRoZSBNYXBUeXBlSWQgdG8gYWRkXG4gICAgLy8gdG8gdGhlIG1hcCB0eXBlIGNvbnRyb2wuXG4gICAgdmFyIG1hcCA9IG5ldyBnb29nbGUubWFwcy5NYXAoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ21hcCcpLCB7XG4gICAgICAgIGNlbnRlcjoge2xhdDogMTEuNjcwMCwgbG5nOiA3NS45NTc4fSxcbiAgICAgICAgem9vbTogMTEsXG4gICAgICAgIG1hcFR5cGVDb250cm9sT3B0aW9uczoge1xuICAgICAgICAgICAgbWFwVHlwZUlkczogWydyb2FkbWFwJywgJ3NhdGVsbGl0ZScsICdoeWJyaWQnLCAndGVycmFpbicsXG4gICAgICAgICAgICAgICAgJ3N0eWxlZF9tYXAnXVxuICAgICAgICB9XG4gICAgfSk7XG5cbiAgICAvL0Fzc29jaWF0ZSB0aGUgc3R5bGVkIG1hcCB3aXRoIHRoZSBNYXBUeXBlSWQgYW5kIHNldCBpdCB0byBkaXNwbGF5LlxuICAgIG1hcC5tYXBUeXBlcy5zZXQoJ3N0eWxlZF9tYXAnLCBzdHlsZWRNYXBUeXBlKTtcbiAgICBtYXAuc2V0TWFwVHlwZUlkKCdzdHlsZWRfbWFwJyk7XG5cbiAgICB2YXIgbXlDZW50ZXIgPSBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKDExLjY3MDAsIDc1Ljk1NzgpO1xuICAgIHZhciBtYXJrZXIgPSBuZXcgZ29vZ2xlLm1hcHMuTWFya2VyKHtwb3NpdGlvbjogbXlDZW50ZXJ9KTtcbiAgICBtYXJrZXIuc2V0TWFwKG1hcCk7XG4gICAgXG4gICAgIFxuXG4gICAgXG59IiwiICAgICQoJy5uYXZiYXItbmF2PmxpPmEnKS5vbignY2xpY2snLCBmdW5jdGlvbigpXG4gICAge1xuICAgICAgICAgICB2YXIgaWQgPSAkKHRoaXMpLmF0dHIoJ2lkJyk7XG4gICAgICAgICAgIC8vIGFsZXJ0KGlkKTtcbiAgICAgICAgICAgaWYoaWQgIT0gXCJhZG1pbl9uYXZkcm9wXCIpXG4gICAgICAgICAgICAgICAgICAkKCcubmF2YmFyLWNvbGxhcHNlJykuY29sbGFwc2UoJ2hpZGUnKTtcbiAgICB9KTtcbiAgICBcbiAgICAkKCcubmF2YmFyLWNvbGxhcHNlJykub24oJ3Nob3duLmJzLmNvbGxhcHNlJywgZnVuY3Rpb24oKVxuICAgIHtcbiAgICAgICAvL2FsZXJ0KFwiaGVsbG9cIik7XG4gICAgICAgICQoJyN0YWcnKS5odG1sKFwiTWVudVwiKTtcbiAgICAgICAgJCgnI3RhZycpLnJlbW92ZUNsYXNzKFwiZmEgZmEtYmFyc1wiKTtcbiAgICAgICAgJCgnI3RhZycpLmFkZENsYXNzKFwiZmEgZmEtdGltZXNcIik7XG4gICAgICAgIFxuICAgICAgIFxuICAgIH0pO1xuICAgIFxuICAgIFxuICAgICQoJy5uYXZiYXItY29sbGFwc2UnKS5vbignaGlkZGVuLmJzLmNvbGxhcHNlJywgZnVuY3Rpb24oKVxuICAgIHtcbiAgICAgIC8vICQoJyN0YWcnKS5odG1sKFwiT1wiKTtcbiAgICAgICAgICAgICAkKCcjdGFnJykuaHRtbChcIk1lbnVcIik7XG4gICAgICAgICAgICAkKCcjdGFnJykucmVtb3ZlQ2xhc3MoXCJmYSBmYS10aW1lc1wiKTtcbiAgICAgICAgICAgICAgJCgnI3RhZycpLmFkZENsYXNzKFwiZmEgZmEtYmFyc1wiKTtcbiAgICB9KTsiLCIvLyBtYWdpYy5qc1xuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkge1xuXG4gICAgLy8gcHJvY2VzcyB0aGUgZm9ybVxuICAgICQoJyNmb3Jtc3ViJykuc3VibWl0KGZ1bmN0aW9uIChldmVudCkge1xuXG4gICAgICAgIC8vIGdldCB0aGUgZm9ybSBkYXRhXG4gICAgICAgIC8vIHRoZXJlIGFyZSBtYW55IHdheXMgdG8gZ2V0IHRoaXMgZGF0YSB1c2luZyBqUXVlcnkgKHlvdSBjYW4gdXNlIHRoZSBjbGFzcyBvciBpZCBhbHNvKVxuICAgICAgICB2YXIgZm9ybURhdGEgPSB7XG4gICAgICAgICAvLyAgICduYW1lJzogJCgnaW5wdXRbbmFtZT1uYW1lXScpLnZhbCgpLFxuICAgICAgICAgICAgJ2VtYWlsJzogJCgnaW5wdXRbbmFtZT1lbWFpbF0nKS52YWwoKSxcbiAgICAgICAgICAvLyAgJ3N1cGVyaGVyb0FsaWFzJzogJCgnaW5wdXRbbmFtZT1zdXBlcmhlcm9BbGlhc10nKS52YWwoKVxuICAgICAgICB9O1xuXG4gICAgICAgIC8vIHByb2Nlc3MgdGhlIGZvcm1cbiAgICAgICAgJC5hamF4KHtcbiAgICAgICAgICAgIHR5cGU6ICdQT1NUJywgLy8gZGVmaW5lIHRoZSB0eXBlIG9mIEhUVFAgdmVyYiB3ZSB3YW50IHRvIHVzZSAoUE9TVCBmb3Igb3VyIGZvcm0pXG4gICAgICAgICAgICB1cmw6ICcvc3VicycsIC8vIHRoZSB1cmwgd2hlcmUgd2Ugd2FudCB0byBQT1NUXG4gICAgICAgICAgICBkYXRhOiBmb3JtRGF0YSwgLy8gb3VyIGRhdGEgb2JqZWN0XG4gICAgICAgICAgICBkYXRhVHlwZTogJ2pzb24nLCAvLyB3aGF0IHR5cGUgb2YgZGF0YSBkbyB3ZSBleHBlY3QgYmFjayBmcm9tIHRoZSBzZXJ2ZXJcbiAgICAgICAgICAgIGVuY29kZTogdHJ1ZVxuICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC8vIHVzaW5nIHRoZSBkb25lIHByb21pc2UgY2FsbGJhY2tcbiAgICAgICAgICAgICAgICAuZG9uZShmdW5jdGlvbiAoZGF0YSkge1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIGxvZyBkYXRhIHRvIHRoZSBjb25zb2xlIHNvIHdlIGNhbiBzZWVcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YSk7XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gaGVyZSB3ZSB3aWxsIGhhbmRsZSBlcnJvcnMgYW5kIHZhbGlkYXRpb24gbWVzc2FnZXNcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFkYXRhLnN1Y2Nlc3MpIFxuICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNzdWJtc2dcIikuaHRtbChkYXRhLmVycm9ycy5lbWFpbCk7XG4vKiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGhhbmRsZSBlcnJvcnMgZm9yIG5hbWUgLS0tLS0tLS0tLS0tLS0tXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YS5lcnJvcnMubmFtZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNuYW1lLWdyb3VwJykuYWRkQ2xhc3MoJ2hhcy1lcnJvcicpOyAvLyBhZGQgdGhlIGVycm9yIGNsYXNzIHRvIHNob3cgcmVkIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJCgnI25hbWUtZ3JvdXAnKS5hcHBlbmQoJzxkaXYgY2xhc3M9XCJoZWxwLWJsb2NrXCI+JyArIGRhdGEuZXJyb3JzLm5hbWUgKyAnPC9kaXY+Jyk7IC8vIGFkZCB0aGUgYWN0dWFsIGVycm9yIG1lc3NhZ2UgdW5kZXIgb3VyIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGhhbmRsZSBlcnJvcnMgZm9yIGVtYWlsIC0tLS0tLS0tLS0tLS0tLVxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRhdGEuZXJyb3JzLmVtYWlsKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJCgnI2VtYWlsLWdyb3VwJykuYWRkQ2xhc3MoJ2hhcy1lcnJvcicpOyAvLyBhZGQgdGhlIGVycm9yIGNsYXNzIHRvIHNob3cgcmVkIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJCgnI2VtYWlsLWdyb3VwJykuYXBwZW5kKCc8ZGl2IGNsYXNzPVwiaGVscC1ibG9ja1wiPicgKyBkYXRhLmVycm9ycy5lbWFpbCArICc8L2Rpdj4nKTsgLy8gYWRkIHRoZSBhY3R1YWwgZXJyb3IgbWVzc2FnZSB1bmRlciBvdXIgaW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gaGFuZGxlIGVycm9ycyBmb3Igc3VwZXJoZXJvIGFsaWFzIC0tLS0tLS0tLS0tLS0tLVxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRhdGEuZXJyb3JzLnN1cGVyaGVyb0FsaWFzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJCgnI3N1cGVyaGVyby1ncm91cCcpLmFkZENsYXNzKCdoYXMtZXJyb3InKTsgLy8gYWRkIHRoZSBlcnJvciBjbGFzcyB0byBzaG93IHJlZCBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNzdXBlcmhlcm8tZ3JvdXAnKS5hcHBlbmQoJzxkaXYgY2xhc3M9XCJoZWxwLWJsb2NrXCI+JyArIGRhdGEuZXJyb3JzLnN1cGVyaGVyb0FsaWFzICsgJzwvZGl2PicpOyAvLyBhZGQgdGhlIGFjdHVhbCBlcnJvciBtZXNzYWdlIHVuZGVyIG91ciBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgfSovXG5cbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gQUxMIEdPT0QhIGp1c3Qgc2hvdyB0aGUgc3VjY2VzcyBtZXNzYWdlIVxuICAgICAgICAgICAgICAgICAgICAgICAvLyAkKCdmb3JtJykuYXBwZW5kKCc8ZGl2IGNsYXNzPVwiYWxlcnQgYWxlcnQtc3VjY2Vzc1wiPicgKyBkYXRhLm1lc3NhZ2UgKyAnPC9kaXY+Jyk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHVzdWFsbHkgYWZ0ZXIgZm9ybSBzdWJtaXNzaW9uLCB5b3UnbGwgd2FudCB0byByZWRpcmVjdFxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gd2luZG93LmxvY2F0aW9uID0gJy90aGFuay15b3UnOyAvLyByZWRpcmVjdCBhIHVzZXIgdG8gYW5vdGhlciBwYWdlXG4gICAgICAgICAgICAgICAgICAgICAgLy8gIGFsZXJ0KCdzdWNjZXNzJytkYXRhLm1lc3NhZ2UpOyAvLyBmb3Igbm93IHdlJ2xsIGp1c3QgYWxlcnQgdGhlIHVzZXJcbiAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgICAkKFwiI3N1Ym1zZ1wiKS5odG1sKGRhdGEubWVzc2FnZSk7XG5cbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgLy8gc3RvcCB0aGUgZm9ybSBmcm9tIHN1Ym1pdHRpbmcgdGhlIG5vcm1hbCB3YXkgYW5kIHJlZnJlc2hpbmcgdGhlIHBhZ2VcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICB9KTtcblxufSk7IiwiLy8gbWFnaWMuanNcbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcblxuICAgIC8vIHByb2Nlc3MgdGhlIGZvcm1cbiAgICAkKCcjY29udGFjdElkJykuc3VibWl0KGZ1bmN0aW9uIChldmVudCkge1xuXG4gICAgICAgIC8vIGdldCB0aGUgZm9ybSBkYXRhXG4gICAgICAgIC8vIHRoZXJlIGFyZSBtYW55IHdheXMgdG8gZ2V0IHRoaXMgZGF0YSB1c2luZyBqUXVlcnkgKHlvdSBjYW4gdXNlIHRoZSBjbGFzcyBvciBpZCBhbHNvKVxuICAgICAgICB2YXIgZm9ybURhdGEgPSB7XG4gICAgICAgICAgICAnYW1lJzogJCgnaW5wdXRbbmFtZT1uYW1lXScpLnZhbCgpLFxuICAgICAgICAgICAgJ2VtYWlsJzogJCgnaW5wdXRbbmFtZT1lbWFpbF0nKS52YWwoKSxcbiAgICAgICAgICAgICdjb21tZW50cyc6ICQoJ2lucHV0W25hbWU9Y29tbWVudHNdJykudmFsKClcbiAgICAgICAgfTtcblxuICAgICAgICAvLyBwcm9jZXNzIHRoZSBmb3JtXG4gICAgICAgICQuYWpheCh7XG4gICAgICAgICAgICB0eXBlOiAnUE9TVCcsIC8vIGRlZmluZSB0aGUgdHlwZSBvZiBIVFRQIHZlcmIgd2Ugd2FudCB0byB1c2UgKFBPU1QgZm9yIG91ciBmb3JtKVxuICAgICAgICAgICAgdXJsOiAnL2NvbnRhY3QnLCAvLyB0aGUgdXJsIHdoZXJlIHdlIHdhbnQgdG8gUE9TVFxuICAgICAgICAgICAgZGF0YTogZm9ybURhdGEsIC8vIG91ciBkYXRhIG9iamVjdFxuICAgICAgICAgICAgZGF0YVR5cGU6ICdqc29uJywgLy8gd2hhdCB0eXBlIG9mIGRhdGEgZG8gd2UgZXhwZWN0IGJhY2sgZnJvbSB0aGUgc2VydmVyXG4gICAgICAgICAgICBlbmNvZGU6IHRydWVcbiAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAvLyB1c2luZyB0aGUgZG9uZSBwcm9taXNlIGNhbGxiYWNrXG4gICAgICAgICAgICAgICAgLmRvbmUoZnVuY3Rpb24gKGRhdGEpIHtcblxuICAgICAgICAgICAgICAgICAgICAvLyBsb2cgZGF0YSB0byB0aGUgY29uc29sZSBzbyB3ZSBjYW4gc2VlXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEpO1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIGhlcmUgd2Ugd2lsbCBoYW5kbGUgZXJyb3JzIGFuZCB2YWxpZGF0aW9uIG1lc3NhZ2VzXG4gICAgICAgICAgICAgICAgICAgIGlmICghZGF0YS5zdWNjZXNzKVxuICAgICAgICAgICAgICAgICAgICB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjY29udGFjdG1zZ1wiKS5odG1sKGRhdGEuZXJyb3JzLmVtYWlsKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8qICAgICAgICAgICAgICAgICAgICAgICAgLy8gaGFuZGxlIGVycm9ycyBmb3IgbmFtZSAtLS0tLS0tLS0tLS0tLS1cbiAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YS5lcnJvcnMubmFtZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNuYW1lLWdyb3VwJykuYWRkQ2xhc3MoJ2hhcy1lcnJvcicpOyAvLyBhZGQgdGhlIGVycm9yIGNsYXNzIHRvIHNob3cgcmVkIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgJCgnI25hbWUtZ3JvdXAnKS5hcHBlbmQoJzxkaXYgY2xhc3M9XCJoZWxwLWJsb2NrXCI+JyArIGRhdGEuZXJyb3JzLm5hbWUgKyAnPC9kaXY+Jyk7IC8vIGFkZCB0aGUgYWN0dWFsIGVycm9yIG1lc3NhZ2UgdW5kZXIgb3VyIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGhhbmRsZSBlcnJvcnMgZm9yIGVtYWlsIC0tLS0tLS0tLS0tLS0tLVxuICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkYXRhLmVycm9ycy5lbWFpbCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNlbWFpbC1ncm91cCcpLmFkZENsYXNzKCdoYXMtZXJyb3InKTsgLy8gYWRkIHRoZSBlcnJvciBjbGFzcyB0byBzaG93IHJlZCBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNlbWFpbC1ncm91cCcpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImhlbHAtYmxvY2tcIj4nICsgZGF0YS5lcnJvcnMuZW1haWwgKyAnPC9kaXY+Jyk7IC8vIGFkZCB0aGUgYWN0dWFsIGVycm9yIG1lc3NhZ2UgdW5kZXIgb3VyIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGhhbmRsZSBlcnJvcnMgZm9yIHN1cGVyaGVybyBhbGlhcyAtLS0tLS0tLS0tLS0tLS1cbiAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YS5lcnJvcnMuc3VwZXJoZXJvQWxpYXMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjc3VwZXJoZXJvLWdyb3VwJykuYWRkQ2xhc3MoJ2hhcy1lcnJvcicpOyAvLyBhZGQgdGhlIGVycm9yIGNsYXNzIHRvIHNob3cgcmVkIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgJCgnI3N1cGVyaGVyby1ncm91cCcpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImhlbHAtYmxvY2tcIj4nICsgZGF0YS5lcnJvcnMuc3VwZXJoZXJvQWxpYXMgKyAnPC9kaXY+Jyk7IC8vIGFkZCB0aGUgYWN0dWFsIGVycm9yIG1lc3NhZ2UgdW5kZXIgb3VyIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgfSovXG5cbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gQUxMIEdPT0QhIGp1c3Qgc2hvdyB0aGUgc3VjY2VzcyBtZXNzYWdlIVxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gJCgnZm9ybScpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImFsZXJ0IGFsZXJ0LXN1Y2Nlc3NcIj4nICsgZGF0YS5tZXNzYWdlICsgJzwvZGl2PicpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB1c3VhbGx5IGFmdGVyIGZvcm0gc3VibWlzc2lvbiwgeW91J2xsIHdhbnQgdG8gcmVkaXJlY3RcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHdpbmRvdy5sb2NhdGlvbiA9ICcvdGhhbmsteW91JzsgLy8gcmVkaXJlY3QgYSB1c2VyIHRvIGFub3RoZXIgcGFnZVxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gIGFsZXJ0KCdzdWNjZXNzJytkYXRhLm1lc3NhZ2UpOyAvLyBmb3Igbm93IHdlJ2xsIGp1c3QgYWxlcnQgdGhlIHVzZXJcblxuICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNjb250YWN0bXNnXCIpLmh0bWwoZGF0YS5tZXNzYWdlKTtcblxuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAvLyBzdG9wIHRoZSBmb3JtIGZyb20gc3VibWl0dGluZyB0aGUgbm9ybWFsIHdheSBhbmQgcmVmcmVzaGluZyB0aGUgcGFnZVxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIH0pO1xuXG5cblxuICAgICQoJyNlbnF1aXJ5Zm9ybScpLnN1Ym1pdChmdW5jdGlvbiAoZXZlbnQpIHtcblxuICAgICAvLyAgdmFyIHJlc3VsdCA9ICQoJyNlbnFpZCcpLnZhbCgpO1xuICAgICAgIHZhciBuYW1lID0gJCgnI25hbWUnKS52YWwoKTtcbiAgICAgICB2YXIgZW1haWw9JCgnI2VtYWlsJykudmFsKCk7XG4gICAgICAgdmFyIG1vYmlsZT0kKCcjbW9iaWxlJykudmFsKCk7XG4gICAgICAgdmFyIGNvbW1lbnRzPSQoJyNjb21tZW50cycpLnZhbCgpO1xuICAgICBcbiAgICAgICBcbiAgICAgICAvLyBhbGVydChcImhhaVwiKTtcblxuICAgICAgICAvLyBnZXQgdGhlIGZvcm0gZGF0YVxuICAgICAgICAvLyB0aGVyZSBhcmUgbWFueSB3YXlzIHRvIGdldCB0aGlzIGRhdGEgdXNpbmcgalF1ZXJ5ICh5b3UgY2FuIHVzZSB0aGUgY2xhc3Mgb3IgaWQgYWxzbylcbiAgICAgICAgdmFyIGZvcm1EYXRhID0ge1xuICAgICAgICAgICAgICduYW1lJzogbmFtZSxcbiAgICAgICAgICAgICAnZW1haWwnOiBlbWFpbCxcbiAgICAgICAgICAgJ2NvbW1lbnRzJzogY29tbWVudHMsXG4gICAgICAgICAgICAgJ21vYmlsZScgIDptb2JpbGVcbiAgICAgICAgfTtcbiAgICAgICAvLyBjb25zb2xlLmxvZyhcIkhlbGxvIDo6IFwiK2Zvcm1EYXRhKTtcbiAgICAgICAvLyBcbiAgICAgICAvLyBcbiAgICAgICAgLy8gcHJvY2VzcyB0aGUgZm9ybVxuICAgICAgICBcbiAgICAgICAvLyBhbGVydChmb3JtRGF0YS5uYW1lK1wiOjpcIitmb3JtRGF0YS5lbWFpbCArXCI6OlwiK2Zvcm1EYXRhLm1vYmlsZStcIjo6XCIrZm9ybURhdGEuY29tbWVudHMpO1xuICAgICAgICAkLmFqYXgoe1xuICAgICAgICAgICAgdHlwZTogJ1BPU1QnLCAvLyBkZWZpbmUgdGhlIHR5cGUgb2YgSFRUUCB2ZXJiIHdlIHdhbnQgdG8gdXNlIChQT1NUIGZvciBvdXIgZm9ybSlcbiAgICAgICAgICAgIHVybDogJy9jb250YWN0JywgLy8gdGhlIHVybCB3aGVyZSB3ZSB3YW50IHRvIFBPU1RcbiAgICAgICAgICAgIGRhdGE6IGZvcm1EYXRhLCAvLyBvdXIgZGF0YSBvYmplY3RcbiAgICAgICAgICAgIGRhdGFUeXBlOiAnanNvbicsIC8vIHdoYXQgdHlwZSBvZiBkYXRhIGRvIHdlIGV4cGVjdCBiYWNrIGZyb20gdGhlIHNlcnZlclxuICAgICAgICAgICAgZW5jb2RlOiB0cnVlXG4gICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLy8gdXNpbmcgdGhlIGRvbmUgcHJvbWlzZSBjYWxsYmFja1xuICAgICAgICAgICAgICAgIC5kb25lKGZ1bmN0aW9uIChkYXRhKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gbG9nIGRhdGEgdG8gdGhlIGNvbnNvbGUgc28gd2UgY2FuIHNlZVxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcblxuICAgICAgICAgICAgICAgICAgICAvLyBoZXJlIHdlIHdpbGwgaGFuZGxlIGVycm9ycyBhbmQgdmFsaWRhdGlvbiBtZXNzYWdlc1xuICAgICAgICAgICAgICAgICAgICBpZiAoIWRhdGEuc3VjY2VzcylcbiAgICAgICAgICAgICAgICAgICAge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAkKFwiI2NvbnRhY3Rtc2dcIikuaHRtbChkYXRhLm1lc3NhZ2UpO1xuICAgICAgICAgICAgICAgICAgICAgICAgLyogICAgICAgICAgICAgICAgICAgICAgICAvLyBoYW5kbGUgZXJyb3JzIGZvciBuYW1lIC0tLS0tLS0tLS0tLS0tLVxuICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkYXRhLmVycm9ycy5uYW1lKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgJCgnI25hbWUtZ3JvdXAnKS5hZGRDbGFzcygnaGFzLWVycm9yJyk7IC8vIGFkZCB0aGUgZXJyb3IgY2xhc3MgdG8gc2hvdyByZWQgaW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjbmFtZS1ncm91cCcpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImhlbHAtYmxvY2tcIj4nICsgZGF0YS5lcnJvcnMubmFtZSArICc8L2Rpdj4nKTsgLy8gYWRkIHRoZSBhY3R1YWwgZXJyb3IgbWVzc2FnZSB1bmRlciBvdXIgaW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICAgLy8gaGFuZGxlIGVycm9ycyBmb3IgZW1haWwgLS0tLS0tLS0tLS0tLS0tXG4gICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRhdGEuZXJyb3JzLmVtYWlsKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgJCgnI2VtYWlsLWdyb3VwJykuYWRkQ2xhc3MoJ2hhcy1lcnJvcicpOyAvLyBhZGQgdGhlIGVycm9yIGNsYXNzIHRvIHNob3cgcmVkIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgJCgnI2VtYWlsLWdyb3VwJykuYXBwZW5kKCc8ZGl2IGNsYXNzPVwiaGVscC1ibG9ja1wiPicgKyBkYXRhLmVycm9ycy5lbWFpbCArICc8L2Rpdj4nKTsgLy8gYWRkIHRoZSBhY3R1YWwgZXJyb3IgbWVzc2FnZSB1bmRlciBvdXIgaW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICAgLy8gaGFuZGxlIGVycm9ycyBmb3Igc3VwZXJoZXJvIGFsaWFzIC0tLS0tLS0tLS0tLS0tLVxuICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkYXRhLmVycm9ycy5zdXBlcmhlcm9BbGlhcykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNzdXBlcmhlcm8tZ3JvdXAnKS5hZGRDbGFzcygnaGFzLWVycm9yJyk7IC8vIGFkZCB0aGUgZXJyb3IgY2xhc3MgdG8gc2hvdyByZWQgaW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjc3VwZXJoZXJvLWdyb3VwJykuYXBwZW5kKCc8ZGl2IGNsYXNzPVwiaGVscC1ibG9ja1wiPicgKyBkYXRhLmVycm9ycy5zdXBlcmhlcm9BbGlhcyArICc8L2Rpdj4nKTsgLy8gYWRkIHRoZSBhY3R1YWwgZXJyb3IgbWVzc2FnZSB1bmRlciBvdXIgaW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICB9Ki9cblxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBBTEwgR09PRCEganVzdCBzaG93IHRoZSBzdWNjZXNzIG1lc3NhZ2UhXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyAkKCdmb3JtJykuYXBwZW5kKCc8ZGl2IGNsYXNzPVwiYWxlcnQgYWxlcnQtc3VjY2Vzc1wiPicgKyBkYXRhLm1lc3NhZ2UgKyAnPC9kaXY+Jyk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHVzdWFsbHkgYWZ0ZXIgZm9ybSBzdWJtaXNzaW9uLCB5b3UnbGwgd2FudCB0byByZWRpcmVjdFxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gd2luZG93LmxvY2F0aW9uID0gJy90aGFuay15b3UnOyAvLyByZWRpcmVjdCBhIHVzZXIgdG8gYW5vdGhlciBwYWdlXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyAgYWxlcnQoJ3N1Y2Nlc3MnK2RhdGEubWVzc2FnZSk7IC8vIGZvciBub3cgd2UnbGwganVzdCBhbGVydCB0aGUgdXNlclxuXG4gICAgICAgICAgICAgICAgICAgICAgICAkKFwiI2NvbnRhY3Rtc2dcIikuaHRtbChkYXRhLm1lc3NhZ2UpO1xuXG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgIC8vIHN0b3AgdGhlIGZvcm0gZnJvbSBzdWJtaXR0aW5nIHRoZSBub3JtYWwgd2F5IGFuZCByZWZyZXNoaW5nIHRoZSBwYWdlXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgfSk7XG5cblxuXG59KTtcblxuXG5cblxuXG5cbiIsIi8vIEEgJCggZG9jdW1lbnQgKS5yZWFkeSgpIGJsb2NrLlxuJCggZG9jdW1lbnQgKS5yZWFkeShmdW5jdGlvbigpIHtcbiAgICAkKFwiI3dheWFuYWRfMVwiKS5hZGRDbGFzcyhcImFjdGl2ZVwiKTtcblxufSk7IiwiJCggZG9jdW1lbnQgKS5yZWFkeShmdW5jdGlvbigpIFxue1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDw5OyBpKyspIFxuICAgIHtcbiAgICAgICAgLypcbiAgICAgICAgICBpZihpJTM9PTApXG4gICAgICAgICAgJChcIiNwYWNrYWdlX1wiK2kpLmFkZENsYXNzKFwiY2FyZFNldDBcIik7XG4gICAgICBcbiAgICAgICAgICBpZihpJTM9PTEpXG4gICAgICAgICAgJChcIiNwYWNrYWdlX1wiK2kpLmFkZENsYXNzKFwiY2FyZFNldDFcIik7XG4gICAgICBcbiAgICAgICAgICAgaWYoaSUzPT0yKVxuICAgICAgICAgICQoXCIjcGFja2FnZV9cIitpKS5hZGRDbGFzcyhcImNhcmRTZXQyXCIpO1xuKi9cbiAgICAgIFxuICAgIH1cbiAgIFxuXG59KTtcbiIsIiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKClcbntcbiAgICBcbiAgICAgICAgJChcIi5jYXJkLXRleHRcIikuYWRkQ2xhc3MoXCJ0ZXh0LWp1c3RpZnlcIik7XG4gICAgXG4gICAgXG4gICAgXG59KTsiXX0=

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFhX293bF9jYXJvdXNlbC9vd2xfY2Fyb3VzZWwuanMiLCJhYl9TbGlkZV9Gcm9tX0JvdHRvbS9zbGlkZV9ib3R0b20uanMiLCJhY19tb3ZlX2FuaW0vbW92ZV9hbmltLmpzIiwiYWRfZ29vZ2xlX21hcC9nb29nbGUtbWFwLmpzIiwiYWVfbW9iaWxlX21lbnUvbWVudV9tb2JpbGUuanMiLCJhZl9zdWJzY3JpcHRpb24vc3Vic2NyaXB0aW9uLmpzIiwiYWdfY29udGFjdC9jb250YWN0LmpzIiwiYWhfd2F5YW5hZC93YXlhbmFkLmpzIiwiYWlfcGFja2FnZS9wYWNrYWdlLmpzIiwiYWlfdGV4dGJhbGFuY2UvYmFsYW5jZS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUM3QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDWEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDaENBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDcktBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUN6QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3RFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNqS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ0pBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNuQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJhYV9ob21lX3BhZ2UuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgIFxuICAkKCcub3dsLWNhcm91c2VsJykub3dsQ2Fyb3VzZWwoe1xuICBcbiAgICAgLy8gQ1NTIFN0eWxlc1xuICAgIGJhc2VDbGFzcyA6IFwib3dsLWNhcm91c2VsXCIsXG4gICAgdGhlbWUgOiBcIm93bC10aGVtZVwiLFxuICAgIGxvb3A6dHJ1ZSxcbiAgICBhdXRvcGxheTp0cnVlLCAgICAgICAgXG4gICAgbWFyZ2luOjEwLFxuICAgIHJlc3BvbnNpdmVDbGFzczp0cnVlLFxuICAgIG5hdjp0cnVlLFxuICAgIHJld2luZDp0cnVlLFxuICAgIHJlc3BvbnNpdmU6e1xuICAgICAgICAwOntcbiAgICAgICAgICAgIGl0ZW1zOjEsXG4gICAgICAgICAgICBuYXY6dHJ1ZVxuICAgICAgICB9LFxuICAgICAgICA2MDA6e1xuICAgICAgICAgICAgaXRlbXM6MyxcbiAgICAgICAgICAgIG5hdjpmYWxzZVxuICAgICAgICB9LFxuICAgICAgICAxMDAwOntcbiAgICAgICAgICAgIGl0ZW1zOjMsXG4gICAgICAgICAgICBuYXY6dHJ1ZSxcbiAgICAgICAgICAgIGxvb3A6ZmFsc2VcbiAgICAgICAgfVxuICAgIH1cbn0pO1xuXG4iLCIgICQod2luZG93KS5zY3JvbGwoZnVuY3Rpb24oKSB7XG4gICQoXCIuc2xpZGVhbmltXCIpLmVhY2goZnVuY3Rpb24oKXtcbiAgICB2YXIgcG9zID0gJCh0aGlzKS5vZmZzZXQoKS50b3A7XG5cbiAgICB2YXIgd2luVG9wID0gJCh3aW5kb3cpLnNjcm9sbFRvcCgpO1xuICAgIGlmIChwb3MgPCB3aW5Ub3AgKyA2MDApIHtcbiAgICAgICQodGhpcykuYWRkQ2xhc3MoXCJzbGlkZVwiKTtcbiAgICB9XG4gIH0pO1xufSk7ICAgICAgICAgXG4gXG4iLCIkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKVxue1xuICAgIC8vIEFkZCBzbW9vdGggc2Nyb2xsaW5nIHRvIGFsbCBsaW5rcyBpbiBuYXZiYXIgKyBmb290ZXIgbGlua1xuICAgICQoXCIubmF2YmFyIGEsI215Rm9vdGVyIGFbaHJlZj0nI3BhY2thZ2UnXSwjbXlGb290ZXIgYVtocmVmPScjY29udGFjdCddLCNteUZvb3RlciBhW2hyZWY9JyN0b3BuYXZpZCddLCAjbXlGb290ZXIgYVtocmVmPScjd2F5YW5hZCddLCAjbXlGb290ZXIgYVtocmVmPScjcHJvZmlsZSddIFwiKS5vbignY2xpY2snLCBmdW5jdGlvbiAoZXZlbnQpIHtcblxuICAgICAgICAvLyBNYWtlIHN1cmUgdGhpcy5oYXNoIGhhcyBhIHZhbHVlIGJlZm9yZSBvdmVycmlkaW5nIGRlZmF1bHQgYmVoYXZpb3JcbiAgICAgICAgaWYgKHRoaXMuaGFzaCAhPT0gXCJcIilcbiAgICAgICAge1xuXG4gICAgICAgICAgICAvLyBQcmV2ZW50IGRlZmF1bHQgYW5jaG9yIGNsaWNrIGJlaGF2aW9yXG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgICAgICAvLyBTdG9yZSBoYXNoXG4gICAgICAgICAgICB2YXIgaGFzaCA9IHRoaXMuaGFzaDtcblxuICAgICAgICAgICAgLy8gVXNpbmcgalF1ZXJ5J3MgYW5pbWF0ZSgpIG1ldGhvZCB0byBhZGQgc21vb3RoIHBhZ2Ugc2Nyb2xsXG4gICAgICAgICAgICAvLyBUaGUgb3B0aW9uYWwgbnVtYmVyICg5MDApIHNwZWNpZmllcyB0aGUgbnVtYmVyIG9mIG1pbGxpc2Vjb25kcyBpdCB0YWtlcyB0byBzY3JvbGwgdG8gdGhlIHNwZWNpZmllZCBhcmVhXG4gICAgICAgICAgICAkKCdodG1sLCBib2R5JykuYW5pbWF0ZSh7XG4gICAgICAgICAgICAgICAgc2Nyb2xsVG9wOiAkKGhhc2gpLm9mZnNldCgpLnRvcFxuICAgICAgICAgICAgfSwgOTAwLCBmdW5jdGlvbiAoKSB7XG5cbiAgICAgICAgICAgICAgICAvLyBBZGQgaGFzaCAoIykgdG8gVVJMIHdoZW4gZG9uZSBzY3JvbGxpbmcgKGRlZmF1bHQgY2xpY2sgYmVoYXZpb3IpXG4gICAgICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLmhhc2ggPSBoYXNoO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0gLy8gRW5kIGlmXG4gICAgfSk7XG5cblxuXG5cblxuXG59KTsiLCJcbiBcblxuXG5cbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpXG57XG5cblxuICAgIGZ1bmN0aW9uIG15TWFwKCkge1xuXG4gICAgICAgIHZhciBteUNlbnRlciA9IG5ldyBnb29nbGUubWFwcy5MYXRMbmcoMTEuNjcwMCwgNzUuOTU3OCk7XG4gICAgICAgIHZhciBtYXBQcm9wID0ge2NlbnRlcjogbXlDZW50ZXIsIHpvb206IDEyLCBzY3JvbGx3aGVlbDogZmFsc2UsIGRyYWdnYWJsZTogZmFsc2UsIG1hcFR5cGVJZDogZ29vZ2xlLm1hcHMuTWFwVHlwZUlkLlJPQURNQVB9O1xuICAgICAgICB2YXIgbWFwID0gbmV3IGdvb2dsZS5tYXBzLk1hcChkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcIm1hcFwiKSwgbWFwUHJvcCk7XG4gICAgICAgIHZhciBtYXJrZXIgPSBuZXcgZ29vZ2xlLm1hcHMuTWFya2VyKHtwb3NpdGlvbjogbXlDZW50ZXJ9KTtcbiAgICAgICAgbWFya2VyLnNldE1hcChtYXApO1xuICAgIH1cblxuICAgIGdvb2dsZS5tYXBzLmV2ZW50LmFkZERvbUxpc3RlbmVyKHdpbmRvdywgJ2xvYWQnLCBpbml0TWFwLCB7cGFzc2l2ZTogdHJ1ZX0pO1xuXG5cblxuXG59KTtcblxuIFxuZnVuY3Rpb24gaW5pdE1hcCgpIHtcblxuICAgIC8vIENyZWF0ZSBhIG5ldyBTdHlsZWRNYXBUeXBlIG9iamVjdCwgcGFzc2luZyBpdCBhbiBhcnJheSBvZiBzdHlsZXMsXG4gICAgLy8gYW5kIHRoZSBuYW1lIHRvIGJlIGRpc3BsYXllZCBvbiB0aGUgbWFwIHR5cGUgY29udHJvbC5cbiAgICB2YXIgc3R5bGVkTWFwVHlwZSA9IG5ldyBnb29nbGUubWFwcy5TdHlsZWRNYXBUeXBlKFxuICAgICAgICAgICAgW1xuICAgICAgICAgICAgICAgIHtlbGVtZW50VHlwZTogJ2dlb21ldHJ5Jywgc3R5bGVyczogW3tjb2xvcjogJyNlYmUzY2QnfV19LFxuICAgICAgICAgICAgICAgIHtlbGVtZW50VHlwZTogJ2xhYmVscy50ZXh0LmZpbGwnLCBzdHlsZXJzOiBbe2NvbG9yOiAnIzUyMzczNSd9XX0sXG4gICAgICAgICAgICAgICAge2VsZW1lbnRUeXBlOiAnbGFiZWxzLnRleHQuc3Ryb2tlJywgc3R5bGVyczogW3tjb2xvcjogJyNmNWYxZTYnfV19LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICdhZG1pbmlzdHJhdGl2ZScsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnZ2VvbWV0cnkuc3Ryb2tlJyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNjOWIyYTYnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICdhZG1pbmlzdHJhdGl2ZS5sYW5kX3BhcmNlbCcsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnZ2VvbWV0cnkuc3Ryb2tlJyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNkY2QyYmUnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICdhZG1pbmlzdHJhdGl2ZS5sYW5kX3BhcmNlbCcsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnbGFiZWxzLnRleHQuZmlsbCcsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjYWU5ZTkwJ31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAnbGFuZHNjYXBlLm5hdHVyYWwnLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2dlb21ldHJ5JyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNkZmQyYWUnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICdwb2knLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2dlb21ldHJ5JyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNkZmQyYWUnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICdwb2knLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2xhYmVscy50ZXh0LmZpbGwnLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnIzkzODE3Yyd9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ3BvaS5wYXJrJyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdnZW9tZXRyeS5maWxsJyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNhNWIwNzYnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICdwb2kucGFyaycsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnbGFiZWxzLnRleHQuZmlsbCcsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjNDQ3NTMwJ31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAncm9hZCcsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnZ2VvbWV0cnknLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnI2Y1ZjFlNid9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ3JvYWQuYXJ0ZXJpYWwnLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2dlb21ldHJ5JyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNmZGZjZjgnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICdyb2FkLmhpZ2h3YXknLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2dlb21ldHJ5JyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNmOGM5NjcnfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICdyb2FkLmhpZ2h3YXknLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2dlb21ldHJ5LnN0cm9rZScsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjZTliYzYyJ31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAncm9hZC5oaWdod2F5LmNvbnRyb2xsZWRfYWNjZXNzJyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdnZW9tZXRyeScsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjZTk4ZDU4J31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAncm9hZC5oaWdod2F5LmNvbnRyb2xsZWRfYWNjZXNzJyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdnZW9tZXRyeS5zdHJva2UnLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnI2RiODU1NSd9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ3JvYWQubG9jYWwnLFxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50VHlwZTogJ2xhYmVscy50ZXh0LmZpbGwnLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnIzgwNmI2Myd9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ3RyYW5zaXQubGluZScsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnZ2VvbWV0cnknLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnI2RmZDJhZSd9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ3RyYW5zaXQubGluZScsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnbGFiZWxzLnRleHQuZmlsbCcsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjOGY3ZDc3J31dXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgIGZlYXR1cmVUeXBlOiAndHJhbnNpdC5saW5lJyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdsYWJlbHMudGV4dC5zdHJva2UnLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnI2ViZTNjZCd9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ3RyYW5zaXQuc3RhdGlvbicsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnZ2VvbWV0cnknLFxuICAgICAgICAgICAgICAgICAgICBzdHlsZXJzOiBbe2NvbG9yOiAnI2RmZDJhZSd9XVxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBmZWF0dXJlVHlwZTogJ3dhdGVyJyxcbiAgICAgICAgICAgICAgICAgICAgZWxlbWVudFR5cGU6ICdnZW9tZXRyeS5maWxsJyxcbiAgICAgICAgICAgICAgICAgICAgc3R5bGVyczogW3tjb2xvcjogJyNiOWQzYzInfV1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgZmVhdHVyZVR5cGU6ICd3YXRlcicsXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnRUeXBlOiAnbGFiZWxzLnRleHQuZmlsbCcsXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlcnM6IFt7Y29sb3I6ICcjOTI5OThkJ31dXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgXSxcbiAgICAgICAgICAgIHtuYW1lOiAnU3R5bGVkIE1hcCd9KTtcblxuICAgIC8vIENyZWF0ZSBhIG1hcCBvYmplY3QsIGFuZCBpbmNsdWRlIHRoZSBNYXBUeXBlSWQgdG8gYWRkXG4gICAgLy8gdG8gdGhlIG1hcCB0eXBlIGNvbnRyb2wuXG4gICAgdmFyIG1hcCA9IG5ldyBnb29nbGUubWFwcy5NYXAoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ21hcCcpLCB7XG4gICAgICAgIGNlbnRlcjoge2xhdDogMTEuNjcwMCwgbG5nOiA3NS45NTc4fSxcbiAgICAgICAgem9vbTogMTEsXG4gICAgICAgIG1hcFR5cGVDb250cm9sT3B0aW9uczoge1xuICAgICAgICAgICAgbWFwVHlwZUlkczogWydyb2FkbWFwJywgJ3NhdGVsbGl0ZScsICdoeWJyaWQnLCAndGVycmFpbicsXG4gICAgICAgICAgICAgICAgJ3N0eWxlZF9tYXAnXVxuICAgICAgICB9XG4gICAgfSk7XG5cbiAgICAvL0Fzc29jaWF0ZSB0aGUgc3R5bGVkIG1hcCB3aXRoIHRoZSBNYXBUeXBlSWQgYW5kIHNldCBpdCB0byBkaXNwbGF5LlxuICAgIG1hcC5tYXBUeXBlcy5zZXQoJ3N0eWxlZF9tYXAnLCBzdHlsZWRNYXBUeXBlKTtcbiAgICBtYXAuc2V0TWFwVHlwZUlkKCdzdHlsZWRfbWFwJyk7XG5cbiAgICB2YXIgbXlDZW50ZXIgPSBuZXcgZ29vZ2xlLm1hcHMuTGF0TG5nKDExLjY3MDAsIDc1Ljk1NzgpO1xuICAgIHZhciBtYXJrZXIgPSBuZXcgZ29vZ2xlLm1hcHMuTWFya2VyKHtwb3NpdGlvbjogbXlDZW50ZXJ9KTtcbiAgICBtYXJrZXIuc2V0TWFwKG1hcCk7XG4gICAgXG4gICAgIFxuXG4gICAgXG59IiwiICAgICQoJy5uYXZiYXItbmF2PmxpPmEnKS5vbignY2xpY2snLCBmdW5jdGlvbigpXG4gICAge1xuICAgICAgICAgICB2YXIgaWQgPSAkKHRoaXMpLmF0dHIoJ2lkJyk7XG4gICAgICAgICAgIC8vIGFsZXJ0KGlkKTtcbiAgICAgICAgICAgaWYoaWQgIT0gXCJhZG1pbl9uYXZkcm9wXCIpXG4gICAgICAgICAgICAgICAgICAkKCcubmF2YmFyLWNvbGxhcHNlJykuY29sbGFwc2UoJ2hpZGUnKTtcbiAgICB9KTtcbiAgICBcbiAgICAkKCcubmF2YmFyLWNvbGxhcHNlJykub24oJ3Nob3duLmJzLmNvbGxhcHNlJywgZnVuY3Rpb24oKVxuICAgIHtcbiAgICAgICAvL2FsZXJ0KFwiaGVsbG9cIik7XG4gICAgICAgICQoJyN0YWcnKS5odG1sKFwiTWVudVwiKTtcbiAgICAgICAgJCgnI3RhZycpLnJlbW92ZUNsYXNzKFwiZmEgZmEtYmFyc1wiKTtcbiAgICAgICAgJCgnI3RhZycpLmFkZENsYXNzKFwiZmEgZmEtdGltZXNcIik7XG4gICAgICAgIFxuICAgICAgIFxuICAgIH0pO1xuICAgIFxuICAgIFxuICAgICQoJy5uYXZiYXItY29sbGFwc2UnKS5vbignaGlkZGVuLmJzLmNvbGxhcHNlJywgZnVuY3Rpb24oKVxuICAgIHtcbiAgICAgIC8vICQoJyN0YWcnKS5odG1sKFwiT1wiKTtcbiAgICAgICAgICAgICAkKCcjdGFnJykuaHRtbChcIk1lbnVcIik7XG4gICAgICAgICAgICAkKCcjdGFnJykucmVtb3ZlQ2xhc3MoXCJmYSBmYS10aW1lc1wiKTtcbiAgICAgICAgICAgICAgJCgnI3RhZycpLmFkZENsYXNzKFwiZmEgZmEtYmFyc1wiKTtcbiAgICB9KTsiLCIvLyBtYWdpYy5qc1xuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkge1xuXG4gICAgLy8gcHJvY2VzcyB0aGUgZm9ybVxuICAgICQoJyNmb3Jtc3ViJykuc3VibWl0KGZ1bmN0aW9uIChldmVudCkge1xuXG4gICAgICAgIC8vIGdldCB0aGUgZm9ybSBkYXRhXG4gICAgICAgIC8vIHRoZXJlIGFyZSBtYW55IHdheXMgdG8gZ2V0IHRoaXMgZGF0YSB1c2luZyBqUXVlcnkgKHlvdSBjYW4gdXNlIHRoZSBjbGFzcyBvciBpZCBhbHNvKVxuICAgICAgICB2YXIgZm9ybURhdGEgPSB7XG4gICAgICAgICAvLyAgICduYW1lJzogJCgnaW5wdXRbbmFtZT1uYW1lXScpLnZhbCgpLFxuICAgICAgICAgICAgJ2VtYWlsJzogJCgnaW5wdXRbbmFtZT1lbWFpbF0nKS52YWwoKSxcbiAgICAgICAgICAvLyAgJ3N1cGVyaGVyb0FsaWFzJzogJCgnaW5wdXRbbmFtZT1zdXBlcmhlcm9BbGlhc10nKS52YWwoKVxuICAgICAgICB9O1xuXG4gICAgICAgIC8vIHByb2Nlc3MgdGhlIGZvcm1cbiAgICAgICAgJC5hamF4KHtcbiAgICAgICAgICAgIHR5cGU6ICdQT1NUJywgLy8gZGVmaW5lIHRoZSB0eXBlIG9mIEhUVFAgdmVyYiB3ZSB3YW50IHRvIHVzZSAoUE9TVCBmb3Igb3VyIGZvcm0pXG4gICAgICAgICAgICB1cmw6ICcvc3VicycsIC8vIHRoZSB1cmwgd2hlcmUgd2Ugd2FudCB0byBQT1NUXG4gICAgICAgICAgICBkYXRhOiBmb3JtRGF0YSwgLy8gb3VyIGRhdGEgb2JqZWN0XG4gICAgICAgICAgICBkYXRhVHlwZTogJ2pzb24nLCAvLyB3aGF0IHR5cGUgb2YgZGF0YSBkbyB3ZSBleHBlY3QgYmFjayBmcm9tIHRoZSBzZXJ2ZXJcbiAgICAgICAgICAgIGVuY29kZTogdHJ1ZVxuICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC8vIHVzaW5nIHRoZSBkb25lIHByb21pc2UgY2FsbGJhY2tcbiAgICAgICAgICAgICAgICAuZG9uZShmdW5jdGlvbiAoZGF0YSkge1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIGxvZyBkYXRhIHRvIHRoZSBjb25zb2xlIHNvIHdlIGNhbiBzZWVcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YSk7XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gaGVyZSB3ZSB3aWxsIGhhbmRsZSBlcnJvcnMgYW5kIHZhbGlkYXRpb24gbWVzc2FnZXNcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFkYXRhLnN1Y2Nlc3MpIFxuICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNzdWJtc2dcIikuaHRtbChkYXRhLmVycm9ycy5lbWFpbCk7XG4vKiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGhhbmRsZSBlcnJvcnMgZm9yIG5hbWUgLS0tLS0tLS0tLS0tLS0tXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YS5lcnJvcnMubmFtZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNuYW1lLWdyb3VwJykuYWRkQ2xhc3MoJ2hhcy1lcnJvcicpOyAvLyBhZGQgdGhlIGVycm9yIGNsYXNzIHRvIHNob3cgcmVkIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJCgnI25hbWUtZ3JvdXAnKS5hcHBlbmQoJzxkaXYgY2xhc3M9XCJoZWxwLWJsb2NrXCI+JyArIGRhdGEuZXJyb3JzLm5hbWUgKyAnPC9kaXY+Jyk7IC8vIGFkZCB0aGUgYWN0dWFsIGVycm9yIG1lc3NhZ2UgdW5kZXIgb3VyIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIGhhbmRsZSBlcnJvcnMgZm9yIGVtYWlsIC0tLS0tLS0tLS0tLS0tLVxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRhdGEuZXJyb3JzLmVtYWlsKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJCgnI2VtYWlsLWdyb3VwJykuYWRkQ2xhc3MoJ2hhcy1lcnJvcicpOyAvLyBhZGQgdGhlIGVycm9yIGNsYXNzIHRvIHNob3cgcmVkIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJCgnI2VtYWlsLWdyb3VwJykuYXBwZW5kKCc8ZGl2IGNsYXNzPVwiaGVscC1ibG9ja1wiPicgKyBkYXRhLmVycm9ycy5lbWFpbCArICc8L2Rpdj4nKTsgLy8gYWRkIHRoZSBhY3R1YWwgZXJyb3IgbWVzc2FnZSB1bmRlciBvdXIgaW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gaGFuZGxlIGVycm9ycyBmb3Igc3VwZXJoZXJvIGFsaWFzIC0tLS0tLS0tLS0tLS0tLVxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRhdGEuZXJyb3JzLnN1cGVyaGVyb0FsaWFzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJCgnI3N1cGVyaGVyby1ncm91cCcpLmFkZENsYXNzKCdoYXMtZXJyb3InKTsgLy8gYWRkIHRoZSBlcnJvciBjbGFzcyB0byBzaG93IHJlZCBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNzdXBlcmhlcm8tZ3JvdXAnKS5hcHBlbmQoJzxkaXYgY2xhc3M9XCJoZWxwLWJsb2NrXCI+JyArIGRhdGEuZXJyb3JzLnN1cGVyaGVyb0FsaWFzICsgJzwvZGl2PicpOyAvLyBhZGQgdGhlIGFjdHVhbCBlcnJvciBtZXNzYWdlIHVuZGVyIG91ciBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgfSovXG5cbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gQUxMIEdPT0QhIGp1c3Qgc2hvdyB0aGUgc3VjY2VzcyBtZXNzYWdlIVxuICAgICAgICAgICAgICAgICAgICAgICAvLyAkKCdmb3JtJykuYXBwZW5kKCc8ZGl2IGNsYXNzPVwiYWxlcnQgYWxlcnQtc3VjY2Vzc1wiPicgKyBkYXRhLm1lc3NhZ2UgKyAnPC9kaXY+Jyk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHVzdWFsbHkgYWZ0ZXIgZm9ybSBzdWJtaXNzaW9uLCB5b3UnbGwgd2FudCB0byByZWRpcmVjdFxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gd2luZG93LmxvY2F0aW9uID0gJy90aGFuay15b3UnOyAvLyByZWRpcmVjdCBhIHVzZXIgdG8gYW5vdGhlciBwYWdlXG4gICAgICAgICAgICAgICAgICAgICAgLy8gIGFsZXJ0KCdzdWNjZXNzJytkYXRhLm1lc3NhZ2UpOyAvLyBmb3Igbm93IHdlJ2xsIGp1c3QgYWxlcnQgdGhlIHVzZXJcbiAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgICAkKFwiI3N1Ym1zZ1wiKS5odG1sKGRhdGEubWVzc2FnZSk7XG5cbiAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgLy8gc3RvcCB0aGUgZm9ybSBmcm9tIHN1Ym1pdHRpbmcgdGhlIG5vcm1hbCB3YXkgYW5kIHJlZnJlc2hpbmcgdGhlIHBhZ2VcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICB9KTtcblxufSk7IiwiLy8gbWFnaWMuanNcbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcblxuICAgIC8vIHByb2Nlc3MgdGhlIGZvcm1cbiAgICAkKCcjY29udGFjdElkJykuc3VibWl0KGZ1bmN0aW9uIChldmVudCkge1xuXG4gICAgICAgIC8vIGdldCB0aGUgZm9ybSBkYXRhXG4gICAgICAgIC8vIHRoZXJlIGFyZSBtYW55IHdheXMgdG8gZ2V0IHRoaXMgZGF0YSB1c2luZyBqUXVlcnkgKHlvdSBjYW4gdXNlIHRoZSBjbGFzcyBvciBpZCBhbHNvKVxuICAgICAgICB2YXIgZm9ybURhdGEgPSB7XG4gICAgICAgICAgICAnYW1lJzogJCgnaW5wdXRbbmFtZT1uYW1lXScpLnZhbCgpLFxuICAgICAgICAgICAgJ2VtYWlsJzogJCgnaW5wdXRbbmFtZT1lbWFpbF0nKS52YWwoKSxcbiAgICAgICAgICAgICdjb21tZW50cyc6ICQoJ2lucHV0W25hbWU9Y29tbWVudHNdJykudmFsKClcbiAgICAgICAgfTtcblxuICAgICAgICAvLyBwcm9jZXNzIHRoZSBmb3JtXG4gICAgICAgICQuYWpheCh7XG4gICAgICAgICAgICB0eXBlOiAnUE9TVCcsIC8vIGRlZmluZSB0aGUgdHlwZSBvZiBIVFRQIHZlcmIgd2Ugd2FudCB0byB1c2UgKFBPU1QgZm9yIG91ciBmb3JtKVxuICAgICAgICAgICAgdXJsOiAnL2NvbnRhY3QnLCAvLyB0aGUgdXJsIHdoZXJlIHdlIHdhbnQgdG8gUE9TVFxuICAgICAgICAgICAgZGF0YTogZm9ybURhdGEsIC8vIG91ciBkYXRhIG9iamVjdFxuICAgICAgICAgICAgZGF0YVR5cGU6ICdqc29uJywgLy8gd2hhdCB0eXBlIG9mIGRhdGEgZG8gd2UgZXhwZWN0IGJhY2sgZnJvbSB0aGUgc2VydmVyXG4gICAgICAgICAgICBlbmNvZGU6IHRydWVcbiAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAvLyB1c2luZyB0aGUgZG9uZSBwcm9taXNlIGNhbGxiYWNrXG4gICAgICAgICAgICAgICAgLmRvbmUoZnVuY3Rpb24gKGRhdGEpIHtcblxuICAgICAgICAgICAgICAgICAgICAvLyBsb2cgZGF0YSB0byB0aGUgY29uc29sZSBzbyB3ZSBjYW4gc2VlXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEpO1xuXG4gICAgICAgICAgICAgICAgICAgIC8vIGhlcmUgd2Ugd2lsbCBoYW5kbGUgZXJyb3JzIGFuZCB2YWxpZGF0aW9uIG1lc3NhZ2VzXG4gICAgICAgICAgICAgICAgICAgIGlmICghZGF0YS5zdWNjZXNzKVxuICAgICAgICAgICAgICAgICAgICB7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjY29udGFjdG1zZ1wiKS5odG1sKGRhdGEuZXJyb3JzLmVtYWlsKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8qICAgICAgICAgICAgICAgICAgICAgICAgLy8gaGFuZGxlIGVycm9ycyBmb3IgbmFtZSAtLS0tLS0tLS0tLS0tLS1cbiAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YS5lcnJvcnMubmFtZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNuYW1lLWdyb3VwJykuYWRkQ2xhc3MoJ2hhcy1lcnJvcicpOyAvLyBhZGQgdGhlIGVycm9yIGNsYXNzIHRvIHNob3cgcmVkIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgJCgnI25hbWUtZ3JvdXAnKS5hcHBlbmQoJzxkaXYgY2xhc3M9XCJoZWxwLWJsb2NrXCI+JyArIGRhdGEuZXJyb3JzLm5hbWUgKyAnPC9kaXY+Jyk7IC8vIGFkZCB0aGUgYWN0dWFsIGVycm9yIG1lc3NhZ2UgdW5kZXIgb3VyIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGhhbmRsZSBlcnJvcnMgZm9yIGVtYWlsIC0tLS0tLS0tLS0tLS0tLVxuICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkYXRhLmVycm9ycy5lbWFpbCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNlbWFpbC1ncm91cCcpLmFkZENsYXNzKCdoYXMtZXJyb3InKTsgLy8gYWRkIHRoZSBlcnJvciBjbGFzcyB0byBzaG93IHJlZCBpbnB1dFxuICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNlbWFpbC1ncm91cCcpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImhlbHAtYmxvY2tcIj4nICsgZGF0YS5lcnJvcnMuZW1haWwgKyAnPC9kaXY+Jyk7IC8vIGFkZCB0aGUgYWN0dWFsIGVycm9yIG1lc3NhZ2UgdW5kZXIgb3VyIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGhhbmRsZSBlcnJvcnMgZm9yIHN1cGVyaGVybyBhbGlhcyAtLS0tLS0tLS0tLS0tLS1cbiAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoZGF0YS5lcnJvcnMuc3VwZXJoZXJvQWxpYXMpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjc3VwZXJoZXJvLWdyb3VwJykuYWRkQ2xhc3MoJ2hhcy1lcnJvcicpOyAvLyBhZGQgdGhlIGVycm9yIGNsYXNzIHRvIHNob3cgcmVkIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgJCgnI3N1cGVyaGVyby1ncm91cCcpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImhlbHAtYmxvY2tcIj4nICsgZGF0YS5lcnJvcnMuc3VwZXJoZXJvQWxpYXMgKyAnPC9kaXY+Jyk7IC8vIGFkZCB0aGUgYWN0dWFsIGVycm9yIG1lc3NhZ2UgdW5kZXIgb3VyIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgfSovXG5cbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gQUxMIEdPT0QhIGp1c3Qgc2hvdyB0aGUgc3VjY2VzcyBtZXNzYWdlIVxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gJCgnZm9ybScpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImFsZXJ0IGFsZXJ0LXN1Y2Nlc3NcIj4nICsgZGF0YS5tZXNzYWdlICsgJzwvZGl2PicpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB1c3VhbGx5IGFmdGVyIGZvcm0gc3VibWlzc2lvbiwgeW91J2xsIHdhbnQgdG8gcmVkaXJlY3RcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHdpbmRvdy5sb2NhdGlvbiA9ICcvdGhhbmsteW91JzsgLy8gcmVkaXJlY3QgYSB1c2VyIHRvIGFub3RoZXIgcGFnZVxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gIGFsZXJ0KCdzdWNjZXNzJytkYXRhLm1lc3NhZ2UpOyAvLyBmb3Igbm93IHdlJ2xsIGp1c3QgYWxlcnQgdGhlIHVzZXJcblxuICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNjb250YWN0bXNnXCIpLmh0bWwoZGF0YS5tZXNzYWdlKTtcblxuICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAvLyBzdG9wIHRoZSBmb3JtIGZyb20gc3VibWl0dGluZyB0aGUgbm9ybWFsIHdheSBhbmQgcmVmcmVzaGluZyB0aGUgcGFnZVxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIH0pO1xuXG5cblxuICAgICQoJyNlbnF1aXJ5Zm9ybScpLnN1Ym1pdChmdW5jdGlvbiAoZXZlbnQpIHtcblxuICAgICAvLyAgdmFyIHJlc3VsdCA9ICQoJyNlbnFpZCcpLnZhbCgpO1xuICAgICAgIHZhciBuYW1lID0gJCgnI25hbWUnKS52YWwoKTtcbiAgICAgICB2YXIgZW1haWw9JCgnI2VtYWlsJykudmFsKCk7XG4gICAgICAgdmFyIG1vYmlsZT0kKCcjbW9iaWxlJykudmFsKCk7XG4gICAgICAgdmFyIGNvbW1lbnRzPSQoJyNjb21tZW50cycpLnZhbCgpO1xuICAgICBcbiAgICAgICBcbiAgICAgICAvLyBhbGVydChcImhhaVwiKTtcblxuICAgICAgICAvLyBnZXQgdGhlIGZvcm0gZGF0YVxuICAgICAgICAvLyB0aGVyZSBhcmUgbWFueSB3YXlzIHRvIGdldCB0aGlzIGRhdGEgdXNpbmcgalF1ZXJ5ICh5b3UgY2FuIHVzZSB0aGUgY2xhc3Mgb3IgaWQgYWxzbylcbiAgICAgICAgdmFyIGZvcm1EYXRhID0ge1xuICAgICAgICAgICAgICduYW1lJzogbmFtZSxcbiAgICAgICAgICAgICAnZW1haWwnOiBlbWFpbCxcbiAgICAgICAgICAgJ2NvbW1lbnRzJzogY29tbWVudHMsXG4gICAgICAgICAgICAgJ21vYmlsZScgIDptb2JpbGVcbiAgICAgICAgfTtcbiAgICAgICAvLyBjb25zb2xlLmxvZyhcIkhlbGxvIDo6IFwiK2Zvcm1EYXRhKTtcbiAgICAgICAvLyBcbiAgICAgICAvLyBcbiAgICAgICAgLy8gcHJvY2VzcyB0aGUgZm9ybVxuICAgICAgICBcbiAgICAgICAvLyBhbGVydChmb3JtRGF0YS5uYW1lK1wiOjpcIitmb3JtRGF0YS5lbWFpbCArXCI6OlwiK2Zvcm1EYXRhLm1vYmlsZStcIjo6XCIrZm9ybURhdGEuY29tbWVudHMpO1xuICAgICAgICAkLmFqYXgoe1xuICAgICAgICAgICAgdHlwZTogJ1BPU1QnLCAvLyBkZWZpbmUgdGhlIHR5cGUgb2YgSFRUUCB2ZXJiIHdlIHdhbnQgdG8gdXNlIChQT1NUIGZvciBvdXIgZm9ybSlcbiAgICAgICAgICAgIHVybDogJy9jb250YWN0JywgLy8gdGhlIHVybCB3aGVyZSB3ZSB3YW50IHRvIFBPU1RcbiAgICAgICAgICAgIGRhdGE6IGZvcm1EYXRhLCAvLyBvdXIgZGF0YSBvYmplY3RcbiAgICAgICAgICAgIGRhdGFUeXBlOiAnanNvbicsIC8vIHdoYXQgdHlwZSBvZiBkYXRhIGRvIHdlIGV4cGVjdCBiYWNrIGZyb20gdGhlIHNlcnZlclxuICAgICAgICAgICAgZW5jb2RlOiB0cnVlXG4gICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLy8gdXNpbmcgdGhlIGRvbmUgcHJvbWlzZSBjYWxsYmFja1xuICAgICAgICAgICAgICAgIC5kb25lKGZ1bmN0aW9uIChkYXRhKSB7XG5cbiAgICAgICAgICAgICAgICAgICAgLy8gbG9nIGRhdGEgdG8gdGhlIGNvbnNvbGUgc28gd2UgY2FuIHNlZVxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcblxuICAgICAgICAgICAgICAgICAgICAvLyBoZXJlIHdlIHdpbGwgaGFuZGxlIGVycm9ycyBhbmQgdmFsaWRhdGlvbiBtZXNzYWdlc1xuICAgICAgICAgICAgICAgICAgICBpZiAoIWRhdGEuc3VjY2VzcylcbiAgICAgICAgICAgICAgICAgICAge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAkKFwiI2NvbnRhY3Rtc2dcIikuaHRtbChkYXRhLm1lc3NhZ2UpO1xuICAgICAgICAgICAgICAgICAgICAgICAgLyogICAgICAgICAgICAgICAgICAgICAgICAvLyBoYW5kbGUgZXJyb3JzIGZvciBuYW1lIC0tLS0tLS0tLS0tLS0tLVxuICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkYXRhLmVycm9ycy5uYW1lKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgJCgnI25hbWUtZ3JvdXAnKS5hZGRDbGFzcygnaGFzLWVycm9yJyk7IC8vIGFkZCB0aGUgZXJyb3IgY2xhc3MgdG8gc2hvdyByZWQgaW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjbmFtZS1ncm91cCcpLmFwcGVuZCgnPGRpdiBjbGFzcz1cImhlbHAtYmxvY2tcIj4nICsgZGF0YS5lcnJvcnMubmFtZSArICc8L2Rpdj4nKTsgLy8gYWRkIHRoZSBhY3R1YWwgZXJyb3IgbWVzc2FnZSB1bmRlciBvdXIgaW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICAgLy8gaGFuZGxlIGVycm9ycyBmb3IgZW1haWwgLS0tLS0tLS0tLS0tLS0tXG4gICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGRhdGEuZXJyb3JzLmVtYWlsKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgJCgnI2VtYWlsLWdyb3VwJykuYWRkQ2xhc3MoJ2hhcy1lcnJvcicpOyAvLyBhZGQgdGhlIGVycm9yIGNsYXNzIHRvIHNob3cgcmVkIGlucHV0XG4gICAgICAgICAgICAgICAgICAgICAgICAgJCgnI2VtYWlsLWdyb3VwJykuYXBwZW5kKCc8ZGl2IGNsYXNzPVwiaGVscC1ibG9ja1wiPicgKyBkYXRhLmVycm9ycy5lbWFpbCArICc8L2Rpdj4nKTsgLy8gYWRkIHRoZSBhY3R1YWwgZXJyb3IgbWVzc2FnZSB1bmRlciBvdXIgaW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICAgXG4gICAgICAgICAgICAgICAgICAgICAgICAgLy8gaGFuZGxlIGVycm9ycyBmb3Igc3VwZXJoZXJvIGFsaWFzIC0tLS0tLS0tLS0tLS0tLVxuICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkYXRhLmVycm9ycy5zdXBlcmhlcm9BbGlhcykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICQoJyNzdXBlcmhlcm8tZ3JvdXAnKS5hZGRDbGFzcygnaGFzLWVycm9yJyk7IC8vIGFkZCB0aGUgZXJyb3IgY2xhc3MgdG8gc2hvdyByZWQgaW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjc3VwZXJoZXJvLWdyb3VwJykuYXBwZW5kKCc8ZGl2IGNsYXNzPVwiaGVscC1ibG9ja1wiPicgKyBkYXRhLmVycm9ycy5zdXBlcmhlcm9BbGlhcyArICc8L2Rpdj4nKTsgLy8gYWRkIHRoZSBhY3R1YWwgZXJyb3IgbWVzc2FnZSB1bmRlciBvdXIgaW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgICB9Ki9cblxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBBTEwgR09PRCEganVzdCBzaG93IHRoZSBzdWNjZXNzIG1lc3NhZ2UhXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyAkKCdmb3JtJykuYXBwZW5kKCc8ZGl2IGNsYXNzPVwiYWxlcnQgYWxlcnQtc3VjY2Vzc1wiPicgKyBkYXRhLm1lc3NhZ2UgKyAnPC9kaXY+Jyk7XG5cbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIHVzdWFsbHkgYWZ0ZXIgZm9ybSBzdWJtaXNzaW9uLCB5b3UnbGwgd2FudCB0byByZWRpcmVjdFxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gd2luZG93LmxvY2F0aW9uID0gJy90aGFuay15b3UnOyAvLyByZWRpcmVjdCBhIHVzZXIgdG8gYW5vdGhlciBwYWdlXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyAgYWxlcnQoJ3N1Y2Nlc3MnK2RhdGEubWVzc2FnZSk7IC8vIGZvciBub3cgd2UnbGwganVzdCBhbGVydCB0aGUgdXNlclxuXG4gICAgICAgICAgICAgICAgICAgICAgICAkKFwiI2NvbnRhY3Rtc2dcIikuaHRtbChkYXRhLm1lc3NhZ2UpO1xuXG4gICAgICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgIC8vIHN0b3AgdGhlIGZvcm0gZnJvbSBzdWJtaXR0aW5nIHRoZSBub3JtYWwgd2F5IGFuZCByZWZyZXNoaW5nIHRoZSBwYWdlXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgfSk7XG5cblxuXG59KTtcblxuXG5cblxuXG5cbiIsIi8vIEEgJCggZG9jdW1lbnQgKS5yZWFkeSgpIGJsb2NrLlxuJCggZG9jdW1lbnQgKS5yZWFkeShmdW5jdGlvbigpIHtcbiAgICAkKFwiI3dheWFuYWRfMVwiKS5hZGRDbGFzcyhcImFjdGl2ZVwiKTtcblxufSk7IiwiJCggZG9jdW1lbnQgKS5yZWFkeShmdW5jdGlvbigpIFxue1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDw5OyBpKyspIFxuICAgIHtcbiAgICAgICAgLypcbiAgICAgICAgICBpZihpJTM9PTApXG4gICAgICAgICAgJChcIiNwYWNrYWdlX1wiK2kpLmFkZENsYXNzKFwiY2FyZFNldDBcIik7XG4gICAgICBcbiAgICAgICAgICBpZihpJTM9PTEpXG4gICAgICAgICAgJChcIiNwYWNrYWdlX1wiK2kpLmFkZENsYXNzKFwiY2FyZFNldDFcIik7XG4gICAgICBcbiAgICAgICAgICAgaWYoaSUzPT0yKVxuICAgICAgICAgICQoXCIjcGFja2FnZV9cIitpKS5hZGRDbGFzcyhcImNhcmRTZXQyXCIpO1xuKi9cbiAgICAgIFxuICAgIH1cbiAgIFxuXG59KTtcbiIsIiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKClcbntcbiAgICBcbiAgICAgICAgJChcIi5jYXJkLXRleHRcIikuYWRkQ2xhc3MoXCJ0ZXh0LWp1c3RpZnlcIik7XG4gICAgXG4gICAgXG4gICAgXG59KTsiXX0=
