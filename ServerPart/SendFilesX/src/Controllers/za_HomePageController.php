<?php

namespace tour\Controllers;
use tour\Validation\Validator;
//authors decided file level directives
use duncan3dc\Laravel\BladeInstance;
use Illuminate\Database\Capsule\Manager as DB;
use tour\email_send\Semail;

class za_HomePageController extends BaseController {
    
    
   
    public function getShowHomePage() {


        
        
       //  DB::select('UPDATE ViewCount SET ViewCount = ViewCount+1');

       DB::statement('UPDATE ViewCount SET ViewCount = ViewCount+1');


        $galleryWayanad = DB::select('SELECT * FROM galleryWayanad WHERE status = :status', array(
                    'status' => 0
        ));

        $galleryProfile = DB::select('SELECT * FROM galleryProfile WHERE status = :status', array(
                    'status' => 0
        ));

        $galleryPackage = DB::select('SELECT * FROM galleryPackage');


        $code = $this->blade->render('aa_ServerPart.aa_WorkSpace.aa_HOME_PAGE.ac_home_page', [
            'page_name' => '#home-page',
            'galleryWayanad' => $galleryWayanad,
            'galleryProfile' => $galleryProfile,
            'galleryPackage' => $galleryPackage
        ]);





        echo $code;
    }
    
    
    public function getHit() {


        
        
          

        $Table = DB::select('SELECT * FROM ViewCount');
    
       

        echo $Table[0]->ViewCount;
    }
 

    public function postSubsHomePage() {
        
        // dd("Hello");

         
           

         


         



        $errors = array();      // array to hold validation errors
        $data = array();      // array to pass back data
// validate the variables ======================================================
        // if any of these variables don't exist, add an error to our $errors array

    

        if (empty($_POST['email']))
            $errors['email'] = 'Email is required.';
        //dd($error);
        else
        {
                      $message = '';

          $okay = true;
          $email =$_POST['email'];



          $user =  DB::select('SELECT * FROM Subscription WHERE email = :email',
          array(
          'email'  => $email
          )
          );

          if ($user == null)
          {
            DB::statement('INSERT INTO Subscription (email) VALUES (:email)',
                                    array(
                                            'email'     => $email ,
                                          
                                         )
                     );

          $message= $message."<br> Added Mail for Subscription  " .$email;


          }
          else
          {
          $okay = false;
          
          
          $message= $message."<br> Already registered  ";
          }
          
            

            
        }
 

// return a response ===========================================================
        // if there are any errors in our errors array, return a success boolean of false
        if (!empty($errors)) {

            // if there are items in our errors array, return those errors
            $data['success'] = false;
            $data['errors'] = $errors;
        } else {

            // if there are no errors process our form, then return a message
            // DO ALL YOUR FORM PROCESSING HERE
            // THIS CAN BE WHATEVER YOU WANT TO DO (LOGIN, SAVE, UPDATE, WHATEVER)
            // show a message of success and provide a true success variable
            $data['success'] = true;
            $data['message'] = $message;
        }

        // return all our data to an AJAX call
        echo json_encode($data);
    }
    
    
    public function postContactHomePage() 
    {
           $errors = array();      // array to hold validation errors
        $data = array(); 
        
            
        //dd($_POST['comments']);  
        

        $validation_data = [
          'name' => 'min:3',
          'comments' => 'min:10',
        ];
        
       $Name = $_POST['name'];
       $Email=$_POST['email'];
       $Mobile=$_POST['mobile'];
       $Comments=$_POST['comments'];
 
        
      
 
        
    

        // validate data
        $validator = new Validator();

        $errors = $validator->isValid($validation_data);

        // if validation fails, go back to register
        // page and display error message

        if (sizeof($errors) > 0)
        {
             $data['success'] = false;  
             $data['message']= $errors;
            
             echo json_encode($data);
         
             exit();
        }
        
        
     
      
       $message = $this->blade->render('aa_ServerPart.aa_WorkSpace.emails.enquiry-email',
            [  'name' => $Name ,
               'mobile' =>$Mobile,
                'email' => $Email,
                'message'=>$Comments
               
               
             ]
        );
               
   
        Semail::_semail(getenv('ENQ_DEST_HOST'), "new Enquiry", $message);
   
      
        
             $data['success'] = true;  
             $data['message']= "Your enquiry is Registered";

                    echo json_encode($data);
 
         
    }

}

?>
