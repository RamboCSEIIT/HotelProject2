<?php


use Phinx\Migration\AbstractMigration;

class GalleryWayanad extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
         $this->execute("
             CREATE TABLE `galleryWayanad` (
                                    `id` int(11) NOT NULL AUTO_INCREMENT,
                                    `image_link` varchar(255) NOT NULL,
                                    `status` int(11) NOT NULL DEFAULT 0,
                                     `heading`  varchar(1055) NOT NULL DEFAULT 'heading',
                                     `description`  varchar(1055) NOT NULL DEFAULT 'description',                                     
                                    `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                    `updated_at` datetime DEFAULT NULL,
                                     PRIMARY KEY (`id`)
                                                                    ) 
        ");   

    }
}
