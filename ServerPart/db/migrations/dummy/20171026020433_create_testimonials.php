<?php


use Phinx\Migration\AbstractMigration;

class CreateTestimonials extends AbstractMigration
{
     
    public function up()
    {
 // create the table
        $testimonials = $this->table('testimonials');
        $testimonials->addColumn('title', 'string')
              ->addColumn('testimonial', 'text')
              ->addColumn('user_id', 'integer')      
              ->addForeignKey('user_id','users','id',['delete'=>'cascade','update'=>'cascade'])                 
              ->addColumn('created_at', 'datetime',['default'=>'CURRENT_TIMESTAMP'])
              ->addColumn('updated_at', 'datetime',['null'=>true])  
              ->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('testimonials');
    }
}
