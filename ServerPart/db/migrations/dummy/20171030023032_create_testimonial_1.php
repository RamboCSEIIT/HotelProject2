<?php


use Phinx\Migration\AbstractMigration;

class CreateTestimonial extends AbstractMigration
{
   //https://www.info.teradata.com/HTMLPubs/DB_TTU_16_00/index.html#page/Database_Management/B035-1094-160K/fti1472240591762.html  
     public function up()
    {
                 $this->execute("
             CREATE TABLE `testimonials` (
                                            `id` int(11) NOT NULL AUTO_INCREMENT,
                                            `title` varchar(255) NOT NULL,
                                            `testimonial` text NOT NULL,
                                            `user_id` int(11) NOT NULL,
                                            `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                            `updated_at` datetime DEFAULT NULL,
                                             PRIMARY KEY (`id`),
                                             KEY `user_id` (`user_id`),
                                             CONSTRAINT `forign_cons` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                                  ) 
        ");
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        
        $this->execute(" 
            ALTER TABLE testimonials DROP FOREIGN KEY forign_cons
         ");
        // $this->table('testimonials')->dropForeignKey('user_id')->save();
       
        $this->execute(" 
            DROP TABLE testimonials
         ");
    }
}
