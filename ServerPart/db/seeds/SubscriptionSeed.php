<?php


use Phinx\Seed\AbstractSeed;

class SubscriptionSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
         $range=5;
         $faker = Faker\Factory::create();
         $data = [];
         for ($i = 0; $i < $range; $i++) 
         {
            $data[] = 
            [
                'email'         => $faker->email,
            ];

        }

         $this->insert('Subscription', $data);

    }
}

  
