<?php

use Phinx\Seed\AbstractSeed;

class CreatePackageSeeder extends AbstractSeed
{
/**
 * Run Method.
 *
 * Write your database seeder using this method.
 *
 * More information on writing seeders is available here:
 * http://docs.phinx.org/en/latest/seeding.html
 */
public function run()
{
$faker = Faker\Factory::create();
$data = [];
$payment_mode = [];

                                   




$heading = [];
$subheading = [];

$cost=[];
$tentative=[];
$make_front=[];
$day1=[];
$day2=[];
$day3=[];
$day4=[];
$day5=[];



$heading[0]="1 Night 2 Days  ";
$subheading[0]="Wayanad To Wayanad GroupTour";
//$image_link = [];
$make_front[0]=1;
$cost[0]=1300;
$tentative[0]=0;
$day1[0]="<b>Day One:</b> Sightseeing Vythiri View Point, Pookode lake,  Chain tree, Banasura Earth Dam, Meenmutty Waterfalls, Karlad Lake and Overnight stay at hotel. 
";
$day2[0]="<b>Day Two:</b> Muthanga Wildlife sanctuary, Edakkal Caves, Neelimala View Point, Heritage museum, Ambalavayal farm, Pandum Rock, Karapuzha Dam and drops at Calicut railway station";
$day3[0]=NULL;
$day4[0]=NULL;
$day5[0]=NULL;
$payment_mode[0]="per person";

$heading[1]="1 Night  2 days ";
$subheading[1]="Calicut To Wayanad Group Tour";
//$image_link = [];
$make_front[1]=1;
$cost[1]=1600;
$tentative[1]=0;
$day1[1]="<b>Day One: </b> Pickup from calicut railway station at 7.30 am. Vythiri View Point, pookode lake, chain tree, Banasura Earth Dam, Meenmutty Waterfalls, Karlad Lake. Ovvernight stay at hotel";
$day2[1]="<b>Day Two: </b> Muthanga Wildlife sanctuary, Edakkal Caves, Neelimala View Point, Heritage museum, Ambalavayal farm, Pandum Rock, Karapuzha Dam and drops at Calicut railway station.";
$day3[1]=NULL;
$day4[1]=NULL;
$day5[1]=NULL;
$payment_mode[1]="per person";


$heading[2]="Ayurvedic Treatment";
$subheading[2]="";
//$image_link = [];
$make_front[2]=1;
$cost[2]=0;
$tentative[2]=1;
$day1[2]="<b>Rejuvenative Treatments:</b> Rejuvenative treatment are designed to revitalize the body tissue improve circulation and remove accumulated stress and toxins from mind and body.<br> <b>Curative Treatments:</b> Ateam of dedicated Ayurvedic Physicians meet the health needs of all our guests each of whom fills on a detaild health questionnaire and undergoes a basic medical examination. All the treatments may be selected only after the consultation of our physician ";
$day2[2]=NULL;
$day3[2]=NULL;
$day4[2]=NULL;
$day5[2]=NULL;
$payment_mode[2]="per person";

$heading[3]="1 Night  2 days ";
$subheading[3]="Mysore To Wayanad Group Tour ";
//$image_link = [];
$make_front[3]=1;
$cost[3]=2300;
$tentative[3]=0;
$day1[3]=" <b>Day One: </b> Starts on 11.30 pm at Mysore Bus Stand. Vythiri View Point, pookode lake, chain tree, Banasura Earth Dam, Meenmutty Waterfalls, Karlad Lake. Overnight stay at hotel.";
$day2[3]="<b>Day Two:  </b>Muthanga Wildlife sanctuary, Edakkal Caves, Neelimala View Point, Heritage museum, Ambalavayal farm, Pandum Rock, Karapuzha Dam and drops at Mysore bus stand.";
$day3[3]=NULL;
$day4[3]=NULL;
$day5[3]=NULL;
$payment_mode[3]="per person";


$heading[4]="1 Night  2 days ";
$subheading[4]="Banglore To Wayanad Group Tour";
//$image_link = [];
$make_front[4]=1;
$cost[4]=2400;
$tentative[4]=0;
$day1[4]="<b>Day One : </b>Start on 11.30 pm at Banglore Bus Stand. Sightseeing Vythiri View Point, pookode lake, chain tree, Banasura Earth Dam, Meenmutty Waterfalls, Karlad Lake. Overnight stay at hotel.";
$day2[4]="<b>Day Two : </b>Muthanga Wildlife sanctuary, Edakkal Caves, Neelimala View Point, Heritage museum, Ambalavayal farm, Pandum Rock, Karapuzha Dam And drops at Banglore bus stand.";
$day3[4]=NULL;
$day4[4]=NULL;
$day5[4]=NULL;
$payment_mode[4]="per person";

 
$heading[5]="1 Night  2 days ";
$subheading[5]="Chennai To Wayanad Group Tour ";
//$image_link = [];
$make_front[5]=1;
$cost[5]=1650;
$tentative[5]=0;
$day1[5]="<b>Day One: </b>Pickup from calicut railway station at 7.30 am. Vythiri View Point, pookode lake, chain tree, Banasura Earth Dam, Meenmutty Waterfalls, Karlad Lake. Overnight stay at hotel.";
$day2[5]="<b>Day Two: </b>Muthanga Wildlife sanctuary, Edakkal Caves, Neelimala View Point, Heritage museum, Ambalavayal farm, Pandum Rock, Karapuzha Dam and drops at Calicut railway station";
$day3[5]=NULL;
$day4[5]=NULL;
$day5[5]=NULL;
$payment_mode[5]="per person";


 



$heading[6]="2 Nights 3 Days";
$subheading[6]="Honeymoon Package";
//$image_link = [];
$make_front[6]=1;
$cost[6]=16500;
$tentative[6]=0;
$day1[6]="<p>Pick from Kalpetta bus stand (Wayanad).  Welcome drink on arrival.  Accommodation in Banasura Hill Valley home stay near meenmutty water falls.  Fruit basket in room on the day of arrival.  Two-day sightseeing (Wayanad).</p>
 <p>Simple flower arrangement once during the stay.  Dinner included. Bed tea Breakfast included.  Candle light dinner with a celebratory cake Drop back services to the Kalpetta bus stand (Wayanad) </p>";
$day2[6]=NULL;
$day3[6]=NULL;
$day4[6]=NULL;
$day5[6]=NULL;
$payment_mode[6]="";

 
$heading[7]="1 Night 2 Days  ";
$subheading[7]="Wayanad To Wayanad  Family Packages";
//$image_link = [];
$make_front[7]=1;
$cost[7]=5700;
$tentative[7]=0;
$day1[7]="<b>Day One: </b>First day tour Starting at 08.30 Am at Kalpetta. Vythiri View Point, pookode lake, chain tree, Banasura Earth Dam, Meenmutty Waterfalls, Karlad Lake. Overnight stay at hotel.";
$day2[7]="<b>Day Two: </b>Second day tour with Breakfast Start 6.30 AM at Muthanga Wildlife sanctuary, Edakkal Caves, Neelimala View Point, Heritage museum, Ambalavayal farm, Pandum Rock, Karapuzha Dam Sulthan Bathery Jain Temple.";
$day3[7]=NULL;
$day4[7]=NULL;
$day5[7]=NULL;
$payment_mode[7]="per person";

$heading[8]="2 Nights 3 Days ";
$subheading[8]="Wayanad To Wayanad  Group Tour";
//$image_link = [];
$make_front[8]=1;
$cost[8]=2550;
$tentative[8]=0;
$day1[8]="<b>Day One: </b>Starting at 08.30 Am at Kalpetta. Early morning fresh up and going to Sightseeing Vythiri View Point, pookode lake, chain tree, Banasura Earth Dam, Meenmutty Waterfalls, Karlad Lake. Overnight stay at hotel.";
$day2[8]="<b>Day Two: </b>Muthanga Wildlife sanctuary, Edakkal Caves, Neelimala View Point, Heritage museum, Ambalavayal farm, Pandum Rock, Karapuzha Dam Sulthan Bathery Jain Temple.";
$day3[8]="<b>Day Three: </b>Starts at 7.30 amThirunelly Temple, Tholpetty wild life sanctuary. KuruvaIsland, Seetha Lava Kushatemple";
$day4[8]=NULL;
$day5[8]=NULL;
$payment_mode[8]="per person";

$heading[9]="1 Night 2 Days";
$subheading[9]="Honeymoon Package";
//$image_link = [];
$make_front[9]=1;
$cost[9]=10500;
$tentative[9]=0;
$day1[9]="<p>Pick from Kalpetta bus stand (Wayanad).  Welcome drink on arrival.  Accommodation in Banasura Hill Valley home stay near meenmutty water falls.  Fruit basket in room on the day of arrival.  Two-day sightseeing (Wayanad).</p>
 <p>Simple flower arrangement once during the stay.  Dinner included. Bed tea Breakfast included.  Candle light dinner with a celebratory cake Drop back services to the Kalpetta bus stand (Wayanad) </p>";
$day2[9]=NULL;
$day3[9]=NULL;
$day4[9]=NULL;
$day5[9]=NULL;
$payment_mode[9]="";

$heading[10]="1 Night 2 Days";
$subheading[10]="Wayanad Group Trekking";
//$image_link = [];
$make_front[10]=1;
$cost[10]=2550;
$tentative[10]=0;
$day1[10]="<b>Day One: </b><p>Arrival at Wayanad, proceed to hotel and fresh up sight seeing of Banasura Earth Dam, Meenmutty Water Falls, Karlad Lake, Baboo Rafting, Pookode Lake, Vythiri view Point, Chain Tree. Overnight stay at Banasura Hill Valley or Tent camping
with camp fire-DJ</p>";
$day2[10]="<b>Day Two: </b><p>After breakfast proceed to Trekking at Brahmagiri hills.</p>
";
$day3[10]=NULL;
$day4[10]=NULL;
$day5[10]=NULL;
$payment_mode[10]="per person";

 /* $heading[1] = "Two Night  Three days<br>Calicut To Wayanad Tour ";
$desc[1] = "  <br><br><b>Day One <br><br></b>

Pickup from calicut railway station at 7.30 am.Vythiri View Point,pookode lake,chain tree,Banasura Earth Dam,Meenmutty Waterfalls,Karlad Lake. Ovvernight stay at hotel
<br><br><b>Day Two<br><br></b>
 Muthanga Wildlife sanctuary, Edakkal Caves,Neelimala View Point,Heritage museum,Ambalavayal farm,Pandum Rock,Karapuzha Dam and drops at Calicut railway station.
";

$cost[1]=1600;
$tentative[1]=0;

////////////////////////////////////////////////////////////////

$heading[2] = "Wayanad  Ayurvedic Treatment ";
$desc[2] = "<b><br><br>Rejuvenative Treatments:<br><br></b> Rejuvenative treatment are designed to revitalize the body tissue improve circulation and remove accumulated stress and toxins from mind and body.  <b> <br><br>Curative Treatments:<br><br></b>  Ateam of dedicated Ayurvedic Physicians meet the health needs of all our guests each of whom fills on a detaild health questionnaire and undergoes a basic medical examination.All the treatments may be selected only after the consultation of our physician  ";
$cost[2]=10000;
$tentative[2]=1;
 <b><br><br>Rejuvenative Treatments:<br><br></b> Rejuvenative treatment are designed to revitalize the body tissue improve circulation and remove accumulated stress and toxins from mind and body.  <b> <br><br>Curative Treatments:<br><br></b>  Ateam of dedicated Ayurvedic Physicians meet the health needs of all our guests each of whom fills on a detaild health questionnaire and undergoes a basic medical examination.All the treatments may be selected only after the consultation of our physician 

///////////////////////////////////////////////////////////////////////////////////




$heading[3] = "One Night  Two days <br>Mysore To Wayanad ";
$desc[3] = "
<br><br><b>Day One <br><br></b>


Starts on 11.30 pm at Mysore Bus Stand.Vythiri View Point,pookode lake,chain tree,Banasura Earth Dam,Meenmutty Waterfalls,Karlad Lake. Overnight stay at hotel.
<br><br><b>Day Two<br><br></b>
Muthanga Wildlife sanctuary, Edakkal Caves,Neelimala View Point,Heritage museum,Ambalavayal farm,Pandum Rock,Karapuzha Dam and drops at Mysore  bus stand.

 ";
$cost[3]=2300;
$tentative[3]=0;


/////////////////////////////////////////////////////

$heading[4] = "One Night  Two days <br>Banglore To Wayanad";
$desc[4] =  "
<br><br><b>Day One <br><br></b>


Start on 11.30 pm at Banglore Bus Stand.Sightseeing
Vythiri View Point,pookode lake,chain tree,Banasura Earth Dam,Meenmutty Waterfalls,Karlad Lake.Overnight stay at hotel.


<br><br><b>Day Two<br><br></b>
Muthanga Wildlife sanctuary, Edakkal Caves,Neelimala View Point,Heritage museum,Ambalavayal farm,Pandum Rock,Karapuzha Dam And drops at Banglore bus stand.

";
$cost[4]=2400;
$tentative[4]=0;

//////////////////////////////////////////////////////////////////////////////////////


$heading[5] = "One Night  Two days<br>Chennai To Wayanad Tour";
$desc[5] = "
<br><br><b>Day One <br><br></b>


Pickup from calicut railway station at 7.30 am .Vythiri View Point,pookode lake,chain tree,Banasura Earth Dam,Meenmutty Waterfalls,Karlad Lake.Overnight stay at hotel.


<br><br><b>Day Two<br><br></b>
Muthanga Wildlife sanctuary, Edakkal Caves,Neelimala View Point,Heritage museum,Ambalavayal farm,Pandum Rock,Karapuzha Dam and drops at Calicut railway station

 ";
$cost[5]=1650;
$tentative[5]=0;

////////////////////////////////////////////
$heading[6] = "Two Night Three Days<br>Wayanad To Wayanad";
$desc[6] = "
<br><br><b>Day One <br><br></b>


Starting at 08.30 Am at Kalpetta.Early morning fresh up and going to Sightseeing Vythiri View Point,pookode lake,chain tree,Banasura Earth Dam,Meenmutty Waterfalls,Karlad Lake. Overnight stay at hotel.


<br><br><b>Day Two<br><br></b>
Muthanga Wildlife sanctuary, Edakkal Caves,Neelimala View Point,Heritage museum,Ambalavayal farm,Pandum Rock,Karapuzha Dam Sulthan Bathery Jain Temple.

<br><br><b>Day Three<br><br></b>
Starts at 7.30 amThirunelly Temple,Tholpetty wild life sanctuary.KuruvaIsland,Seetha Lava Kushatemple

 ";
$cost[6]=2550;
$tentative[6]=0;

///////////////////////////////////////


$heading[7] = "One Night Two Days Family Packages<br>Wayanad To Wayanad";
$desc[7] = "
<br><br><b>Day One <br><br></b>


First day tour Starting at 08.30 Am at Kalpetta. Vythiri View Point,pookode lake,chain tree,Banasura Earth Dam,Meenmutty Waterfalls,Karlad Lake. Overnight stay at hotel.


<br><br><b>Day Two<br><br></b>
Second day tour with Breakfast Start 6.30 AM at Muthanga Wildlife sanctuary, Edakkal Caves,Neelimala View Point,Heritage museum,Ambalavayal farm,Pandum Rock,Karapuzha Dam Sulthan Bathery Jain Temple.

 

 
 ";
$cost[7]=5700;
$tentative[7]=0;
*/

/////////////////////////////////////






         
         for ($i = 0; $i < 11; $i++) 
         {
             
             
            if($i!=10)  
             
              $temp_image ="02_IMAGES/aa_HOME_PAGE/02_Packages/package_".($i%8).".jpg";
            else
              $temp_image ="02_IMAGES/aa_HOME_PAGE/02_Packages/package_".(8).".jpg";
                
              
          
                
          
               
            $data[] = 
            [
                            'heading'=>$heading[$i],
                            'subheading'=>$subheading[$i],
                            'image_link' =>$temp_image,            
                            //$image_link = [];
                            'make_front'=>$make_front[$i],
                            'cost'=>$cost[$i],
                            'tentative'=>$tentative[$i],
                            'day1'=>$day1[$i],
                            'day2'=>$day2[$i],
                            'day3'=>$day3[$i],
                            'day4'=>$day4[$i],
                            'day5'=>$day5[$i],
                            'payment_mode'=>$payment_mode[$i]
                   ];

        }

         $this->insert('galleryPackage', $data);
         
         
         
         
         
    }
}
