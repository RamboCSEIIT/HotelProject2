<?php
namespace tour\Controllers;

use duncan3dc\Laravel\BladeInstance;

use tour\Validation\Validator;
use Cocur\Slugify\Slugify; 


 
use Illuminate\Database\Capsule\Manager as DB;
 

class AdminController extends BaseController
{

    /**
     * Saved edited page; called via ajax
     * @return string
     */
    public function postSavePage()
    {
        //echo "Hello";
        //it wont work here coz no display of echo only computation happens
        // the echos will return to ajax
        // dd("Inside post save page");
        
         $okay = true;
        // $result_test='OK';
        
        if ($_REQUEST['page_id'] > 0) 
        {
                DB::statement(
                                 'UPDATE pages
                                  SET page_content = :page_content
                                  WHERE id = :page_id'
                       ,
                array(
                        'page_content'  => $_REQUEST['thedata'],
                        'page_id'       => $_REQUEST['page_id']
                      ));
                
        } 
        else
        {
      
                        $slugify = new Slugify;
                         
                        $browser_title = $_REQUEST['browser_title'];
                        $page_content  = $_REQUEST['thedata'];
                        $slug          =  $slugify->slugify($browser_title);
                        $page_name     ='@';
             
                        $results =  DB::select('SELECT slug FROM pages WHERE slug = :slug',
                                                        array(
                                                                 'slug'  => $slug
                                                             )
                                                  );
                        
                    //  $result_test=count($results) ;
                        
                     
                        
                        if(count($results)>0)
                        {
                             $okay = false;
                              
                        }
                        else
                        {
                            DB::statement('INSERT INTO pages (browser_title, page_content, slug,page_name) VALUES (:browser_title, :page_content,:slug,:page_name)',
                                    array(
                                            'browser_title'  => $browser_title,
                                            'page_content'   => $page_content,
                                            'slug'           => $slug,
                                            'page_name'      => $page_name)
                                         );
        
                            
                        }

            
            

            
        }
        
        if ($okay)
        {
           
            echo 'OK';
        } 
        else
        {
            echo "Browser title is already in use!";
        }  
        
        
                
        

    }

    public function getAddPage()
    {
        
        // dd("getAddPage");
         
        $page_id = 0;
        $page_content = "Enter your content here";
        $browser_title = "";

        echo $this->blade->render('generic-page', [
            'page_id' => $page_id,
            'browser_title' => $browser_title,
            'page_content' => $page_content,
            'page_name' => '@'
        ]); 
    }

}
