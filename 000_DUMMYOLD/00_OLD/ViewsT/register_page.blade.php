 @extends('base_page')

 
    @section('title') 
      Register  
    @stop
   
     <!-- auto matically takes this as red --> 
@section('cssBlock') 
 
      
      <style>
       
       .error
       {
          color: #FF0000;
       }

      </style>
    
@stop
  
 
 @section('content')
  
         <div class="row">
            <div class="col-sm-2  ">

            </div>
            <div class="col-sm-8">
                        <h1>Register </h1>
                            <hr>

                          

            </div>
             
              <div class="col-sm-2  ">

            </div>
           
        </div>
        
        
   
        <div class="row">
            
             <div class="col-sm-2  ">

            </div>
            
            <div class="col-sm-8  ">
                
                <form  class="form-horizontal required" name ="registration_form" id = "registration_form"  action ="/register" method="post" novalidate >
                        
       
                            <div class="form-group">
                                <label for="first_name" class="col-sm-2 control-label">First Name</label>
                                <div class="col-sm-10">
                                  <input type="text" class="form-control required" id="first_name_id" name = "first_name_name" placeholder="first name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="last_name" class="col-sm-2 control-label">Last Name</label>
                                <div class="col-sm-10">
                                  <input type="text" class="form-control required" id="last_name" name = "last_name_name"  placeholder="last name">
                                </div>
                            </div>

                          <div class="form-group">
                              <label for="email_name" class="col-sm-2 control-label">Email</label>
                              <div class="col-sm-10">
                                <input type="email" class="form-control required" id="email_name_id" name = "email_name_name" placeholder="user@example.com">
                              </div>
                          </div>
                          <div class="form-group">
                              <label for="verify_email_name" class="col-sm-2 control-label">verify Email</label>
                              <div class="col-sm-10">
                                <input type="email" class="form-control required email" id="verify_email_name_id" name = "verify_email_name_name" placeholder="user@example.com">
                              </div>
                          </div>

                              <div class="form-group">
                                <label for="password" class="col-sm-2 control-label">Password</label>
                                  <div class="col-sm-10">
                                    <input type="password" class="form-control required" id="password_id" name = "password_name" placeholder="Password">
                                  </div>
                              </div>


                          <div class="form-group">
                            <label for="verify_password" class="col-sm-2 control-label">verify Password</label>
                              <div class="col-sm-10">
                                <input type="password" class="form-control required" id="verify_password_id" name = "verify_password_name" placeholder="Password">
                              </div>
                          </div>
                
                          <div class="form-group">
                            <label for="mob_number" class="col-sm-2 control-label">Mobile Number</label>
                              <div class="col-sm-10">
                                <input type="number" class="form-control required" id="mob_number_id" name = "mob_number_name" placeholder=" example  91XXXXNNNNNN">
                              </div>
                          </div>



                          <div class="form-group">
                              <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary"   >Register</button>
                              </div>
                          </div>
                </form>


            </div>
            
            
             <div class="col-sm-2  ">

            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                 <br>
                 <br>
            </div>
         </div>
  
   
     
  @stop
 
  @section('bottomjs')
 
    
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
 
                    <script type="text/javascript">
                        $(document).ready(
                                
                                        function()
                                        {
                                                     //  alert("Welcome to JQuery");

                                                    $("#registration_form").validate({
                                                      rules :
                                                      {
                                                                  verify_email_name_name : 
                                                                  {
                                                                             required : true,
                                                                             email :true,
                                                                             equalTo:"#email_name_id"

                                                                  }, 
                                                                  password_name: 
                                                                  {
                                                                             required: true,
                                                                             minlength: 3
                                                                  },
                                                                  verify_password_name:
                                                                  {
                                                                            required: true,
                                                                            minlength: 3,
                                                                            equalTo: "#password_id"
                                                                  },
                                                                  
                                                                  first_name_name: 
                                                                  {
                                                                             required: true,
                                                                             minlength: 3
                                                                  },
                                                                  
                                                                  last_name_name: 
                                                                  {
                                                                             required: true,
                                                                             minlength: 3
                                                                  }

                                                      }
                                                    });
                                                    });
               </script>
 
  @stop


 