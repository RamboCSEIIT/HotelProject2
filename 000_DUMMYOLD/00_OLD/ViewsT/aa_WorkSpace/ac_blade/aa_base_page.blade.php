
<!--  It says document type for browser as html 5 file -->

<!DOCTYPE html>
<html>

<head>
    
  <meta charset="utf-8">
    <!--  For IE only -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" type="image/png" href="/02_IMAGES/favicon.png">
  <!-- Font Awesome -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  
 
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
   

      @yield('cssBlock')
    
  <title>    @yield('title')  </title>
</head>

<body   data-spy="scroll"  data-offset="1"  >
   
    
    
 @yield('content')
 
 

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
{{--   
    <script type="text/javascript" src="/05_LOCAL/JS/jquery-3.2.1.min.js"></script> 
    <script type="text/javascript" src="/05_LOCAL/JS/popper.min.js"></script> 
    <script type="text/javascript" src="/05_LOCAL/popper.min.js"></script> 
          --}} 
 @yield('bottomJS')
    
    
      
   
   
    
   
</body>

</html>
