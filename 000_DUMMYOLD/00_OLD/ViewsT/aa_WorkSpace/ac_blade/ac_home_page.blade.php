 @extends('aa_WorkSpace.ac_blade.aa_base_page') 
 
@section('title') 
      Banasura Hill Valley Home Stay
 @stop 
 
  @section('cssBlock') 
  @include('aa_WorkSpace.aa_html.aa_HomePage.za_css') 
 @stop

@section('content') 
     

   <div class="  container-fluid">
       <div class="row">
           <div class=".col-sm-12 col-md-12">
                  @include('aa_WorkSpace.aa_html.aa_HomePage.ah_navbar')
           </div>
        
       </div>
          
    </div> 

   
         

  <br>
  <br>
          @include('aa_WorkSpace.aa_html.aa_HomePage.aa_jumbotron')    
 
           @include('aa_WorkSpace.aa_html.aa_HomePage.ab_icons')  
           @include('aa_WorkSpace.aa_html.aa_HomePage.ac_wayanad') 
  
           @include('aa_WorkSpace.aa_html.aa_HomePage.ae_package') 
           @include('aa_WorkSpace.aa_html.aa_HomePage.ad_portfolios')
 
           @include('aa_WorkSpace.aa_html.aa_HomePage.ag_contact')  
           @include('aa_WorkSpace.aa_html.aa_HomePage.ag_google_map')
           @include('aa_WorkSpace.aa_html.aa_HomePage.ai_footer_page')   

     
     
 @stop
 
 
 @section('bottomJS') 
 
      @include('aa_WorkSpace.aa_html.aa_HomePage.zb_javascript')   
     
 @stop 
 
 
  