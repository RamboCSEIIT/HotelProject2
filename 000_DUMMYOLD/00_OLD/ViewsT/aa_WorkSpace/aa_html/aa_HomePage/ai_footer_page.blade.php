    <footer id="myFooter">
        <div class="container ">
            <div class="row">
                <div class="col-md-4 ">
                    <h5>Current Page</h5>
                    <ul>
                        <li><a href="#topnavid" >Page Top</a></li>
                        <li><a href="#wayanad">Wayanad</a></li>
                        <li><a href="#profile">Our Resort</a></li>
                        <li><a href="#contact">Contact</a></li>
                        <li><a href="#package">Packages</a></li>
                    </ul>
                </div>
                  
                 
                <div class="col-md-4">
                    <h5>Info</h5>
                    <ul>
                        <li><a href="/about-tour">About Us</a></li>
                        <li><a href="/faq">Faq</a></li>
                        <li><a href="/login">Login</a></li>
                         <li><a href="/register">Register</a></li>
                    </ul>
                </div>
                
                 
                
               
                 
                 
                
                <div class="col-md-4">
                    <h5>Address</h5>
                    <ul>
                       <li> <a href="#">  Banasura Hill Valley </a> </li>
                        <li> <a href="#">  Near Meenmutty Water Falls </a> </li>
                        <li> <a href="#">  Wayanad Kerala,673575 </a> </li>
                        <li> <a href="#">  ph:+91-8129723182</a> </li>
                        <li> <a href="#">  info@wayanadtoursandtravels.com </a> </li>
                    </ul>
                </div>
            </div>
            <!-- Here we use the Google Embed API to show Google Maps. -->
            <!-- In order for this to work in your project you will need to generate a unique API key.  -->
 
        </div>
        <div class="social-networks">
            <a href="https://www.facebook.com/WAYANADTOURSANDTRAVELS/" class="twitter"><i class="fa fa-twitter"></i></a>
            <a href="https://www.facebook.com/WAYANADTOURSANDTRAVELS/" class="facebook"><i class="fa fa-facebook"></i></a>
            <a href="https://www.facebook.com/WAYANADTOURSANDTRAVELS/" class="google"><i class="fa fa-google-plus"></i></a>
        </div>
        <div class="footer-copyright">
             
            <p>© 2017 Copyright Text </p>
        </div>
    </footer>