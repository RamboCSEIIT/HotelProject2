<div id="topnavid">
 
         <nav id = "myScrollspy" class="navbar navbar-expand-sm  fixed-top" >
            <!-- 
           style="background-color: #f4511e;"
              <a class="navbar-brand" href="#">Navbar</a>
             <li id ="logout-page"><a href="/logout"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span>Logout</a></li>

            -->
              
                
            <div class="navbar-brand" style="color:whitesmoke" >    
                <div class="text-center">
                     <img class="logo img-circle" src="/02_IMAGES/favicon.png" alt="Logo" width="30" height="30">
                     
                </div>
                      
 
                      
             </div>  
            
                
                <button class="navbar-toggler"  type="button" data-toggle="collapse" data-target="#collapsibleNavbar" style="margin:10px;color:whitesmoke">
                  <span class="icon" >  <i class="fa fa-bars" id="tag" aria-hidden="true">Menu</i></span>
                </button>
                
   
            
                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                  
                    
                    <ul class="navbar-nav mr-auto company_email_font">
                      
                      
                                <li  id ="home-page" class="nav-item">
                                  <a class="nav-link" href="/">Home</a>
                                </li>
                                <li  id ="about-tour-page" class="nav-item">
                                  <a class="nav-link  " href="/about-tour">About</a>
                                </li>
                                <li id ="register-page" class="nav-item">
                                  <a class="nav-link" href="/register">Register</a>
                                </li>    
                                 <li id ="about-package-page" class="nav-item">
                                  <a class="nav-link " id="packages_nav" href="#package">Packages</a>
                                </li>    
                                
                                
                                



                         <li id ="add-entry-page" class="nav-item">
                                             <a class="nav-link " href="#contact">Enquiry</a>
                                 </li>  


                                
                                
                                  <li id ="home-stay" class="nav-item">
                                  <a class="nav-link " href="#profile">Homestay</a>
                                 </li>  
                    </ul>
                    
                    
                    
                    <ul class="navbar-nav ml-auto">
                        
                           @if (tour\auth\LoggedIn::user() && tour\auth\LoggedIn::user()[0]->access_level == 2)
                                <li class="nav-item dropdown" style="background-color:#f4511e; ">
                                   <a class="nav-link dropdown-toggle " href="#" id="admin_navdrop" data-toggle="dropdown">
                                     Admin
                                   </a>
                                   <div class="dropdown-menu" style="background-color:#f4511e; ">
                                     <a class="dropdown-item " id ="epage" href="#" style="background-color:#f4511e; ">Edit Page</a>
                                       
                                     <a class="dropdown-item " id ="apage" href="#" style="background-color:#f4511e; ">Add a page</a>
                                     <a class="dropdown-item "id ="senq"  href="#" style="background-color:#f4511e; ">Show Enquiry</a>
                                   </div>
                                 </li>
                                <li  id ="logout-page" class="nav-item">
                                  
                                  <a class="nav-link " href="/logout"> <i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>
                                </li>

  
                           @elseif(tour\auth\LoggedIn::user())           
                                <li  id ="logout-page" class="nav-item">
                                  
                                  <a class="nav-link " href="/logout"> <i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>
                                </li>
    
                           @else   

                                 <li  id ="login-page" class="nav-item">
                                    
                                  <a class="nav-link " href="/login"> <i class="fa fa-sign-in" aria-hidden="true"></i>Login</a>
                                </li>
                           @endif
                     
                     </ul>
                     
                </div>  
        </nav>

 
</div>
