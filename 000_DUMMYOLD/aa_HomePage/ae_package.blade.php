@section('image_pkg') 





@foreach ($galleryPackage as  $index => $gallery_item)




<div class="card">

    <div class="card-body cardSet1 text-white">
        <img src="{!!  $gallery_item->image_link !!}" alt="Smiley face" class="img-fluid"  >
        <h3 class="card-title mt-2 text-center">{!!  $gallery_item->heading !!}</h3>
        <h5 class="card-subtitle text-center">Package</h5>
        <p class="card-text mt-3">
            {!!  $gallery_item->description !!}
        </p>
             
        <a href="#" class="btn btn-dark btn-small float-right">Read more...</a>
    </div>
</div>









@endforeach


@stop 

<div id="package" class="slideanim">
    <br> 
    <div class="container">
        <div class="card-deck">

          @yield('image_pkg')  


        </div>




    </div>

    <br> 
    <br> 
    <br> 





</div>

