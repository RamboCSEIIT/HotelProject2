 
@section('contact')

<form    method="POST" id="enquiryform" action="/contact" style="padding-left: 15px;padding-right: 15px">

    <div class="row">
        <div class="col-sm-12 form-group">
            <input class="form-control" id="name" name="name" placeholder="Name" type="text" required>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-6 form-group">
            <input class="form-control" id="mobile" name="mobile" placeholder="mobile" type="number" required>
        </div>
        <div class="col-sm-6 form-group">
            <input class="form-control" id="email" name="email" placeholder="Email" type="email" required>
        </div>
    </div>
    
         
    <textarea class="form-control" id="comments" name="comments"  placeholder="Comment" rows="5"></textarea><br>
  
    <div class="row">
        <div class="col-sm-12 form-group text-center">
  
            <button class="btn btn-default     centered button_round   " type="submit">Send</button>
       
        </div>
    </div>
   
</form>
@stop



@section('address')
<!-- Container (Pricing Section) -->
<div class="panel panel-default text-center">
    <div class="panel-heading">
        <h1 class="font_contact"> Address </h1>
    </div>

    <div class="panel-body">
        <br> <br>   <br> <br> 

        <ul style="list-style: none;"  >

            <li>    Banasura Hill Valley Home Stay   </li>
            <li>   Near Meenmutty Water Falls,Banasura Sagar   </li>
            <li>    Wayanad Kerala,673575  </li>
            <li>   ph:+91-8129723182, +91-9947177040  </li>
            <li>   +919744686455,+91 8848810585  </li>
            <li>    Email:- info@wayanadtoursandtravels.com   </li>


        </ul>
 <br> <br>   <br> <br>




        <br>
        <br>

    </div>


</div> 
 <br> <br>   

@stop

<div id="contact" >
    
 
 
    <!-- Container (Contact Section) -->
    <div class="container-fluid well well-sm ">
        <div class="row ">
            <br>
            
        </div>


        <div class="row">
            <div class="col-md-1">

            </div> 
            <div class="col-lg-4 "  >
                <div class="row ">
                    <br><br>
                </div>
                 @yield('address')
            </div>

            <div class="col-lg-6 rounded">
                <br> 
                <br>
                <h2 class="text-center">CONTACT</h2> 
                <br>
                <br>

                @yield('contact') 
              <div class="text-center">      <span id="contactmsg" class="company_moto_font " style="color: whitesmoke;background-color:maroon;font-size: 15px"></span>
                  
              </div>

            </div>

            <div class="col-sm-1">

            </div> 
        </div>

        <div class="row">










        </div>

        <div class="row">
            <br>
            <br>

        </div>

    </div>
    
          
</div>

