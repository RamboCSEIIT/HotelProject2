@php
$row_slice = $galleryPackage[0];
$classbg = "cardSet"."0";
@endphp


 

<div class="fallback-message">
    <p>Your browser <b>doesn't support the features required</b> by impress.js, so you are presented with a simplified version of this presentation.</p>
    <p>For the best experience please use the latest <b>Chrome</b>, <b>Safari</b> or <b>Firefox</b> browser.</p>
</div>

<div id="impress" data-transition-duration="2000">
@php
$count =sizeof($galleryPackage);
$angle_step = 2.0*3.14/$count;
$id_name= "FirstID";
$total_angle =0;

$first_index=0;
$last_index=$count-1;

$first_id ="Package"."-".$first_index;

$last_id = "Package"."-".$last_index;

//dd($first_id);

$last_x = 4500*cos($angle_step*$first_index);
$last_y = 4500*sin($angle_step*$last_index);

@endphp



@foreach ($galleryPackage as  $index => $gallery_item)


@php
   $classbg = "cardSet".$index%3;
   $row_slice = $gallery_item;
@endphp



@if($index==$first_index)

 <div id="{!!$first_id!!}" class="step " data-x="0" data-y="0" data-z="0" data-goto-prev="{!! $last_id !!}">
      
 </div>
@endif

 @if($index==$last_index)
 
        <div id="{!! $last_id !!}" class="step" data-x="{!! $last_x !!}}" data-y="{!! $last_y !!}" data-z="0" data-rotate-z="45" data-rotate-y="-315" data-rotate-order="zyx"
         data-goto-next="{!! $first_id !!}">
        <p>9</p>
    </div>
 
@endif

   
  
    

@endforeach


</div>
 
<div id="impress-toolbar"></div>
<div id="impress-help"></div>

