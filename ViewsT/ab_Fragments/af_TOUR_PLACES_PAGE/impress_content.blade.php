@section('viewpoint')

<div class="container" id="tour-places" style="background: red;" >
    <div class="text-center" style="font-size:20px;margin-top: 5px">
        Use arrow keys to move to next place
    </div> 

    <div class="container " style="width:100%;height:300px;padding: 0px;border: 0px;margin: 30px 0px;overflow: hidden" id="image_holder">


        <div class="row justify-content-center text-center" style="height:inherit">
            <div class="col-12"> 

                <img class="img-fluid" src="/02_IMAGES/af_TOUR_PLACES_PAGE/e_viewpoint.jpg" >
                <div class="title1">Vythiri view point and Chain Tree</div>



                <!-- end details -->


                <!-- end hero -->



            </div>

        </div>
    </div>



    <!-- end container -->
    <div class="description container movie-card text-justify">


        <p> 

            Lakkidi is the gateway of Wayanad as it is located at the crest of the serpentine Thamarassery ghat pass. There are nine hairpin curves from Adivaram (Downhill) to Lakkidi through steep mountains and a journey through the serpentine ghat pass is worth an experience.  It is about 2296 ft (700 m) above the sea level and the major town nearby is Vythiri which lies five km away.
        </p>
        <p> 
            The lush greenery of the hills, gorges and streams seen on both sides of the passage up the hill are sure to linger in the visitors’ mind for a long time. Tourists often stop by Lakkidi View Point, the vantage point which offers dazzling views of the surrounding cliffs and valleys.</p>
        <p> 
            Lakkidi has also emerged as a popular hill destination with many expensive resorts and plantation stays providing stay facility for foreigners and other affluent tourists. </p>


        <h4>
            Chain Tree
        </h4>
        <p>You can see monkeys frolicking through the roadside and once you start travelling through the road to Vythiri, you will see a ficus tree on the left side. You will also notice a heavy chain anchored to the ground and tied around the stout branches of the tree.
        </p>
        There is a legend associated with the chain bound ficus tree.  It is said that a British engineer, after many attempts, failed to create a passage through the forests of Wayanad. Then a tribal youth, Karinthandan, guided him in making the route. The engineer was reluctant to share the glory and hence he killed Karinthandan. Local people believe that later the soul of the tribal youth started haunting the travellers on this way and at last a priest chained the spirit to the tree.
        </p>


        <!-- end column2 -->
    </div>
    <!-- end description -->
    <div class="row justify-content-center">
        <div class=".col ">

            <a href="/"> <i class="fa fa-arrow-circle-left"></i>  Home Page </a>

        </div>

    </div>

</div>

@stop

@section('kuruva')

<div class="container" id="tour-places" style="background: red;" >
    <div class="text-center" style="font-size:20px;margin-top: 5px">
        Use arrow keys to move to next place
    </div> 

    <div class="container " style="width:100%;height:300px;padding: 0px;border: 0px;margin: 30px 0px;overflow: hidden" id="image_holder">


        <div class="row justify-content-center text-center" style="height:inherit">
            <div class="col-12"> 

                <img class="img-fluid" src="/02_IMAGES/af_TOUR_PLACES_PAGE/b_kuruvadweep.jpg" >
                <div class="title1">Kuruadweep-a group of islands in Wayanad</div>



                <!-- end details -->


                <!-- end hero -->



            </div>

        </div>
    </div>



    <!-- end container -->
    <div class="description container movie-card text-justify">



        <p>
            Located about 14 km east of Mananthavady in the northern district of Wayanad, Kuruvadweep is a group of islets forming 950 acres of evergreen forest. These islets are located in one of the tributaries of the River Kabani, which is close to the border with the State of Karnataka.</p>

        <p>There are three prominent islets among the group, where one would come across a variety of vegetation, some rare orchids and wild flowers. One of these main islands contain two small fresh water lakes These islets are also haven for migratory birds, whose activities break the otherwise silent environs of these beautiful islets. The avian life here includes hornbills, parrots and a host of butterfly species.</p>

        <p>Some of these islets have massive trees, the boughs of which at some places stoop down and caress the river water. The whole of Kuruvadweep has a unique eco-system worth exploring for nature lovers. Those who venture into these islets needs to refrain from actions that would hamper the environment.</p>

        <p>For those with a bent for trekking can make the most of it by heading for Kuruvadweep, while it is an exciting proposition for those carving for some blissful and serene moments; and for some it is definitely a place where one can get under the cool shades of big trees and listen to the lullaby of the gurgling river.</p>



        <!-- end column2 -->
    </div>
    <!-- end description --> 
    <div class="row justify-content-center">
        <div class=".col ">

            <a href="/"> <i class="fa fa-arrow-circle-left"></i>  Home Page </a>

        </div>

    </div>

</div>

@stop

@section('muthanga')

<div class="container" id="tour-places" style="background: red;" >
    <div class="text-center" style="font-size:20px;margin-top: 5px">
        Use arrow keys to move to next place
    </div> 

    <div class="container " style="width:100%;height:300px;padding: 0px;border: 0px;margin: 30px 0px;overflow: hidden" id="image_holder">


        <div class="row justify-content-center text-center" style="height:inherit">
            <div class="col-12"> 

                <img class="img-fluid" src="/02_IMAGES/af_TOUR_PLACES_PAGE/d_muthanga.jpg" >
                <div class="title1">Muthanga wild life sanctuary</div>



                <!-- end details -->


                <!-- end hero -->



            </div>

        </div>
    </div>



    <!-- end container -->
    <div class="description container movie-card text-justify">



        <p>Established in 1973, Muthanga Wildlife sanctuary is contiguous to the protected area network of Nagarhole and Bandipur of Karnataka on the northeast and Mudumalai of Tamil Nadu on the southeast. Rich in bio-diversity, the sanctuary is an integral part of the Nilgiri Biosphere Reserve. The management lays emphasis on scientific conservation with due consideration for the general lifestyle of the tribals and others who live in and around the forest region.The Sanctuary has a large population of pachyderms and has been declared a Project Elephant site.</p>

        <p>Elephants roam freely here and tigers are sighted occasionally. Various species of deer, monkeys, birds etc also live here. The Reserve is also home to a small population of tigers, a profusion of birds, butterflies and insects. The trees and plants in the sanctuary are typical of the south Indian moist deciduous forests and west coast semi evergreen forests. A drive along the road to Muthanga and further, offers chances to watch these roaming animals. Elephant rides are arranged by the Forest Department.
        </p>



        <!-- end column2 -->
    </div>
    <!-- end description --> 
        <div class="row justify-content-center">
        <div class=".col ">

            <a href="/"> <i class="fa fa-arrow-circle-left"></i>  Home Page </a>

        </div>

    </div>


</div>

@stop


@section('chembra')

<div class="container" id="tour-places" style="background: red;" >
    <div class="text-center" style="font-size:20px;margin-top: 5px">
        Use arrow keys to move to next place
    </div> 

    <div class="container " style="width:100%;height:300px;padding: 0px;border: 0px;margin: 30px 0px;overflow: hidden" id="image_holder">


        <div class="row justify-content-center text-center" style="height:inherit">
            <div class="col-12"> 

                <img class="img-fluid" src="/02_IMAGES/af_TOUR_PLACES_PAGE/g_chembra.jpg" >
                <div class="title1">Chembra Peak</div>



                <!-- end details -->


                <!-- end hero -->



            </div>

        </div>
    </div>



    <!-- end container -->
    <div class="description container movie-card text-justify">


        <p>
            Hike up the rugged terrains of the Chembra Peak located 2,100 metres above sea level on the southern part of Wayanad. Chembra is the tallest peak in Wayanad and is an ideal area for trekking. There is a heart shaped lake on the way to the top of the peak, which is believed to have never dried up, is a major tourist attraction here. With permission from the Forest Department, one-day treks and two-day wildlife treks are possible. You can have group treks of up to ten people or hike on your own, accompanied by a guide
        </p>
        <!-- end avatars -->


        <!-- end column2 -->
    </div>
    
        <div class="row justify-content-center">
        <div class=".col ">

            <a href="/"> <i class="fa fa-arrow-circle-left"></i>  Home Page </a>

        </div>

    </div>

    <!-- end description -->

</div>

@stop


@section('edakkal')

<div class="container" id="tour-places" style="background: red;" >
    <div class="text-center" style="font-size:20px;margin-top: 5px">
        Use arrow keys to move to next place
    </div> 

    <div class="container " style="width:100%;height:300px;padding: 0px;border: 0px;margin: 30px 0px;overflow: hidden" id="image_holder">


        <div class="row justify-content-center text-center" style="height:inherit">
            <div class="col-12"> 

                <img class="img-fluid" src="/02_IMAGES/af_TOUR_PLACES_PAGE/f_Edakkal_Caves.jpg" >
                <div class="title1">Edakkal caves</div>



                <!-- end details -->


                <!-- end hero -->



            </div>

        </div>
    </div>



    <!-- end container -->
    <div class="description container movie-card text-justify">


        <p>What kind of people were our ancestors? What kind of lives did they lead? Often, the paths leading to the answers to these questions are as fascinating as the answers themselves. If you would like to walk a little way along one such wonderful path of discovery, one good place to visit would be the Edakkal caves in the Ambukuthi Hills in North Kerala, considered to be one of the earliest centres of human habitation. Inside the cave you will find ancient stone scripts, pictorial wall inscriptions of human and animal figures with peculiar headdresses, the swastik form, symbols and cave drawings of human figures, wheels, bows, knives, trees and so on.</p><p>
            Similar cave drawings, considered to be 7000 years old, are found only in Stiriya in the European Alps and a few rocky places in Africa. The caves, which are found around 10 km from Sultan Bathery, are two natural rock formations believed to have been formed by a large split in a huge rock.</p>

        <!-- end avatars -->


        <!-- end column2 -->
    </div>
    <!-- end description -->
    
        <div class="row justify-content-center">
        <div class=".col ">

            <a href="/"> <i class="fa fa-arrow-circle-left"></i>  Home Page </a>

        </div>

    </div>


</div>

@stop


@section('meenmutty')

<div class="container" id="tour-places" style="background: red;" >
    <div class="text-center" style="font-size:20px;margin-top: 5px">
        Use arrow keys to move to next place
    </div> 

    <div class="container " style="width:100%;height:300px;padding: 0px;border: 0px;margin: 30px 0px;overflow: hidden" id="image_holder">


        <div class="row justify-content-center text-center" style="height:inherit">
            <div class="col-12"> 

                <img class="img-fluid" src="/02_IMAGES/af_TOUR_PLACES_PAGE/c_meenmutty.jpg" >
                <div class="title1">Meenmutty Waterfalls</div>



                <!-- end details -->


                <!-- end hero -->



            </div>

        </div>
    </div>



    <!-- end container -->
    <div class="description container movie-card text-justify">


        <p>         

            An interesting 2 km jungle trek will lead to the largest and most spectacular waterfall in Wayanad - Meenmutty Waterfalls. A unique feature of Meenmutty waterfalls is that the water drops from nearly 1000 ft over three stages, presenting a triple-decker effect. The falls is located on the Ooty main road in Wayanad.   

        </p>


        <!-- end avatars -->


        <!-- end column2 -->
    </div>
    <!-- end description -->
    <div class="row justify-content-center">
        <div class=".col ">

            <a href="/"> <i class="fa fa-arrow-circle-left"></i>  Home Page </a>

        </div>

    </div>

</div>

@stop


@section('banasura')

<div class="container" id="tour-places" style="background: red;" >
    <div class="text-center" style="font-size:20px;margin-top: 5px">
        Use arrow keys to move to next place.
    </div> 

    <div class="container " style="width:100%;height:300px;padding: 0px;border: 0px;margin: 30px 0px;overflow: hidden" id="image_holder">


        <div class="row justify-content-center text-center" style="height:inherit">
            <div class="col-12"> 

                <img class="img-fluid" src="/02_IMAGES/af_TOUR_PLACES_PAGE/a_banasura.jpg" >
                <div class="title1">Banasura Dam</div>



                <!-- end details -->


                <!-- end hero -->



            </div>

        </div>
    </div>



    <!-- end container -->
    <div class="description container movie-card text-justify">


        <p>         

            Another tourist attraction of Kalpetta is Banasura Sagar dam. It is the largest earth dam in India and the second largest of its kind in Asia. The dam is made up of massive stacks of stones and boulders. Situated about 15 km from Kalpetta, the dam holds a large expanse of water and its picturesque beauty is enhanced by the chain of mountains seen on the backdrop.  It is constructed in the Banasura Lake and the nearby mountains are known as Banasura Hills. Legends say that the Asura king of Banasura, (the son of King Mahabali, who is believed to visit Kerala during every Onam festival) undertook a severe penance on the top of these hills and thus it was named after him.  The scenic mountains beckon adventure tourists and the dam site is an ideal starting point for trekking. 
            <br>
            During monsoon, visitors may also be able to see small islands in the dam’s reservoir. They are formed while the flooded reservoir submerges the surrounding areas too.  The vast expanse of the crystal clear water of the dam dotted with small islands is a photographer’s delight.



        </p>


        <!-- end avatars -->


        <!-- end column2 -->
    </div>
    <!-- end description -->
    <div class="row justify-content-center">
        <div class=".col ">

            <a href="/"> <i class="fa fa-arrow-circle-left"></i>  Home Page </a>

        </div>

    </div>

</div>

@stop

<div id="impress">

    <!--
        
        Here is where interesting thing start to happen.
        
        Each step of the presentation should be an element inside the `#impress` with a class name
        of `step`. These step elements are positioned, rotated and scaled by impress.js, and
        the 'camera' shows them on each step of the presentation.
        
        Positioning information is passed through data attributes.
        
        In the example below we only specify x and y position of the step element with `data-x="-1000"`
        and `data-y="-1500"` attributes. This means that **the center** of the element (yes, the center)
        will be positioned in point x = -1000px and y = -1500px of the presentation 'canvas'.
        
        It will not be rotated or scaled.
        
        --------
        Plugins: For first slide, set the autoplay time to a custom 10 seconds.
        
    -->

    <!--
        
        The `id` attribute of the step element is used to identify it in the URL, but it's optional.
        If it is not defined, it will get a default value of `step-N` where N is a number of slide.
        
        So in the example below it'll be `step-2`.
        
        The hash part of the url when this step is active will be `#/step-2`.
        
        You can also use `#step-2` in a link, to point directly to this particular step.
        
        Please note, that while `#/step-2` (with slash) would also work in a link it's not recommended.
        Using classic `id`-based links like `#step-2` makes these links usable also in fallback mode.
        
    -->

    <!--
        
        This is an example of step element being scaled.
        
        Again, we use a `data-` attribute, this time it's `data-scale="4"`, so it means that this
        element will be 4 times larger than the others.
        From presentation and transitions point of view it means, that it will have to be scaled
        down (4 times) to make it back to its correct size.
        
    -->


    <div id="banasura" class="step" data-x="0" data-y="0" data-scale="6">
        @yield('banasura')  

    </div>

    <!--
        
        This element introduces rotation.
        
        Notation shouldn't be a surprise. We use `data-rotate="90"` attribute, meaning that this
        element should be rotated by 90 degrees clockwise.
        
    -->
    <div id="meenmutty" class="step" data-x="4000" data-y="5000" data-rotate="-90" data-scale="1">
        @yield('meenmutty')    </div>


    <div id="edakkal" class="step" data-x="8000" data-y="5000" data-rotate="90" data-scale="1">
        @yield('edakkal')    </div>

    <div id="chembra" class="step" data-x="8000" data-y="10000" data-rotate="-90" data-scale="1">
        @yield('chembra')    </div>


    <div id="muthanga" class="step" data-x="4000" data-y="10000" data-rotate="90" data-scale="1">
        @yield('muthanga')    </div>

    <div id="kuruva" class="step" data-x="0" data-y="10000" data-rotate="-90" data-scale="1">
        @yield('kuruva')    </div>

    <div id="viewpoint" class="step" data-x="-4000" data-y="10000" data-rotate="90" data-scale="1">
        @yield('viewpoint')    </div>





</div>

<!--
    This is a UI plugin. You can read more about plugins in src/plugins/README.md.
    For now, I'll just tell you that this adds some graphical controls to navigate the
    presentation. In the CSS file you can style them as you want. We've put them bottom right.
-->
<div id="impress-toolbar"></div>


<!--
    
    Hint is not related to impress.js in any way.
    
    But it can show you how to use impress.js features in creative way.
    
    When the presentation step is shown (selected) its element gets the class of "active" and the body element
    gets the class based on active step id `impress-on-ID` (where ID is the step's id)... It may not be
    so clear because of all these "ids" in previous sentence, so for example when the first step (the one with
    the id of `bored`) is active, body element gets a class of `impress-on-bored`.
    
    This class is used by this hint below. Check CSS file to see how it's shown with delayed CSS animation when
    the first step of presentation is visible for a couple of seconds.
    
    ...
    
    And when it comes to this piece of JavaScript below ... kids, don't do this at home ;)
    It's just a quick and dirty workaround to get different hint text for touch devices.
    In a real world it should be at least placed in separate JS file ... and the touch content should be
    probably just hidden somewhere in HTML - not hard-coded in the script.
    
    Just sayin' ;)
    
-->
<div class="hint">
    <p>Use a spacebar or arrow keys to navigate. <br/>
        Press 'P' to launch speaker console.</p>
</div>
<script>
    if ("ontouchstart" in document.documentElement) {
        document.querySelector(".hint").innerHTML = "<p>Swipe left or right to navigate</p>";
    }
</script>

<!--
    
    Last, but not least.
    
    To make all described above really work, you need to include impress.js in the page.
    I strongly encourage to minify it first.
    
    In here I just include full source of the script to make it more readable.
    
    You also need to call a `impress().init()` function to initialize impress.js presentation.
    And you should do it in the end of your document. Not only because it's a good practice, but also
    because it should be done when the whole document is ready.
    Of course you can wrap it in any kind of "DOM ready" event, but I was too lazy to do so ;)
    
-->
<script src="https://impress.js.org/js/impress.js"></script>
<script>impress().init();</script>

<!--
    
    The `impress()` function also gives you access to the API that controls the presentation.
    
    Just store the result of the call:
    
        var api = impress();
    
    and you will get three functions you can call:
    
        `api.init()` - initializes the presentation,
        `api.next()` - moves to next step of the presentation,
        `api.prev()` - moves to previous step of the presentation,
        `api.goto( stepIndex | stepElementId | stepElement, [duration] )` - moves the presentation to the step given by its index number
                id or the DOM element; second parameter can be used to define duration of the transition in ms,
                but it's optional - if not provided default transition duration for the presentation will be used.
    
    You can also simply call `impress()` again to get the API, so `impress().next()` is also allowed.
    Don't worry, it wont initialize the presentation again.
    
    For some example uses of this API check the last part of the source of impress.js where the API
    is used in event handlers.
    
-->


<!-- social, tracking etc, ignore -->

<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-9117370-1']);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();

</script>
