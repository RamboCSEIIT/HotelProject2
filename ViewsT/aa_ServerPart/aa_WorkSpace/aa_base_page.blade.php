
<!--  It says document type for browser as html 5 file
<meta name="viewport" content="width=device-width, initial-scale=1">

-->

<!DOCTYPE html>
<html>

<head>
    
  <meta charset="utf-8">
    <!--  For IE only -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
  <meta name="description" content="">
  <meta name="author" content=""> 
  <meta http-equiv="ScreenOrientation" content="autoRotate:disabled">
  <link rel="shortcut icon" type="image/png" href="02_IMAGES/favicon.png">
  
  
  
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
   

    @yield('cssBlock')
    
  <title>    @yield('title')  </title>
</head>

<body  id="body" data-spy="scroll"  data-offset="1"  class="impress-not-supported" >
   
    
    
 @yield('content')
 
 

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
 
  
 
 @yield('bottomJS')
    
    
      
   
   
    
   
</body>

</html>
