@extends('aa_ServerPart.aa_WorkSpace.aa_base_page') 
 
@section('title') 
      Project
@stop 
 
@section('cssBlock') 
  @include('aa_ServerPart.aa_WorkSpace.af_TOUR_PLACES_PAGE.aa_include.za_css') 
@stop

@section('content') 

  
<div class="fallback-message" hidden>
    <p>Your browser <b>doesn't support the features required</b> by impress.js, so you are presented with a simplified version of this presentation.</p>
    <p>For the best experience please use the latest <b>Chrome</b>, <b>Safari</b> or <b>Firefox</b> browser.</p>
</div>

        
 
         @include('aa_ServerPart.ab_body.af_TOUR_PLACES_PAGE.body')
         
 
                 

   
    
      
 
           
     
     
 @stop
 
 
 @section('bottomJS') 
 
      @include('aa_ServerPart.aa_WorkSpace.af_TOUR_PLACES_PAGE.aa_include.zb_javascript')   
     
 @stop 
 
 
  