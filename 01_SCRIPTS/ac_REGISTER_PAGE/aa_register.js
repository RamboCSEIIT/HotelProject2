$(document).ready(function () { 
    
    

    $('#registration_form').validate({// initialize the plugin
        rules: {

            
            verify_email_name_name:
                    {
                        required: true,
                        email: true,
                        equalTo: "#email_name_id"

                    },
            password_name:
                    {
                        required: true,
                        minlength: 3
                    },
            verify_password_name:
                    {
                        required: true,
                        minlength: 3,
                        equalTo: "#password_id"
                    },

            first_name_name:
                    {
                        required: true,
                        minlength: 3
                    },

            last_name_name:
                    {
                        required: true,
                        minlength: 3
                    }
        }
    });
    
     

});  

    


