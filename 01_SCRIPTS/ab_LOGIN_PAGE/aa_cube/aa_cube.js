var DEFAULT_WIDTH = 420;
var VIEWPORT_WIDTH_FACT_MOB = 0.8;
var VIEWPORT_HEIGHT_FACT_MOB = 0.7;
var VIEWPORT_HEIGHT_FACT = 0.7;
var MOBILE_WIDTH_BREAK_DOWN = 498;
var adjust_shadow_offset;
var current_X = -1;
var current_Y = 0;
var increment = 1;
var width_height;
var adjust_top;
var adjust_bottom;
var height;
var width;
var depth;
var HEIGHT_WIDTH_RATIO_MOBILE = 1.4;
var HEIGHT_WIDTH_RATIO = 1.4;
var shadow_offset = 40;
var front_translate;
var back_translate;
var top_translate;
var bottom_translate;
var left_translate;
var right_translate;
var angle = 0;
var transitionEvent;
var button_flag;

function whichTransitionEvent() {
    var t, el = document.createElement("fakeelement");
    var transitions = {
        "transition": "transitionend",
        "OTransition": "oTransitionEnd",
        "MozTransition": "transitionend",
        "WebkitTransition": "webkitTransitionEnd"
    };
    for (t in transitions) {
        if (el.style[t] !== undefined) {
            return transitions[t];
        }
    }
}
////////////////////////////////////////////// 
function rotate_align_reset(angle_val_reset, rotate) {
    $("#cubeR").removeClass('rotate_transition');
    rot_Y = 'rotateY(' + 0 + 'deg)';
    $("#cubeR").css({
        transform: rot_Y
    });
    rot_Y = 'rotateY(' + angle_val_reset + 'deg)';
    $("#cube").css({
        transform: rot_Y
    }).promise().done(function () {
        setTimeout(function () {
            rotate_align_resetC(rotate);
        }, 200);
    });
}

function rotate_align_resetC(rotate) {
    $("#cubeR").addClass("rotate_transition");
    var rot_Y = 'rotateY(' + rotate + 'deg)';
    $("#cubeR").css({
        transform: rot_Y
    });
    $("#cubeR").one(transitionEvent, function (event) {
        //alert("The transition has ended!"); 
        button_flag = true;
        //  $("#cube").addClass("rotate_transition");
    });
}

function rotate_align_reseti(angle_val_reset, rotate) {
    rotate_align_resetR(angle_val_reset);
    //   rotate_align_resetC(rotate);
}

// .load() method deprecated from jQuery 1.8 onward
$(window).on("load", function() {

        transitionEvent = whichTransitionEvent();
    button_flag = true;
    /// front
    $("#r-back-f").click(function () {
        if (button_flag) {
            button_flag = false;
            rotate_align_reset(0, 90);
        }
    });
    // Link admin
    //preview
    $("#r-next-f").click(function () {
        if (button_flag) {
            button_flag = false;
            rotate_align_reset(0, -90);
        }
    });
    /////////////////////////////////////////////////////  
    //left
    $("#r-backl").click(function () {
        if (button_flag) {
            button_flag = false;
            rotate_align_reset(90, 90);
            //rotate_align_resetR(90);
        } // rotate_align_resetR(90);
        // rotate_align_resetC(90);
    });
    $("#r-nextl").click(function () {
        if (button_flag) {
            button_flag = false;
            rotate_align_reset(90, -90);
        }
        // alert("hey");
    });
    /////////////////////////////////////////////////////////   
    //back     
    $("#r-backb").click(function () {
        if (button_flag) {
            button_flag = false;
            rotate_align_reset(180, 90);
        } //alert("hey");
    });
    //user login
    $("#r-nextb").click(function () {
        if (button_flag) {
            button_flag = false;
            rotate_align_reset(180, -90);
        }
    });
    ///////////////////////////////////////////    
    //right
    $("#r-backv").click(function () {
        if (button_flag) {
            button_flag = false;
            rotate_align_reset(270, 90);
        }
    });
    //user login
    $("#r-nextv").click(function () {
        if (button_flag) {
            button_flag = false;
            rotate_align_reset(270, -90);
        }
    });
    //////////////////////////// LEFT PANEL /////////////////////////////
    //////////////////////////// BACK  PANEL  ADDRESS /////////////////////////////  
    //contact
    test_fps();
    make_cube();
    // adjust();
    keyboard();
    $(window).resize(function () 
    {
        // alert("Test");
        make_cube();
    });
  
});


$(document).ready(function () {
    ///////////////////////////////// FRONT PANEL ////////////////////////////////  
       hide_cube(true);
});

function hide_cube(hide) {
    if (hide == true) {
        $("#top").css("visibility", "hidden");
        $("#bottom").css("visibility", "hidden");
        $("#left").css("visibility", "hidden");
        $("#right").css("visibility", "hidden");
        $("#front").css("visibility", "hidden");
        $("#back").css("visibility", "hidden");
        $("#shadow").css("visibility", "hidden");
    } else {
        $("#top").css("visibility", "visible");
        $("#bottom").css("visibility", "visible");
        $("#left").css("visibility", "visible");
        $("#right").css("visibility", "visible");
        $("#front").css("visibility", "visible");
        $("#back").css("visibility", "visible");
        $("#shadow").css("visibility", "visible");
    }
}

function make_cube() {
    hide_cube(true);
    if ($(window).width() >= MOBILE_WIDTH_BREAK_DOWN) {
        adjust_full_width();
         // alert("desktop");
    } else {
       //  alert("mobile");
        adjust_mobile_width();
    }
    hide_cube(false);
}

function adjust_mobile_width() {
    depth = width = Math.floor($(window).width() * VIEWPORT_WIDTH_FACT_MOB);
    height = Math.floor($("#form_containter").outerHeight() + 30);
    $("#viewport").css("width", width);
    $("#viewport").css("height", height);
    $("#viewport").addClass("center_viewport");
    
    
    
    //width = height = depth = DEFAULT_WIDTH;
    var half_depth = Math.floor(depth / 2);
    var half_width = Math.floor(width / 2);
    var half_height = Math.floor(height / 2);
    // $("#viewport").css("top", Math.floor($(document).height() / 2 ));
    // $("#viewport").css("left", Math.floor($(document).width() / 2 ));
    /*
     $("#viewport").css("top", Math.floor($(document).height() / 2 - half_height));
     $("#viewport").css("left", Math.floor($(document).width() / 2 - half_width));
     */
    adjust_shadow_offset = half_height + shadow_offset;
    front_translate = 'translateZ(' + half_depth + 'px)';
    back_translate = 'translateZ(' + (-half_depth) + 'px)' + ' ' + 'rotateY(180deg)';
    top_translate = 'translateY(' + (-half_height) + 'px)' + ' ' + 'rotateX(90deg)';
    bottom_translate = 'translateY(' + half_height + 'px)' + ' ' + 'rotateX(-90deg)';
    left_translate = 'translateX(' + (-half_width) + 'px)' + ' ' + 'rotateY(-90deg)';
    right_translate = 'translateX(' + half_width + 'px)' + ' ' + 'rotateY(90deg)';
    shdow_translate = 'translateY(' + adjust_shadow_offset + 'px)' + ' ' + 'rotateX(-90deg)';
    adjust_view = 'translateZ(' + (-half_depth) + 'px)';
    $("#cubeB").css({
        transform: adjust_view
    });
    $("#front").css({
        transform: front_translate
    });
    $("#back").css({
        transform: back_translate
    });
    $("#top").css({
        transform: top_translate
    });
    $("#bottom").css({
        transform: bottom_translate
    });
    $("#left").css({
        transform: left_translate
    });
    $("#right").css({
        transform: right_translate
    });
    $("#shadow").css({
        transform: shdow_translate
    });
    // var height_content = 
    $("#top").css("height", width);
    $("#bottom").css("height", width);
    $("#shadow").css("height", width);
    var adjust_top = Math.floor(-width / 2);
    var adjust_bottom = Math.floor(height - width / 2);
    top_translate = 'translateY(' + (adjust_top) + 'px)' + ' ' + 'rotateX(90deg)';
    bottom_translate = 'translateY(' + adjust_bottom + 'px)' + ' ' + 'rotateX(-90deg)';
    $("#top").css({
        transform: top_translate
    });
    // $("#top").css("height", width);
    adjust_shadow_offset = adjust_bottom + shadow_offset;
    shdow_translate = 'translateY(' + adjust_shadow_offset + 'px)' + ' ' + 'rotateX(-90deg)';
    $("#bottom").css({
        transform: bottom_translate
    });
    $("#shadow").css({
        transform: shdow_translate
    });
}
function adjust_mobile_widthX() {
    depth = width = Math.floor($(window).width() * VIEWPORT_WIDTH_FACT_MOB);
    height = Math.floor($("#form_containter").outerHeight() + 30);
    $("#viewport").css("width", width);
    $("#viewport").css("height", height);
    $("#viewport").addClass("center_viewport");
    
    
    
    //width = height = depth = DEFAULT_WIDTH;
    var half_depth = Math.floor(depth / 2);
    var half_width = Math.floor(width / 2);
    var half_height = Math.floor(height / 2);
    // $("#viewport").css("top", Math.floor($(document).height() / 2 ));
    // $("#viewport").css("left", Math.floor($(document).width() / 2 ));
    /*
     $("#viewport").css("top", Math.floor($(document).height() / 2 - half_height));
     $("#viewport").css("left", Math.floor($(document).width() / 2 - half_width));
     */
    adjust_shadow_offset = half_height + shadow_offset;
    front_translate = 'translateZ(' + half_depth + 'px)';
    back_translate = 'translateZ(' + (-half_depth) + 'px)' + ' ' + 'rotateY(180deg)';
    top_translate = 'translateY(' + (-half_height) + 'px)' + ' ' + 'rotateX(90deg)';
    bottom_translate = 'translateY(' + half_height + 'px)' + ' ' + 'rotateX(-90deg)';
    left_translate = 'translateX(' + (-half_width) + 'px)' + ' ' + 'rotateY(-90deg)';
    right_translate = 'translateX(' + half_width + 'px)' + ' ' + 'rotateY(90deg)';
    shdow_translate = 'translateY(' + adjust_shadow_offset + 'px)' + ' ' + 'rotateX(-90deg)';
    adjust_view = 'translateZ(' + (-half_depth) + 'px)';
    $("#cubeB").css({
        transform: adjust_view
    });
    $("#front").css({
        transform: front_translate
    });
    $("#back").css({
        transform: back_translate
    });
    $("#top").css({
        transform: top_translate
    });
    $("#bottom").css({
        transform: bottom_translate
    });
    $("#left").css({
        transform: left_translate
    });
    $("#right").css({
        transform: right_translate
    });
    $("#shadow").css({
        transform: shdow_translate
    });
    // var height_content = 
    $("#top").css("height", width);
    $("#bottom").css("height", width);
    $("#shadow").css("height", width);
    var adjust_top = Math.floor(-width / 2);
    var adjust_bottom = Math.floor(height - width / 2);
    top_translate = 'translateY(' + (adjust_top) + 'px)' + ' ' + 'rotateX(90deg)';
    bottom_translate = 'translateY(' + adjust_bottom + 'px)' + ' ' + 'rotateX(-90deg)';
    $("#top").css({
        transform: top_translate
    });
    // $("#top").css("height", width);
    adjust_shadow_offset = adjust_bottom + shadow_offset;
    shdow_translate = 'translateY(' + adjust_shadow_offset + 'px)' + ' ' + 'rotateX(-90deg)';
    $("#bottom").css({
        transform: bottom_translate
    });
    $("#shadow").css({
        transform: shdow_translate
    });
}

function adjust_full_width() {
    width = height = depth = Math.floor($("#form_containter").outerHeight() + 30);
    
     $("#viewport").css("width", width);
    $("#viewport").css("height", height);
    $("#viewport").addClass("center_viewport");
    //width = height = depth = DEFAULT_WIDTH;
    var half_depth = Math.floor(depth / 2);
    var half_width = Math.floor(width / 2);
    var half_height = Math.floor(height / 2);
    // $("#viewport").css("top", Math.floor($(document).height() / 2 ));
    // $("#viewport").css("left", Math.floor($(document).width() / 2 ));
    /*
     $("#viewport").css("top", Math.floor($(document).height() / 2 - half_height));
     $("#viewport").css("left", Math.floor($(document).width() / 2 - half_width));
     */
    adjust_shadow_offset = half_height + shadow_offset;
    front_translate = 'translateZ(' + half_depth + 'px)';
    back_translate = 'translateZ(' + (-half_depth) + 'px)' + ' ' + 'rotateY(180deg)';
    top_translate = 'translateY(' + (-half_height) + 'px)' + ' ' + 'rotateX(90deg)';
    bottom_translate = 'translateY(' + half_height + 'px)' + ' ' + 'rotateX(-90deg)';
    left_translate = 'translateX(' + (-half_width) + 'px)' + ' ' + 'rotateY(-90deg)';
    right_translate = 'translateX(' + half_width + 'px)' + ' ' + 'rotateY(90deg)';
    shdow_translate = 'translateY(' + adjust_shadow_offset + 'px)' + ' ' + 'rotateX(-90deg)';
    adjust_view = 'translateZ(' + (-half_depth) + 'px)';
    $("#cubeB").css({
        transform: adjust_view
    });
    $("#front").css({
        transform: front_translate
    });
    $("#back").css({
        transform: back_translate
    });
    /*
    $("#top").css({
        transform: top_translate
    });
    $("#bottom").css({
        transform: bottom_translate
    });*/
    $("#left").css({
        transform: left_translate
    });
    $("#right").css({
        transform: right_translate
    });
    /*
    $("#shadow").css({
        transform: shdow_translate
    });*/
    // var height_content = 
    
       // var height_content = 
    $("#top").css("height", width);
    $("#bottom").css("height", width);
    $("#shadow").css("height", width);
    var adjust_top = Math.floor(-width / 2);
    var adjust_bottom = Math.floor(height - width / 2);
    top_translate = 'translateY(' + (adjust_top) + 'px)' + ' ' + 'rotateX(90deg)';
    bottom_translate = 'translateY(' + adjust_bottom + 'px)' + ' ' + 'rotateX(-90deg)';
    $("#top").css({
        transform: top_translate
    });
    // $("#top").css("height", width);
    adjust_shadow_offset = adjust_bottom + shadow_offset;
    shdow_translate = 'translateY(' + adjust_shadow_offset + 'px)' + ' ' + 'rotateX(-90deg)';
    $("#bottom").css({
        transform: bottom_translate
    });
    $("#shadow").css({
        transform: shdow_translate
    });
}

function test_fps() {
    var rot_X = 'rotateX(' + current_X + 'deg)';
    $("#cube").css({
        transform: rot_X
    });
    $("#X").html(current_X);
}

function keyboard() {
    // your base. I'm in it
    $(document).keydown(function (e) {
        /*
         37 - left
         38 - up
         39 - right
         40 - down 
         */
        var rot_XY;
        if (e.keyCode == 40) {
            console.log("left pressed");
            // current_X = current_X - increment;
            rot_XY = 'rotateX(' + current_X + 'deg)' + ' ' + 'rotateY(' + current_Y + 'deg)';
            $("#cube").css({
                transform: rot_XY
            });
            $("#X").html(current_X);
            $("#Y").html(current_Y);
            return false;
        }
        //right
        if (e.keyCode == 38) {
            //   current_X = current_X + increment;
            console.log("right pressed");
            rot_XY = 'rotateX(' + current_X + 'deg)' + ' ' + 'rotateY(' + current_Y + 'deg)';
            $("#cube").css({
                transform: rot_XY
            });
            $("#X").html(current_X);
            $("#Y").html(current_Y);
            return false;
        }
        if (e.keyCode == 37) {
            current_Y = current_Y - increment;
            console.log("up pressed");
            rot_XY = 'rotateX(' + current_X + 'deg)' + ' ' + 'rotateY(' + current_Y + 'deg)';
            $("#cube").css({
                transform: rot_XY
            });
            $("#X").html(current_X);
            $("#Y").html(current_Y);
            return false;
        }
        //40 down
        if (e.keyCode == 39) {
            current_Y = current_Y + increment;
            console.log("down pressed");
            rot_XY = 'rotateX(' + current_X + 'deg)' + ' ' + 'rotateY(' + current_Y + 'deg)';
            $("#cube").css({
                transform: rot_XY
            });
            $("#X").html(current_X);
            $("#Y").html(current_Y);
            return false;
        }
    });
}


