 

             
var SCRIPTS_IN_PATH = '01_SCRIPTS/**/*.*';

var jshint = require('gulp-jshint');
var notify = require('gulp-notify');

var gulp                                                      = require('gulp');
var notify                                             = require('gulp-notify');
var concat                                             = require('gulp-concat');
var sourcemaps                                     = require('gulp-sourcemaps');
var gulpif                                                 = require('gulp-if');
var uglify                                             = require('gulp-uglify');
var prettify                                     = require('gulp-jsbeautifier');
var removeEmptyLines                       = require('gulp-remove-empty-lines');

 

 



module.exports =
{


            javaScriptError: function ()
            {

                var result =
                        gulp.src(SCRIPTS_IN_PATH)
                        .pipe(jshint({
                            
                            "esversion": 6
                }))
                        // Use gulp-notify as jshint reporter
                        .pipe(notify(function (file)
                        {
                            if (file.jshint.success)
                            {
                                // Don't show something if success
                                return false;
                            }

                            var errors = file.jshint.results.map(function (data)
                            {
                                if (data.error)
                                {
                                    return "(" + data.error.line + ':' + data.error.character + ') ' + data.error.reason;
                                }
                            }).join("\n");

                            return file.relative + " (" + file.jshint.results.length + " errors)\n" + errors;
                        }));
                //  console.log('JS Error Ended Started -----------------------------------------------------');
                return result;

            }
            
            
            
 


 };

 
global.javaScript = function (options)
{
    
                    var result =
                        gulp.src(  options.SCRIPTS_IN_PATH)
                        .pipe(gulpif(process.env.NODE_ENV === 'development', sourcemaps.init()))
                        .pipe(concat(options.SCRIPTS_OUT_FILE_NAME))
                        .pipe(removeEmptyLines())


                        .pipe(gulpif(process.env.NODE_ENV === 'development',
                                prettify({
                                    mode: 'VERIFY_AND_WRITE'
                                }),
                                uglify()

                                ))
                        .pipe(gulpif(process.env.NODE_ENV === 'development', sourcemaps.write()))
                        .pipe(sourcemaps.write())
                        .pipe(gulp.dest(options.SCRIPTS_OUT_PATH ));
                  //      .pipe(notify({message: 'home page Javascript task complete'}));
                return result;


    
    //console.log("Test");
};

     